-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 15, 2016 at 03:58 AM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `prs`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointment_schedules`
--

CREATE TABLE IF NOT EXISTS `appointment_schedules` (
  `iAppointmentScheduleId` int(1) unsigned NOT NULL,
  `iDate` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `appointment_types`
--

CREATE TABLE IF NOT EXISTS `appointment_types` (
`iAppointmentTypeId` int(1) unsigned NOT NULL,
  `sCode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `sDescription` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bActive` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `iDateCreated` int(1) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `appointment_types`
--

INSERT INTO `appointment_types` (`iAppointmentTypeId`, `sCode`, `sDescription`, `bActive`, `iDateCreated`) VALUES
(1, 'Checkup', 'Checkup', 1, 1472752908),
(2, 'Vaccine', 'Vaccinations', 1, 1472752950),
(3, 'test', 'test', 1, 1473661430);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
`iCityId` int(1) unsigned NOT NULL,
  `sCity` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `iProvinceId` int(11) unsigned NOT NULL,
  `bActive` tinyint(1) unsigned NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=1637 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`iCityId`, `sCity`, `iProvinceId`, `bActive`) VALUES
(1, 'Bangued', 1, 1),
(2, 'Boliney', 1, 1),
(3, 'Bucay', 1, 1),
(4, 'Bucloc', 1, 1),
(5, 'Daguioman', 1, 1),
(6, 'Danglas', 1, 1),
(7, 'Dolores', 1, 1),
(8, 'La Paz', 1, 1),
(9, 'Lacub', 1, 1),
(10, 'Lagangilang', 1, 1),
(11, 'Lagayan', 1, 1),
(12, 'LangiCityIden', 1, 1),
(13, 'Licuan-Baay', 1, 1),
(14, 'Luba', 1, 1),
(15, 'Malibcong', 1, 1),
(16, 'Manabo', 1, 1),
(17, 'Peñarrubia', 1, 1),
(18, 'PiCityIdigan', 1, 1),
(19, 'Pilar', 1, 1),
(20, 'Sallapadan', 1, 1),
(21, 'San IsiCityIdro', 1, 1),
(22, 'San Juan', 1, 1),
(23, 'San Quintin', 1, 1),
(24, 'Tayum', 1, 1),
(25, 'Tineg', 1, 1),
(26, 'Tubo', 1, 1),
(27, 'Villaviciosa', 1, 1),
(28, 'Butuan City', 2, 1),
(29, 'Buenavista', 2, 1),
(30, 'Cabadbaran', 2, 1),
(31, 'Carmen', 2, 1),
(32, 'Jabonga', 2, 1),
(33, 'Kitcharao', 2, 1),
(34, 'Las Nieves', 2, 1),
(35, 'Magallanes', 2, 1),
(36, 'Nasipit', 2, 1),
(37, 'Remedios T. Romualdez', 2, 1),
(38, 'Santiago', 2, 1),
(39, 'Tubay', 2, 1),
(40, 'Bayugan', 3, 1),
(41, 'Bunawan', 3, 1),
(42, 'Esperanza', 3, 1),
(43, 'La Paz', 3, 1),
(44, 'Loreto', 3, 1),
(45, 'ProsperiCityIdad', 3, 1),
(46, 'Rosario', 3, 1),
(47, 'San Francisco', 3, 1),
(48, 'San Luis', 3, 1),
(49, 'Santa Josefa', 3, 1),
(50, 'Sibagat', 3, 1),
(51, 'Talacogon', 3, 1),
(52, 'Trento', 3, 1),
(53, 'Veruela', 3, 1),
(54, 'Altavas', 4, 1),
(55, 'Balete', 4, 1),
(56, 'Banga', 4, 1),
(57, 'Batan', 4, 1),
(58, 'Buruanga', 4, 1),
(59, 'Ibajay', 4, 1),
(60, 'Kalibo', 4, 1),
(61, 'Lezo', 4, 1),
(62, 'Libacao', 4, 1),
(63, 'Madalag', 4, 1),
(64, 'Makato', 4, 1),
(65, 'Malay', 4, 1),
(66, 'Malinao', 4, 1),
(67, 'Nabas', 4, 1),
(68, 'New Washington', 4, 1),
(69, 'Numancia', 4, 1),
(70, 'Tangalan', 4, 1),
(71, 'Legazpi City', 5, 1),
(72, 'Ligao City', 5, 1),
(73, 'Tabaco City', 5, 1),
(74, 'Bacacay', 5, 1),
(75, 'Camalig', 5, 1),
(76, 'Daraga', 5, 1),
(77, 'Guinobatan', 5, 1),
(78, 'Jovellar', 5, 1),
(79, 'Libon', 5, 1),
(80, 'Malilipot', 5, 1),
(81, 'Malinao', 5, 1),
(82, 'Manito', 5, 1),
(83, 'Oas', 5, 1),
(84, 'Pio Duran', 5, 1),
(85, 'Polangui', 5, 1),
(86, 'Rapu-Rapu', 5, 1),
(87, 'Santo Domingo', 5, 1),
(88, 'Tiwi', 5, 1),
(89, 'Anini-y', 6, 1),
(90, 'Barbaza', 6, 1),
(91, 'Belison', 6, 1),
(92, 'Bugasong', 6, 1),
(93, 'Caluya', 6, 1),
(94, 'Culasi', 6, 1),
(95, 'Hamtic', 6, 1),
(96, 'Laua-an', 6, 1),
(97, 'Libertad', 6, 1),
(98, 'Pandan', 6, 1),
(99, 'Patnongon', 6, 1),
(100, 'San Jose', 6, 1),
(101, 'San Remigio', 6, 1),
(102, 'Sebaste', 6, 1),
(103, 'Sibalom', 6, 1),
(104, 'Tibiao', 6, 1),
(105, 'Tobias Fornier', 6, 1),
(106, 'Valderrama', 6, 1),
(107, 'Calanasan', 7, 1),
(108, 'Conner', 7, 1),
(109, 'Flora', 7, 1),
(110, 'Kabugao', 7, 1),
(111, 'Luna', 7, 1),
(112, 'Pudtol', 7, 1),
(113, 'Santa Marcela', 7, 1),
(114, 'Baler', 8, 1),
(115, 'Casiguran', 8, 1),
(116, 'Dilasag', 8, 1),
(117, 'Dinalungan', 8, 1),
(118, 'Dingalan', 8, 1),
(119, 'Dipaculao', 8, 1),
(120, 'Maria Aurora', 8, 1),
(121, 'San Luis', 8, 1),
(122, 'Isabela City', 9, 1),
(123, 'Akbar', 9, 1),
(124, 'Al-Barka', 9, 1),
(125, 'Hadji Mohammad Ajul', 9, 1),
(126, 'Hadji Muhtamad', 9, 1),
(127, 'Lamitan', 9, 1),
(128, 'Lantawan', 9, 1),
(129, 'Maluso', 9, 1),
(130, 'Sumisip', 9, 1),
(131, 'Tabuan-Lasa', 9, 1),
(132, 'Tipo-Tipo', 9, 1),
(133, 'Tuburan', 9, 1),
(134, 'Ungkaya Pukan', 9, 1),
(135, 'Balanga City', 10, 1),
(136, 'Abucay', 10, 1),
(137, 'Bagac', 10, 1),
(138, 'Dinalupihan', 10, 1),
(139, 'Hermosa', 10, 1),
(140, 'Limay', 10, 1),
(141, 'Mariveles', 10, 1),
(142, 'Morong', 10, 1),
(143, 'Orani', 10, 1),
(144, 'Orion', 10, 1),
(145, 'Pilar', 10, 1),
(146, 'Samal', 10, 1),
(147, 'Basco', 11, 1),
(148, 'Itbayat', 11, 1),
(149, 'Ivana', 11, 1),
(150, 'Mahatao', 11, 1),
(151, 'Sabtang', 11, 1),
(152, 'Uyugan', 11, 1),
(153, 'Batangas City', 12, 1),
(154, 'Lipa City', 12, 1),
(155, 'Tanauan City', 12, 1),
(156, 'Agoncillo', 12, 1),
(157, 'Alitagtag', 12, 1),
(158, 'Balayan', 12, 1),
(159, 'Balete', 12, 1),
(160, 'Bauan', 12, 1),
(161, 'Calaca', 12, 1),
(162, 'Calatagan', 12, 1),
(163, 'Cuenca', 12, 1),
(164, 'Ibaan', 12, 1),
(165, 'Laurel', 12, 1),
(166, 'Lemery', 12, 1),
(167, 'Lian', 12, 1),
(168, 'Lobo', 12, 1),
(169, 'Mabini', 12, 1),
(170, 'Malvar', 12, 1),
(171, 'Mataas na Kahoy', 12, 1),
(172, 'Nasugbu', 12, 1),
(173, 'Padre Garcia', 12, 1),
(174, 'Rosario', 12, 1),
(175, 'San Jose', 12, 1),
(176, 'San Juan', 12, 1),
(177, 'San Luis', 12, 1),
(178, 'San Nicolas', 12, 1),
(179, 'San Pascual', 12, 1),
(180, 'Santa Teresita', 12, 1),
(181, 'Santo Tomas', 12, 1),
(182, 'Taal', 12, 1),
(183, 'Talisay', 12, 1),
(184, 'Taysan', 12, 1),
(185, 'Tingloy', 12, 1),
(186, 'Tuy', 12, 1),
(187, 'Baguio City', 13, 1),
(188, 'Atok', 13, 1),
(189, 'Bakun', 13, 1),
(190, 'Bokod', 13, 1),
(191, 'Buguias', 13, 1),
(192, 'Itogon', 13, 1),
(193, 'Kabayan', 13, 1),
(194, 'Kapangan', 13, 1),
(195, 'Kibungan', 13, 1),
(196, 'La TriniCityIdad', 13, 1),
(197, 'Mankayan', 13, 1),
(198, 'Sablan', 13, 1),
(199, 'Tuba', 13, 1),
(200, 'Tublay', 13, 1),
(201, 'Almeria', 14, 1),
(202, 'Biliran', 14, 1),
(203, 'Cabucgayan', 14, 1),
(204, 'Caibiran', 14, 1),
(205, 'Culaba', 14, 1),
(206, 'Kawayan', 14, 1),
(207, 'Maripipi', 14, 1),
(208, 'Naval', 14, 1),
(209, 'Tagbilaran City', 15, 1),
(210, 'Alburquerque', 15, 1),
(211, 'Alicia', 15, 1),
(212, 'Anda', 15, 1),
(213, 'Antequera', 15, 1),
(214, 'Baclayon', 15, 1),
(215, 'Balilihan', 15, 1),
(216, 'Batuan', 15, 1),
(217, 'Bien UniCityIdo', 15, 1),
(218, 'Bilar', 15, 1),
(219, 'Buenavista', 15, 1),
(220, 'Calape', 15, 1),
(221, 'Candijay', 15, 1),
(222, 'Carmen', 15, 1),
(223, 'Catigbian', 15, 1),
(224, 'Clarin', 15, 1),
(225, 'Corella', 15, 1),
(226, 'Cortes', 15, 1),
(227, 'Dagohoy', 15, 1),
(228, 'Danao', 15, 1),
(229, 'Dauis', 15, 1),
(230, 'Dimiao', 15, 1),
(231, 'Duero', 15, 1),
(232, 'Garcia Hernandez', 15, 1),
(233, 'Getafe', 15, 1),
(234, 'Guindulman', 15, 1),
(235, 'Inabanga', 15, 1),
(236, 'Jagna', 15, 1),
(237, 'Lila', 15, 1),
(238, 'Loay', 15, 1),
(239, 'Loboc', 15, 1),
(240, 'Loon', 15, 1),
(241, 'Mabini', 15, 1),
(242, 'Maribojoc', 15, 1),
(243, 'Panglao', 15, 1),
(244, 'Pilar', 15, 1),
(245, 'PresiCityIdent Carlos P. Garcia', 15, 1),
(246, 'Sagbayan', 15, 1),
(247, 'San IsiCityIdro', 15, 1),
(248, 'San Miguel', 15, 1),
(249, 'Sevilla', 15, 1),
(250, 'Sierra Bullones', 15, 1),
(251, 'Sikatuna', 15, 1),
(252, 'Talibon', 15, 1),
(253, 'TriniCityIdad', 15, 1),
(254, 'Tubigon', 15, 1),
(255, 'Ubay', 15, 1),
(256, 'Valencia', 15, 1),
(257, 'Malaybalay City', 16, 1),
(258, 'Valencia City', 16, 1),
(259, 'Baungon', 16, 1),
(260, 'Cabanglasan', 16, 1),
(261, 'Damulog', 16, 1),
(262, 'Dangcagan', 16, 1),
(263, 'Don Carlos', 16, 1),
(264, 'Impasug-ong', 16, 1),
(265, 'Kadingilan', 16, 1),
(266, 'Kalilangan', 16, 1),
(267, 'Kibawe', 16, 1),
(268, 'Kitaotao', 16, 1),
(269, 'Lantapan', 16, 1),
(270, 'Libona', 16, 1),
(271, 'Malitbog', 16, 1),
(272, 'Manolo Fortich', 16, 1),
(273, 'Maramag', 16, 1),
(274, 'Pangantucan', 16, 1),
(275, 'Quezon', 16, 1),
(276, 'San Fernando', 16, 1),
(277, 'Sumilao', 16, 1),
(278, 'Talakag', 16, 1),
(279, 'Malolos City', 17, 1),
(280, 'Meycauayan City', 17, 1),
(281, 'San Jose del Monte City', 17, 1),
(282, 'Angat', 17, 1),
(283, 'Balagtas', 17, 1),
(284, 'Baliuag', 17, 1),
(285, 'Bocaue', 17, 1),
(286, 'Bulacan', 17, 1),
(287, 'Bustos', 17, 1),
(288, 'Calumpit', 17, 1),
(289, 'Doña Remedios TriniCityIdad', 17, 1),
(290, 'Guiguinto', 17, 1),
(291, 'Hagonoy', 17, 1),
(292, 'Marilao', 17, 1),
(293, 'Norzagaray', 17, 1),
(294, 'Obando', 17, 1),
(295, 'Pandi', 17, 1),
(296, 'Paombong', 17, 1),
(297, 'PlariCityIdel', 17, 1),
(298, 'Pulilan', 17, 1),
(299, 'San Ildefonso', 17, 1),
(300, 'San Miguel', 17, 1),
(301, 'San Rafael', 17, 1),
(302, 'Santa Maria', 17, 1),
(303, 'Tuguegarao City', 18, 1),
(304, 'Abulug', 18, 1),
(305, 'Alcala', 18, 1),
(306, 'Allacapan', 18, 1),
(307, 'Amulung', 18, 1),
(308, 'Aparri', 18, 1),
(309, 'Baggao', 18, 1),
(310, 'Ballesteros', 18, 1),
(311, 'Buguey', 18, 1),
(312, 'Calayan', 18, 1),
(313, 'Camalaniugan', 18, 1),
(314, 'Claveria', 18, 1),
(315, 'Enrile', 18, 1),
(316, 'Gattaran', 18, 1),
(317, 'Gonzaga', 18, 1),
(318, 'Iguig', 18, 1),
(319, 'Lal-lo', 18, 1),
(320, 'Lasam', 18, 1),
(321, 'Pamplona', 18, 1),
(322, 'Peñablanca', 18, 1),
(323, 'Piat', 18, 1),
(324, 'Rizal', 18, 1),
(325, 'Sanchez-Mira', 18, 1),
(326, 'Santa Ana', 18, 1),
(327, 'Santa Praxedes', 18, 1),
(328, 'Santa Teresita', 18, 1),
(329, 'Santo Niño', 18, 1),
(330, 'Solana', 18, 1),
(331, 'Tuao', 18, 1),
(332, 'Basud', 19, 1),
(333, 'Capalonga', 19, 1),
(334, 'Daet', 19, 1),
(335, 'Jose Panganiban', 19, 1),
(336, 'Labo', 19, 1),
(337, 'Mercedes', 19, 1),
(338, 'Paracale', 19, 1),
(339, 'San Lorenzo Ruiz', 19, 1),
(340, 'San Vicente', 19, 1),
(341, 'Santa Elena', 19, 1),
(342, 'Talisay', 19, 1),
(343, 'Vinzons', 19, 1),
(344, 'Iriga City', 20, 1),
(345, 'Naga City', 20, 1),
(346, 'Baao', 20, 1),
(347, 'Balatan', 20, 1),
(348, 'Bato', 20, 1),
(349, 'Bombon', 20, 1),
(350, 'Buhi', 20, 1),
(351, 'Bula', 20, 1),
(352, 'Cabusao', 20, 1),
(353, 'Calabanga', 20, 1),
(354, 'Camaligan', 20, 1),
(355, 'Canaman', 20, 1),
(356, 'Caramoan', 20, 1),
(357, 'Del Gallego', 20, 1),
(358, 'Gainza', 20, 1),
(359, 'Garchitorena', 20, 1),
(360, 'Goa', 20, 1),
(361, 'Lagonoy', 20, 1),
(362, 'Libmanan', 20, 1),
(363, 'Lupi', 20, 1),
(364, 'Magarao', 20, 1),
(365, 'Milaor', 20, 1),
(366, 'Minalabac', 20, 1),
(367, 'Nabua', 20, 1),
(368, 'Ocampo', 20, 1),
(369, 'Pamplona', 20, 1),
(370, 'Pasacao', 20, 1),
(371, 'Pili', 20, 1),
(372, 'Presentacion', 20, 1),
(373, 'Ragay', 20, 1),
(374, 'Sagñay', 20, 1),
(375, 'San Fernando', 20, 1),
(376, 'San Jose', 20, 1),
(377, 'Sipocot', 20, 1),
(378, 'Siruma', 20, 1),
(379, 'Tigaon', 20, 1),
(380, 'Tinambac', 20, 1),
(381, 'Catarman', 21, 1),
(382, 'Guinsiliban', 21, 1),
(383, 'Mahinog', 21, 1),
(384, 'Mambajao', 21, 1),
(385, 'Sagay', 21, 1),
(386, 'Roxas City', 22, 1),
(387, 'Cuartero', 22, 1),
(388, 'Dao', 22, 1),
(389, 'Dumalag', 22, 1),
(390, 'Dumarao', 22, 1),
(391, 'Ivisan', 22, 1),
(392, 'Jamindan', 22, 1),
(393, 'Ma-ayon', 22, 1),
(394, 'Mambusao', 22, 1),
(395, 'Panay', 22, 1),
(396, 'Panitan', 22, 1),
(397, 'Pilar', 22, 1),
(398, 'Pontevedra', 22, 1),
(399, 'PresiCityIdent Roxas', 22, 1),
(400, 'Sapi-an', 22, 1),
(401, 'Sigma', 22, 1),
(402, 'Tapaz', 22, 1),
(403, 'Bagamanoc', 23, 1),
(404, 'Baras', 23, 1),
(405, 'Bato', 23, 1),
(406, 'Caramoran', 23, 1),
(407, 'Gigmoto', 23, 1),
(408, 'Pandan', 23, 1),
(409, 'Panganiban', 23, 1),
(410, 'San Andres', 23, 1),
(411, 'San Miguel', 23, 1),
(412, 'Viga', 23, 1),
(413, 'Virac', 23, 1),
(414, 'Cavite City', 24, 1),
(415, 'Dasmariñas City', 24, 1),
(416, 'Tagaytay City', 24, 1),
(417, 'Trece Martires City', 24, 1),
(418, 'Alfonso', 24, 1),
(419, 'Amadeo', 24, 1),
(420, 'Bacoor', 24, 1),
(421, 'Carmona', 24, 1),
(422, 'General Mariano Alvarez', 24, 1),
(423, 'General Emilio Aguinaldo', 24, 1),
(424, 'General Trias', 24, 1),
(425, 'Imus', 24, 1),
(426, 'Indang', 24, 1),
(427, 'Kawit', 24, 1),
(428, 'Magallanes', 24, 1),
(429, 'Maragondon', 24, 1),
(430, 'Mendez', 24, 1),
(431, 'Naic', 24, 1),
(432, 'Noveleta', 24, 1),
(433, 'Rosario', 24, 1),
(434, 'Silang', 24, 1),
(435, 'Tanza', 24, 1),
(436, 'Ternate', 24, 1),
(437, 'Bogo City', 25, 1),
(438, 'Cebu City', 25, 1),
(439, 'Carcar City', 25, 1),
(440, 'Danao City', 25, 1),
(441, 'Lapu-Lapu City', 25, 1),
(442, 'Mandaue City', 25, 1),
(443, 'Naga City', 25, 1),
(444, 'Talisay City', 25, 1),
(445, 'Toledo City', 25, 1),
(446, 'Alcantara', 25, 1),
(447, 'Alcoy', 25, 1),
(448, 'Alegria', 25, 1),
(449, 'Aloguinsan', 25, 1),
(450, 'Argao', 25, 1),
(451, 'Asturias', 25, 1),
(452, 'Badian', 25, 1),
(453, 'Balamban', 25, 1),
(454, 'Bantayan', 25, 1),
(455, 'Barili', 25, 1),
(456, 'Boljoon', 25, 1),
(457, 'Borbon', 25, 1),
(458, 'Carmen', 25, 1),
(459, 'Catmon', 25, 1),
(460, 'Compostela', 25, 1),
(461, 'Consolacion', 25, 1),
(462, 'Cordoba', 25, 1),
(463, 'Daanbantayan', 25, 1),
(464, 'Dalaguete', 25, 1),
(465, 'Dumanjug', 25, 1),
(466, 'Ginatilan', 25, 1),
(467, 'Liloan', 25, 1),
(468, 'MadriCityIdejos', 25, 1),
(469, 'Malabuyoc', 25, 1),
(470, 'Medellin', 25, 1),
(471, 'Minglanilla', 25, 1),
(472, 'Moalboal', 25, 1),
(473, 'Oslob', 25, 1),
(474, 'Pilar', 25, 1),
(475, 'Pinamungahan', 25, 1),
(476, 'Poro', 25, 1),
(477, 'Ronda', 25, 1),
(478, 'Samboan', 25, 1),
(479, 'San Fernando', 25, 1),
(480, 'San Francisco', 25, 1),
(481, 'San Remigio', 25, 1),
(482, 'Santa Fe', 25, 1),
(483, 'Santander', 25, 1),
(484, 'Sibonga', 25, 1),
(485, 'Sogod', 25, 1),
(486, 'Tabogon', 25, 1),
(487, 'Tabuelan', 25, 1),
(488, 'Tuburan', 25, 1),
(489, 'Tudela', 25, 1),
(490, 'Compostela', 26, 1),
(491, 'Laak', 26, 1),
(492, 'Mabini', 26, 1),
(493, 'Maco', 26, 1),
(494, 'Maragusan', 26, 1),
(495, 'Mawab', 26, 1),
(496, 'Monkayo', 26, 1),
(497, 'Montevista', 26, 1),
(498, 'Nabunturan', 26, 1),
(499, 'New Bataan', 26, 1),
(500, 'Pantukan', 26, 1),
(501, 'KiCityIdapawan City', 27, 1),
(502, 'Alamada', 27, 1),
(503, 'Aleosan', 27, 1),
(504, 'Antipas', 27, 1),
(505, 'Arakan', 27, 1),
(506, 'Banisilan', 27, 1),
(507, 'Carmen', 27, 1),
(508, 'Kabacan', 27, 1),
(509, 'Libungan', 27, 1),
(510, 'M''lang', 27, 1),
(511, 'Magpet', 27, 1),
(512, 'Makilala', 27, 1),
(513, 'Matalam', 27, 1),
(514, 'MiCityIdsayap', 27, 1),
(515, 'Pigkawayan', 27, 1),
(516, 'Pikit', 27, 1),
(517, 'PresiCityIdent Roxas', 27, 1),
(518, 'Tulunan', 27, 1),
(519, 'Panabo City', 28, 1),
(520, 'Island Garden City of Samal', 28, 1),
(521, 'Tagum City', 28, 1),
(522, 'Asuncion', 28, 1),
(523, 'Braulio E. Dujali', 28, 1),
(524, 'Carmen', 28, 1),
(525, 'Kapalong', 28, 1),
(526, 'New Corella', 28, 1),
(527, 'San IsiCityIdro', 28, 1),
(528, 'Santo Tomas', 28, 1),
(529, 'Talaingod', 28, 1),
(530, 'Davao City', 29, 1),
(531, 'Digos City', 29, 1),
(532, 'Bansalan', 29, 1),
(533, 'Don Marcelino', 29, 1),
(534, 'Hagonoy', 29, 1),
(535, 'Jose Abad Santos', 29, 1),
(536, 'Kiblawan', 29, 1),
(537, 'Magsaysay', 29, 1),
(538, 'Malalag', 29, 1),
(539, 'Malita', 29, 1),
(540, 'Matanao', 29, 1),
(541, 'Padada', 29, 1),
(542, 'Santa Cruz', 29, 1),
(543, 'Santa Maria', 29, 1),
(544, 'Sarangani', 29, 1),
(545, 'Sulop', 29, 1),
(546, 'Mati City', 30, 1),
(547, 'Baganga', 30, 1),
(548, 'Banaybanay', 30, 1),
(549, 'Boston', 30, 1),
(550, 'Caraga', 30, 1),
(551, 'Cateel', 30, 1),
(552, 'Governor Generoso', 30, 1),
(553, 'Lupon', 30, 1),
(554, 'Manay', 30, 1),
(555, 'San IsiCityIdro', 30, 1),
(556, 'Tarragona', 30, 1),
(557, 'Arteche', 31, 1),
(558, 'Balangiga', 31, 1),
(559, 'Balangkayan', 31, 1),
(560, 'Borongan', 31, 1),
(561, 'Can-aviCityId', 31, 1),
(562, 'Dolores', 31, 1),
(563, 'General MacArthur', 31, 1),
(564, 'Giporlos', 31, 1),
(565, 'Guiuan', 31, 1),
(566, 'Hernani', 31, 1),
(567, 'Jipapad', 31, 1),
(568, 'Lawaan', 31, 1),
(569, 'Llorente', 31, 1),
(570, 'Maslog', 31, 1),
(571, 'Maydolong', 31, 1),
(572, 'Mercedes', 31, 1),
(573, 'Oras', 31, 1),
(574, 'Quinapondan', 31, 1),
(575, 'Salcedo', 31, 1),
(576, 'San Julian', 31, 1),
(577, 'San Policarpo', 31, 1),
(578, 'Sulat', 31, 1),
(579, 'Taft', 31, 1),
(580, 'Buenavista', 32, 1),
(581, 'Jordan', 32, 1),
(582, 'Nueva Valencia', 32, 1),
(583, 'San Lorenzo', 32, 1),
(584, 'Sibunag', 32, 1),
(585, 'Aguinaldo', 33, 1),
(586, 'Alfonso Lista', 33, 1),
(587, 'Asipulo', 33, 1),
(588, 'Banaue', 33, 1),
(589, 'Hingyon', 33, 1),
(590, 'Hungduan', 33, 1),
(591, 'Kiangan', 33, 1),
(592, 'Lagawe', 33, 1),
(593, 'Lamut', 33, 1),
(594, 'Mayoyao', 33, 1),
(595, 'Tinoc', 33, 1),
(596, 'Batac City', 34, 1),
(597, 'Laoag City', 34, 1),
(598, 'Adams', 34, 1),
(599, 'Bacarra', 34, 1),
(600, 'Badoc', 34, 1),
(601, 'Bangui', 34, 1),
(602, 'Banna', 34, 1),
(603, 'Burgos', 34, 1),
(604, 'Carasi', 34, 1),
(605, 'Currimao', 34, 1),
(606, 'Dingras', 34, 1),
(607, 'Dumalneg', 34, 1),
(608, 'Marcos', 34, 1),
(609, 'Nueva Era', 34, 1),
(610, 'Pagudpud', 34, 1),
(611, 'Paoay', 34, 1),
(612, 'Pasuquin', 34, 1),
(613, 'PiCityIddig', 34, 1),
(614, 'Pinili', 34, 1),
(615, 'San Nicolas', 34, 1),
(616, 'Sarrat', 34, 1),
(617, 'Solsona', 34, 1),
(618, 'Vintar', 34, 1),
(619, 'Candon City', 35, 1),
(620, 'Vigan City', 35, 1),
(621, 'Alilem', 35, 1),
(622, 'Banayoyo', 35, 1),
(623, 'Bantay', 35, 1),
(624, 'Burgos', 35, 1),
(625, 'Cabugao', 35, 1),
(626, 'Caoayan', 35, 1),
(627, 'Cervantes', 35, 1),
(628, 'Galimuyod', 35, 1),
(629, 'Gregorio Del Pilar', 35, 1),
(630, 'LiCityIdliCityIdda', 35, 1),
(631, 'Magsingal', 35, 1),
(632, 'Nagbukel', 35, 1),
(633, 'Narvacan', 35, 1),
(634, 'Quirino', 35, 1),
(635, 'Salcedo', 35, 1),
(636, 'San Emilio', 35, 1),
(637, 'San Esteban', 35, 1),
(638, 'San Ildefonso', 35, 1),
(639, 'San Juan', 35, 1),
(640, 'San Vicente', 35, 1),
(641, 'Santa', 35, 1),
(642, 'Santa Catalina', 35, 1),
(643, 'Santa Cruz', 35, 1),
(644, 'Santa Lucia', 35, 1),
(645, 'Santa Maria', 35, 1),
(646, 'Santiago', 35, 1),
(647, 'Santo Domingo', 35, 1),
(648, 'Sigay', 35, 1),
(649, 'Sinait', 35, 1),
(650, 'Sugpon', 35, 1),
(651, 'Suyo', 35, 1),
(652, 'Tagudin', 35, 1),
(653, 'Iloilo City', 36, 1),
(654, 'Passi City', 36, 1),
(655, 'Ajuy', 36, 1),
(656, 'Alimodian', 36, 1),
(657, 'Anilao', 36, 1),
(658, 'Badiangan', 36, 1),
(659, 'Balasan', 36, 1),
(660, 'Banate', 36, 1),
(661, 'Barotac Nuevo', 36, 1),
(662, 'Barotac Viejo', 36, 1),
(663, 'Batad', 36, 1),
(664, 'Bingawan', 36, 1),
(665, 'Cabatuan', 36, 1),
(666, 'Calinog', 36, 1),
(667, 'Carles', 36, 1),
(668, 'Concepcion', 36, 1),
(669, 'Dingle', 36, 1),
(670, 'Dueñas', 36, 1),
(671, 'Dumangas', 36, 1),
(672, 'Estancia', 36, 1),
(673, 'Guimbal', 36, 1),
(674, 'Igbaras', 36, 1),
(675, 'Janiuay', 36, 1),
(676, 'Lambunao', 36, 1),
(677, 'Leganes', 36, 1),
(678, 'Lemery', 36, 1),
(679, 'Leon', 36, 1),
(680, 'Maasin', 36, 1),
(681, 'Miagao', 36, 1),
(682, 'Mina', 36, 1),
(683, 'New Lucena', 36, 1),
(684, 'Oton', 36, 1),
(685, 'Pavia', 36, 1),
(686, 'Pototan', 36, 1),
(687, 'San Dionisio', 36, 1),
(688, 'San Enrique', 36, 1),
(689, 'San Joaquin', 36, 1),
(690, 'San Miguel', 36, 1),
(691, 'San Rafael', 36, 1),
(692, 'Santa Barbara', 36, 1),
(693, 'Sara', 36, 1),
(694, 'Tigbauan', 36, 1),
(695, 'Tubungan', 36, 1),
(696, 'Zarraga', 36, 1),
(697, 'Cauayan City', 37, 1),
(698, 'Santiago City', 37, 1),
(699, 'Alicia', 37, 1),
(700, 'Angadanan', 37, 1),
(701, 'Aurora', 37, 1),
(702, 'Benito Soliven', 37, 1),
(703, 'Burgos', 37, 1),
(704, 'Cabagan', 37, 1),
(705, 'Cabatuan', 37, 1),
(706, 'Cordon', 37, 1),
(707, 'Delfin Albano', 37, 1),
(708, 'Dinapigue', 37, 1),
(709, 'Divilacan', 37, 1),
(710, 'Echague', 37, 1),
(711, 'Gamu', 37, 1),
(712, 'Ilagan', 37, 1),
(713, 'Jones', 37, 1),
(714, 'Luna', 37, 1),
(715, 'Maconacon', 37, 1),
(716, 'Mallig', 37, 1),
(717, 'Naguilian', 37, 1),
(718, 'Palanan', 37, 1),
(719, 'Quezon', 37, 1),
(720, 'Quirino', 37, 1),
(721, 'Ramon', 37, 1),
(722, 'Reina Mercedes', 37, 1),
(723, 'Roxas', 37, 1),
(724, 'San Agustin', 37, 1),
(725, 'San Guillermo', 37, 1),
(726, 'San IsiCityIdro', 37, 1),
(727, 'San Manuel', 37, 1),
(728, 'San Mariano', 37, 1),
(729, 'San Mateo', 37, 1),
(730, 'San Pablo', 37, 1),
(731, 'Santa Maria', 37, 1),
(732, 'Santo Tomas', 37, 1),
(733, 'Tumauini', 37, 1),
(734, 'Tabuk', 38, 1),
(735, 'Balbalan', 38, 1),
(736, 'Lubuagan', 38, 1),
(737, 'Pasil', 38, 1),
(738, 'Pinukpuk', 38, 1),
(739, 'Rizal', 38, 1),
(740, 'Tanudan', 38, 1),
(741, 'Tinglayan', 38, 1),
(742, 'San Fernando City', 39, 1),
(743, 'Agoo', 39, 1),
(744, 'Aringay', 39, 1),
(745, 'Bacnotan', 39, 1),
(746, 'Bagulin', 39, 1),
(747, 'Balaoan', 39, 1),
(748, 'Bangar', 39, 1),
(749, 'Bauang', 39, 1),
(750, 'Burgos', 39, 1),
(751, 'Caba', 39, 1),
(752, 'Luna', 39, 1),
(753, 'Naguilian', 39, 1),
(754, 'Pugo', 39, 1),
(755, 'Rosario', 39, 1),
(756, 'San Gabriel', 39, 1),
(757, 'San Juan', 39, 1),
(758, 'Santo Tomas', 39, 1),
(759, 'Santol', 39, 1),
(760, 'Sudipen', 39, 1),
(761, 'Tubao', 39, 1),
(762, 'Biñan City', 40, 1),
(763, 'Calamba City', 40, 1),
(764, 'San Pablo City', 40, 1),
(765, 'Santa Rosa City', 40, 1),
(766, 'Alaminos', 40, 1),
(767, 'Bay', 40, 1),
(768, 'Cabuyao', 40, 1),
(769, 'Calauan', 40, 1),
(770, 'Cavinti', 40, 1),
(771, 'Famy', 40, 1),
(772, 'Kalayaan', 40, 1),
(773, 'Liliw', 40, 1),
(774, 'Los Baños', 40, 1),
(775, 'Luisiana', 40, 1),
(776, 'Lumban', 40, 1),
(777, 'Mabitac', 40, 1),
(778, 'Magdalena', 40, 1),
(779, 'Majayjay', 40, 1),
(780, 'Nagcarlan', 40, 1),
(781, 'Paete', 40, 1),
(782, 'Pagsanjan', 40, 1),
(783, 'Pakil', 40, 1),
(784, 'Pangil', 40, 1),
(785, 'Pila', 40, 1),
(786, 'Rizal', 40, 1),
(787, 'San Pedro', 40, 1),
(788, 'Santa Cruz', 40, 1),
(789, 'Santa Maria', 40, 1),
(790, 'Siniloan', 40, 1),
(791, 'Victoria', 40, 1),
(792, 'Iligan City', 41, 1),
(793, 'Bacolod', 41, 1),
(794, 'Baloi', 41, 1),
(795, 'Baroy', 41, 1),
(796, 'Kapatagan', 41, 1),
(797, 'Kauswagan', 41, 1),
(798, 'Kolambugan', 41, 1),
(799, 'Lala', 41, 1),
(800, 'Linamon', 41, 1),
(801, 'Magsaysay', 41, 1),
(802, 'Maigo', 41, 1),
(803, 'Matungao', 41, 1),
(804, 'Munai', 41, 1),
(805, 'Nunungan', 41, 1),
(806, 'Pantao Ragat', 41, 1),
(807, 'Pantar', 41, 1),
(808, 'Poona Piagapo', 41, 1),
(809, 'Salvador', 41, 1),
(810, 'Sapad', 41, 1),
(811, 'Sultan Naga Dimaporo', 41, 1),
(812, 'Tagoloan', 41, 1),
(813, 'Tangcal', 41, 1),
(814, 'Tubod', 41, 1),
(815, 'Marawi City', 42, 1),
(816, 'Bacolod-Kalawi', 42, 1),
(817, 'Balabagan', 42, 1),
(818, 'Balindong', 42, 1),
(819, 'Bayang', 42, 1),
(820, 'BiniCityIdayan', 42, 1),
(821, 'Buadiposo-Buntong', 42, 1),
(822, 'Bubong', 42, 1),
(823, 'Bumbaran', 42, 1),
(824, 'Butig', 42, 1),
(825, 'Calanogas', 42, 1),
(826, 'Ditsaan-Ramain', 42, 1),
(827, 'Ganassi', 42, 1),
(828, 'Kapai', 42, 1),
(829, 'Kapatagan', 42, 1),
(830, 'Lumba-Bayabao', 42, 1),
(831, 'Lumbaca-Unayan', 42, 1),
(832, 'Lumbatan', 42, 1),
(833, 'Lumbayanague', 42, 1),
(834, 'Madalum', 42, 1),
(835, 'Madamba', 42, 1),
(836, 'Maguing', 42, 1),
(837, 'Malabang', 42, 1),
(838, 'Marantao', 42, 1),
(839, 'Marogong', 42, 1),
(840, 'Masiu', 42, 1),
(841, 'Mulondo', 42, 1),
(842, 'Pagayawan', 42, 1),
(843, 'Piagapo', 42, 1),
(844, 'Poona Bayabao', 42, 1),
(845, 'Pualas', 42, 1),
(846, 'Saguiaran', 42, 1),
(847, 'Sultan Dumalondong', 42, 1),
(848, 'Picong', 42, 1),
(849, 'Tagoloan II', 42, 1),
(850, 'Tamparan', 42, 1),
(851, 'Taraka', 42, 1),
(852, 'Tubaran', 42, 1),
(853, 'Tugaya', 42, 1),
(854, 'Wao', 42, 1),
(855, 'Ormoc City', 43, 1),
(856, 'Tacloban City', 43, 1),
(857, 'Abuyog', 43, 1),
(858, 'Alangalang', 43, 1),
(859, 'Albuera', 43, 1),
(860, 'Babatngon', 43, 1),
(861, 'Barugo', 43, 1),
(862, 'Bato', 43, 1),
(863, 'Baybay', 43, 1),
(864, 'Burauen', 43, 1),
(865, 'Calubian', 43, 1),
(866, 'Capoocan', 43, 1),
(867, 'Carigara', 43, 1),
(868, 'Dagami', 43, 1),
(869, 'Dulag', 43, 1),
(870, 'Hilongos', 43, 1),
(871, 'Hindang', 43, 1),
(872, 'Inopacan', 43, 1),
(873, 'Isabel', 43, 1),
(874, 'Jaro', 43, 1),
(875, 'Javier', 43, 1),
(876, 'Julita', 43, 1),
(877, 'Kananga', 43, 1),
(878, 'La Paz', 43, 1),
(879, 'Leyte', 43, 1),
(880, 'Liloan', 43, 1),
(881, 'MacArthur', 43, 1),
(882, 'Mahaplag', 43, 1),
(883, 'Matag-ob', 43, 1),
(884, 'Matalom', 43, 1),
(885, 'Mayorga', 43, 1),
(886, 'MeriCityIda', 43, 1),
(887, 'Palo', 43, 1),
(888, 'Palompon', 43, 1),
(889, 'Pastrana', 43, 1),
(890, 'San IsiCityIdro', 43, 1),
(891, 'San Miguel', 43, 1),
(892, 'Santa Fe', 43, 1),
(893, 'Sogod', 43, 1),
(894, 'Tabango', 43, 1),
(895, 'Tabontabon', 43, 1),
(896, 'Tanauan', 43, 1),
(897, 'Tolosa', 43, 1),
(898, 'Tunga', 43, 1),
(899, 'Villaba', 43, 1),
(900, 'Cotabato City', 44, 1),
(901, 'Ampatuan', 44, 1),
(902, 'Barira', 44, 1),
(903, 'Buldon', 44, 1),
(904, 'Buluan', 44, 1),
(905, 'Datu Abdullah Sangki', 44, 1),
(906, 'Datu Anggal MiCityIdtimbang', 44, 1),
(907, 'Datu Blah T. Sinsuat', 44, 1),
(908, 'Datu Hoffer Ampatuan', 44, 1),
(909, 'Datu Montawal', 44, 1),
(910, 'Datu Odin Sinsuat', 44, 1),
(911, 'Datu Paglas', 44, 1),
(912, 'Datu Piang', 44, 1),
(913, 'Datu Salibo', 44, 1),
(914, 'Datu Saudi-Ampatuan', 44, 1),
(915, 'Datu Unsay', 44, 1),
(916, 'General Salipada K. Pendatun', 44, 1),
(917, 'Guindulungan', 44, 1),
(918, 'Kabuntalan', 44, 1),
(919, 'Mamasapano', 44, 1),
(920, 'Mangudadatu', 44, 1),
(921, 'Matanog', 44, 1),
(922, 'Northern Kabuntalan', 44, 1),
(923, 'Pagalungan', 44, 1),
(924, 'Paglat', 44, 1),
(925, 'Pandag', 44, 1),
(926, 'Parang', 44, 1),
(927, 'Rajah Buayan', 44, 1),
(928, 'Shariff Aguak', 44, 1),
(929, 'Shariff Saydona Mustapha', 44, 1),
(930, 'South Upi', 44, 1),
(931, 'Sultan Kudarat', 44, 1),
(932, 'Sultan Mastura', 44, 1),
(933, 'Sultan sa Barongis', 44, 1),
(934, 'Talayan', 44, 1),
(935, 'Talitay', 44, 1),
(936, 'Upi', 44, 1),
(937, 'Boac', 45, 1),
(938, 'Buenavista', 45, 1),
(939, 'Gasan', 45, 1),
(940, 'Mogpog', 45, 1),
(941, 'Santa Cruz', 45, 1),
(942, 'Torrijos', 45, 1),
(943, 'Masbate City', 46, 1),
(944, 'Aroroy', 46, 1),
(945, 'Baleno', 46, 1),
(946, 'Balud', 46, 1),
(947, 'Batuan', 46, 1),
(948, 'Cataingan', 46, 1),
(949, 'Cawayan', 46, 1),
(950, 'Claveria', 46, 1),
(951, 'Dimasalang', 46, 1),
(952, 'Esperanza', 46, 1),
(953, 'Mandaon', 46, 1),
(954, 'Milagros', 46, 1),
(955, 'Mobo', 46, 1),
(956, 'Monreal', 46, 1),
(957, 'Palanas', 46, 1),
(958, 'Pio V. Corpuz', 46, 1),
(959, 'Placer', 46, 1),
(960, 'San Fernando', 46, 1),
(961, 'San Jacinto', 46, 1),
(962, 'San Pascual', 46, 1),
(963, 'Uson', 46, 1),
(964, 'Caloocan', 47, 1),
(965, 'Las Piñas', 47, 1),
(966, 'Makati', 47, 1),
(967, 'Malabon', 47, 1),
(968, 'Mandaluyong', 47, 1),
(969, 'Manila', 47, 1),
(970, 'Marikina', 47, 1),
(971, 'Muntinlupa', 47, 1),
(972, 'Navotas', 47, 1),
(973, 'Parañaque', 47, 1),
(974, 'Pasay', 47, 1),
(975, 'Pasig', 47, 1),
(976, 'Quezon City', 47, 1),
(977, 'San Juan City', 47, 1),
(978, 'Taguig', 47, 1),
(979, 'Valenzuela City', 47, 1),
(980, 'Pateros', 47, 1),
(981, 'Oroquieta City', 48, 1),
(982, 'Ozamiz City', 48, 1),
(983, 'Tangub City', 48, 1),
(984, 'Aloran', 48, 1),
(985, 'Baliangao', 48, 1),
(986, 'Bonifacio', 48, 1),
(987, 'Calamba', 48, 1),
(988, 'Clarin', 48, 1),
(989, 'Concepcion', 48, 1),
(990, 'Don Victoriano Chiongbian', 48, 1),
(991, 'Jimenez', 48, 1),
(992, 'Lopez Jaena', 48, 1),
(993, 'Panaon', 48, 1),
(994, 'PlariCityIdel', 48, 1),
(995, 'Sapang Dalaga', 48, 1),
(996, 'Sinacaban', 48, 1),
(997, 'Tudela', 48, 1),
(998, 'Cagayan de Oro', 49, 1),
(999, 'Gingoog City', 49, 1),
(1000, 'AlubijiCityId', 49, 1),
(1001, 'Balingasag', 49, 1),
(1002, 'Balingoan', 49, 1),
(1003, 'Binuangan', 49, 1),
(1004, 'Claveria', 49, 1),
(1005, 'El Salvador', 49, 1),
(1006, 'Gitagum', 49, 1),
(1007, 'Initao', 49, 1),
(1008, 'Jasaan', 49, 1),
(1009, 'Kinoguitan', 49, 1),
(1010, 'Lagonglong', 49, 1),
(1011, 'Laguindingan', 49, 1),
(1012, 'Libertad', 49, 1),
(1013, 'Lugait', 49, 1),
(1014, 'Magsaysay', 49, 1),
(1015, 'Manticao', 49, 1),
(1016, 'Medina', 49, 1),
(1017, 'Naawan', 49, 1),
(1018, 'Opol', 49, 1),
(1019, 'Salay', 49, 1),
(1020, 'Sugbongcogon', 49, 1),
(1021, 'Tagoloan', 49, 1),
(1022, 'Talisayan', 49, 1),
(1023, 'Villanueva', 49, 1),
(1024, 'Barlig', 50, 1),
(1025, 'Bauko', 50, 1),
(1026, 'Besao', 50, 1),
(1027, 'Bontoc', 50, 1),
(1028, 'Natonin', 50, 1),
(1029, 'Paracelis', 50, 1),
(1030, 'Sabangan', 50, 1),
(1031, 'Sadanga', 50, 1),
(1032, 'Sagada', 50, 1),
(1033, 'Tadian', 50, 1),
(1034, 'Bacolod City', 51, 1),
(1035, 'Bago City', 51, 1),
(1036, 'Cadiz City', 51, 1),
(1037, 'Escalante City', 51, 1),
(1038, 'Himamaylan City', 51, 1),
(1039, 'Kabankalan City', 51, 1),
(1040, 'La Carlota City', 51, 1),
(1041, 'Sagay City', 51, 1),
(1042, 'San Carlos City', 51, 1),
(1043, 'Silay City', 51, 1),
(1044, 'Sipalay City', 51, 1),
(1045, 'Talisay City', 51, 1),
(1046, 'Victorias City', 51, 1),
(1047, 'Binalbagan', 51, 1),
(1048, 'Calatrava', 51, 1),
(1049, 'Candoni', 51, 1),
(1050, 'Cauayan', 51, 1),
(1051, 'Enrique B. Magalona', 51, 1),
(1052, 'Hinigaran', 51, 1),
(1053, 'Hinoba-an', 51, 1),
(1054, 'Ilog', 51, 1),
(1055, 'Isabela', 51, 1),
(1056, 'La Castellana', 51, 1),
(1057, 'Manapla', 51, 1),
(1058, 'Moises Padilla', 51, 1),
(1059, 'Murcia', 51, 1),
(1060, 'Pontevedra', 51, 1),
(1061, 'Pulupandan', 51, 1),
(1062, 'Salvador Benedicto', 51, 1),
(1063, 'San Enrique', 51, 1),
(1064, 'Toboso', 51, 1),
(1065, 'ValladoliCityId', 51, 1),
(1066, 'Bais City', 52, 1),
(1067, 'Bayawan City', 52, 1),
(1068, 'Canlaon City', 52, 1),
(1069, 'Guihulngan City', 52, 1),
(1070, 'Dumaguete City', 52, 1),
(1071, 'Tanjay City', 52, 1),
(1072, 'Amlan', 52, 1),
(1073, 'Ayungon', 52, 1),
(1074, 'Bacong', 52, 1),
(1075, 'Basay', 52, 1),
(1076, 'Bindoy', 52, 1),
(1077, 'Dauin', 52, 1),
(1078, 'Jimalalud', 52, 1),
(1079, 'La Libertad', 52, 1),
(1080, 'Mabinay', 52, 1),
(1081, 'Manjuyod', 52, 1),
(1082, 'Pamplona', 52, 1),
(1083, 'San Jose', 52, 1),
(1084, 'Santa Catalina', 52, 1),
(1085, 'Siaton', 52, 1),
(1086, 'Sibulan', 52, 1),
(1087, 'Tayasan', 52, 1),
(1088, 'Valencia', 52, 1),
(1089, 'Vallehermoso', 52, 1),
(1090, 'Zamboanguita', 52, 1),
(1091, 'Allen', 53, 1),
(1092, 'Biri', 53, 1),
(1093, 'Bobon', 53, 1),
(1094, 'Capul', 53, 1),
(1095, 'Catarman', 53, 1),
(1096, 'Catubig', 53, 1),
(1097, 'Gamay', 53, 1),
(1098, 'Laoang', 53, 1),
(1099, 'Lapinig', 53, 1),
(1100, 'Las Navas', 53, 1),
(1101, 'Lavezares', 53, 1),
(1102, 'Lope de Vega', 53, 1),
(1103, 'Mapanas', 53, 1),
(1104, 'Mondragon', 53, 1),
(1105, 'Palapag', 53, 1),
(1106, 'Pambujan', 53, 1),
(1107, 'Rosario', 53, 1),
(1108, 'San Antonio', 53, 1),
(1109, 'San IsiCityIdro', 53, 1),
(1110, 'San Jose', 53, 1),
(1111, 'San Roque', 53, 1),
(1112, 'San Vicente', 53, 1),
(1113, 'Silvino Lobos', 53, 1),
(1114, 'Victoria', 53, 1),
(1115, 'Cabanatuan City', 54, 1),
(1116, 'Gapan City', 54, 1),
(1117, 'Science City of Muñoz', 54, 1),
(1118, 'Palayan City', 54, 1),
(1119, 'San Jose City', 54, 1),
(1120, 'Aliaga', 54, 1),
(1121, 'Bongabon', 54, 1),
(1122, 'Cabiao', 54, 1),
(1123, 'Carranglan', 54, 1),
(1124, 'Cuyapo', 54, 1),
(1125, 'Gabaldon', 54, 1),
(1126, 'General Mamerto NativiCityIdad', 54, 1),
(1127, 'General Tinio', 54, 1),
(1128, 'Guimba', 54, 1),
(1129, 'Jaen', 54, 1),
(1130, 'Laur', 54, 1),
(1131, 'Licab', 54, 1),
(1132, 'Llanera', 54, 1),
(1133, 'Lupao', 54, 1),
(1134, 'Nampicuan', 54, 1),
(1135, 'Pantabangan', 54, 1),
(1136, 'Peñaranda', 54, 1),
(1137, 'Quezon', 54, 1),
(1138, 'Rizal', 54, 1),
(1139, 'San Antonio', 54, 1),
(1140, 'San IsiCityIdro', 54, 1),
(1141, 'San Leonardo', 54, 1),
(1142, 'Santa Rosa', 54, 1),
(1143, 'Santo Domingo', 54, 1),
(1144, 'Talavera', 54, 1),
(1145, 'Talugtug', 54, 1),
(1146, 'Zaragoza', 54, 1),
(1147, 'Alfonso Castaneda', 55, 1),
(1148, 'Ambaguio', 55, 1),
(1149, 'Aritao', 55, 1),
(1150, 'Bagabag', 55, 1),
(1151, 'Bambang', 55, 1),
(1152, 'Bayombong', 55, 1),
(1153, 'Diadi', 55, 1),
(1154, 'Dupax del Norte', 55, 1),
(1155, 'Dupax del Sur', 55, 1),
(1156, 'Kasibu', 55, 1),
(1157, 'Kayapa', 55, 1),
(1158, 'Quezon', 55, 1),
(1159, 'Santa Fe', 55, 1),
(1160, 'Solano', 55, 1),
(1161, 'Villaverde', 55, 1),
(1162, 'Abra de Ilog', 56, 1),
(1163, 'Calintaan', 56, 1),
(1164, 'Looc', 56, 1),
(1165, 'Lubang', 56, 1),
(1166, 'Magsaysay', 56, 1),
(1167, 'Mamburao', 56, 1),
(1168, 'Paluan', 56, 1),
(1169, 'Rizal', 56, 1),
(1170, 'Sablayan', 56, 1),
(1171, 'San Jose', 56, 1),
(1172, 'Santa Cruz', 56, 1),
(1173, 'Calapan City', 57, 1),
(1174, 'Baco', 57, 1),
(1175, 'Bansud', 57, 1),
(1176, 'Bongabong', 57, 1),
(1177, 'Bulalacao', 57, 1),
(1178, 'Gloria', 57, 1),
(1179, 'Mansalay', 57, 1),
(1180, 'Naujan', 57, 1),
(1181, 'Pinamalayan', 57, 1),
(1182, 'Pola', 57, 1),
(1183, 'Puerto Galera', 57, 1),
(1184, 'Roxas', 57, 1),
(1185, 'San Teodoro', 57, 1),
(1186, 'Socorro', 57, 1),
(1187, 'Victoria', 57, 1),
(1188, 'Puerto Princesa City', 58, 1),
(1189, 'Aborlan', 58, 1),
(1190, 'Agutaya', 58, 1),
(1191, 'Araceli', 58, 1),
(1192, 'Balabac', 58, 1),
(1193, 'Bataraza', 58, 1),
(1194, 'Brooke''s Point', 58, 1),
(1195, 'Busuanga', 58, 1),
(1196, 'Cagayancillo', 58, 1),
(1197, 'Coron', 58, 1),
(1198, 'Culion', 58, 1),
(1199, 'Cuyo', 58, 1),
(1200, 'Dumaran', 58, 1),
(1201, 'El NiCityIdo', 58, 1),
(1202, 'Kalayaan', 58, 1),
(1203, 'Linapacan', 58, 1),
(1204, 'Magsaysay', 58, 1),
(1205, 'Narra', 58, 1),
(1206, 'Quezon', 58, 1),
(1207, 'Rizal', 58, 1),
(1208, 'Roxas', 58, 1),
(1209, 'San Vicente', 58, 1),
(1210, 'Sofronio Española', 58, 1),
(1211, 'Taytay', 58, 1),
(1212, 'Angeles City', 59, 1),
(1213, 'City of San Fernando', 59, 1),
(1214, 'Apalit', 59, 1),
(1215, 'Arayat', 59, 1),
(1216, 'Bacolor', 59, 1),
(1217, 'Candaba', 59, 1),
(1218, 'FloriCityIdablanca', 59, 1),
(1219, 'Guagua', 59, 1),
(1220, 'Lubao', 59, 1),
(1221, 'Mabalacat', 59, 1),
(1222, 'Macabebe', 59, 1),
(1223, 'Magalang', 59, 1),
(1224, 'Masantol', 59, 1),
(1225, 'Mexico', 59, 1),
(1226, 'Minalin', 59, 1),
(1227, 'Porac', 59, 1),
(1228, 'San Luis', 59, 1),
(1229, 'San Simon', 59, 1),
(1230, 'Santa Ana', 59, 1),
(1231, 'Santa Rita', 59, 1),
(1232, 'Santo Tomas', 59, 1),
(1233, 'Sasmuan', 59, 1),
(1234, 'Alaminos City', 60, 1),
(1235, 'Dagupan City', 60, 1),
(1236, 'San Carlos City', 60, 1),
(1237, 'Urdaneta City', 60, 1),
(1238, 'Agno', 60, 1),
(1239, 'Aguilar', 60, 1),
(1240, 'Alcala', 60, 1),
(1241, 'Anda', 60, 1),
(1242, 'Asingan', 60, 1),
(1243, 'Balungao', 60, 1),
(1244, 'Bani', 60, 1),
(1245, 'Basista', 60, 1),
(1246, 'Bautista', 60, 1),
(1247, 'Bayambang', 60, 1),
(1248, 'Binalonan', 60, 1),
(1249, 'Binmaley', 60, 1),
(1250, 'Bolinao', 60, 1),
(1251, 'Bugallon', 60, 1),
(1252, 'Burgos', 60, 1),
(1253, 'Calasiao', 60, 1),
(1254, 'Dasol', 60, 1),
(1255, 'Infanta', 60, 1),
(1256, 'Labrador', 60, 1),
(1257, 'Laoac', 60, 1),
(1258, 'Lingayen', 60, 1),
(1259, 'Mabini', 60, 1),
(1260, 'Malasiqui', 60, 1),
(1261, 'Manaoag', 60, 1),
(1262, 'Mangaldan', 60, 1),
(1263, 'Mangatarem', 60, 1),
(1264, 'Mapandan', 60, 1),
(1265, 'NativiCityIdad', 60, 1),
(1266, 'Pozzorubio', 60, 1),
(1267, 'Rosales', 60, 1),
(1268, 'San Fabian', 60, 1),
(1269, 'San Jacinto', 60, 1),
(1270, 'San Manuel', 60, 1),
(1271, 'San Nicolas', 60, 1),
(1272, 'San Quintin', 60, 1),
(1273, 'Santa Barbara', 60, 1),
(1274, 'Santa Maria', 60, 1),
(1275, 'Santo Tomas', 60, 1),
(1276, 'Sison', 60, 1),
(1277, 'Sual', 60, 1),
(1278, 'Tayug', 60, 1),
(1279, 'Umingan', 60, 1),
(1280, 'Urbiztondo', 60, 1),
(1281, 'Villasis', 60, 1),
(1282, 'Lucena City', 61, 1),
(1283, 'Tayabas City', 61, 1),
(1284, 'Agdangan', 61, 1),
(1285, 'Alabat', 61, 1),
(1286, 'Atimonan', 61, 1),
(1287, 'Buenavista', 61, 1),
(1288, 'Burdeos', 61, 1),
(1289, 'Calauag', 61, 1),
(1290, 'Candelaria', 61, 1),
(1291, 'Catanauan', 61, 1),
(1292, 'Dolores', 61, 1),
(1293, 'General Luna', 61, 1),
(1294, 'General Nakar', 61, 1),
(1295, 'Guinayangan', 61, 1),
(1296, 'Gumaca', 61, 1),
(1297, 'Infanta', 61, 1),
(1298, 'Jomalig', 61, 1),
(1299, 'Lopez', 61, 1),
(1300, 'Lucban', 61, 1),
(1301, 'Macalelon', 61, 1),
(1302, 'Mauban', 61, 1),
(1303, 'Mulanay', 61, 1),
(1304, 'Padre Burgos', 61, 1),
(1305, 'Pagbilao', 61, 1),
(1306, 'Panukulan', 61, 1),
(1307, 'Patnanungan', 61, 1),
(1308, 'Perez', 61, 1),
(1309, 'Pitogo', 61, 1),
(1310, 'PlariCityIdel', 61, 1),
(1311, 'Polillo', 61, 1),
(1312, 'Quezon', 61, 1),
(1313, 'Real', 61, 1),
(1314, 'Sampaloc', 61, 1),
(1315, 'San Andres', 61, 1),
(1316, 'San Antonio', 61, 1),
(1317, 'San Francisco', 61, 1),
(1318, 'San Narciso', 61, 1),
(1319, 'Sariaya', 61, 1),
(1320, 'Tagkawayan', 61, 1),
(1321, 'Tiaong', 61, 1),
(1322, 'Unisan', 61, 1),
(1323, 'Aglipay', 62, 1),
(1324, 'Cabarroguis', 62, 1),
(1325, 'Diffun', 62, 1),
(1326, 'Maddela', 62, 1),
(1327, 'Nagtipunan', 62, 1),
(1328, 'Saguday', 62, 1),
(1329, 'Antipolo City', 63, 1),
(1330, 'Angono', 63, 1),
(1331, 'Baras', 63, 1),
(1332, 'Binangonan', 63, 1),
(1333, 'Cainta', 63, 1),
(1334, 'Cardona', 63, 1),
(1335, 'Jalajala', 63, 1),
(1336, 'Morong', 63, 1),
(1337, 'Pililla', 63, 1),
(1338, 'Rodriguez', 63, 1),
(1339, 'San Mateo', 63, 1),
(1340, 'Tanay', 63, 1),
(1341, 'Taytay', 63, 1),
(1342, 'Teresa', 63, 1),
(1343, 'Alcantara', 64, 1),
(1344, 'Banton', 64, 1),
(1345, 'CajiCityIdiocan', 64, 1),
(1346, 'Calatrava', 64, 1),
(1347, 'Concepcion', 64, 1),
(1348, 'Corcuera', 64, 1),
(1349, 'Ferrol', 64, 1),
(1350, 'Looc', 64, 1),
(1351, 'Magdiwang', 64, 1),
(1352, 'Odiongan', 64, 1),
(1353, 'Romblon', 64, 1),
(1354, 'San Agustin', 64, 1),
(1355, 'San Andres', 64, 1),
(1356, 'San Fernando', 64, 1),
(1357, 'San Jose', 64, 1),
(1358, 'Santa Fe', 64, 1),
(1359, 'Santa Maria', 64, 1),
(1360, 'Calbayog City', 65, 1),
(1361, 'Catbalogan City', 65, 1),
(1362, 'Almagro', 65, 1),
(1363, 'Basey', 65, 1),
(1364, 'Calbiga', 65, 1),
(1365, 'Daram', 65, 1),
(1366, 'Gandara', 65, 1),
(1367, 'Hinabangan', 65, 1),
(1368, 'Jiabong', 65, 1),
(1369, 'Marabut', 65, 1),
(1370, 'Matuguinao', 65, 1),
(1371, 'Motiong', 65, 1),
(1372, 'Pagsanghan', 65, 1),
(1373, 'Paranas', 65, 1),
(1374, 'Pinabacdao', 65, 1),
(1375, 'San Jorge', 65, 1),
(1376, 'San Jose De Buan', 65, 1),
(1377, 'San Sebastian', 65, 1),
(1378, 'Santa Margarita', 65, 1),
(1379, 'Santa Rita', 65, 1),
(1380, 'Santo Niño', 65, 1),
(1381, 'Tagapul-an', 65, 1),
(1382, 'Talalora', 65, 1),
(1383, 'Tarangnan', 65, 1),
(1384, 'Villareal', 65, 1),
(1385, 'Zumarraga', 65, 1),
(1386, 'Alabel', 66, 1),
(1387, 'Glan', 66, 1),
(1388, 'Kiamba', 66, 1),
(1389, 'Maasim', 66, 1),
(1390, 'Maitum', 66, 1),
(1391, 'Malapatan', 66, 1),
(1392, 'Malungon', 66, 1),
(1393, 'Enrique Villanueva', 67, 1),
(1394, 'Larena', 67, 1),
(1395, 'Lazi', 67, 1),
(1396, 'Maria', 67, 1),
(1397, 'San Juan', 67, 1),
(1398, 'Siquijor', 67, 1),
(1399, 'Sorsogon City', 68, 1),
(1400, 'Barcelona', 68, 1),
(1401, 'Bulan', 68, 1),
(1402, 'Bulusan', 68, 1),
(1403, 'Casiguran', 68, 1),
(1404, 'Castilla', 68, 1),
(1405, 'Donsol', 68, 1),
(1406, 'Gubat', 68, 1),
(1407, 'Irosin', 68, 1),
(1408, 'Juban', 68, 1),
(1409, 'Magallanes', 68, 1),
(1410, 'Matnog', 68, 1),
(1411, 'Pilar', 68, 1),
(1412, 'Prieto Diaz', 68, 1),
(1413, 'Santa Magdalena', 68, 1),
(1414, 'General Santos City', 69, 1),
(1415, 'Koronadal City', 69, 1),
(1416, 'Banga', 69, 1),
(1417, 'Lake Sebu', 69, 1),
(1418, 'Norala', 69, 1),
(1419, 'Polomolok', 69, 1),
(1420, 'Santo Niño', 69, 1),
(1421, 'Surallah', 69, 1),
(1422, 'T''boli', 69, 1),
(1423, 'Tampakan', 69, 1),
(1424, 'Tantangan', 69, 1),
(1425, 'Tupi', 69, 1),
(1426, 'Maasin City', 70, 1),
(1427, 'Anahawan', 70, 1),
(1428, 'Bontoc', 70, 1),
(1429, 'Hinunangan', 70, 1),
(1430, 'Hinundayan', 70, 1),
(1431, 'Libagon', 70, 1),
(1432, 'Liloan', 70, 1),
(1433, 'Limasawa', 70, 1),
(1434, 'Macrohon', 70, 1),
(1435, 'Malitbog', 70, 1),
(1436, 'Padre Burgos', 70, 1),
(1437, 'Pintuyan', 70, 1),
(1438, 'Saint Bernard', 70, 1),
(1439, 'San Francisco', 70, 1),
(1440, 'San Juan', 70, 1),
(1441, 'San Ricardo', 70, 1),
(1442, 'Silago', 70, 1),
(1443, 'Sogod', 70, 1),
(1444, 'Tomas Oppus', 70, 1),
(1445, 'Tacurong City', 71, 1),
(1446, 'Bagumbayan', 71, 1),
(1447, 'Columbio', 71, 1),
(1448, 'Esperanza', 71, 1),
(1449, 'Isulan', 71, 1),
(1450, 'Kalamansig', 71, 1),
(1451, 'Lambayong', 71, 1),
(1452, 'Lebak', 71, 1),
(1453, 'Lutayan', 71, 1),
(1454, 'Palimbang', 71, 1),
(1455, 'PresiCityIdent Quirino', 71, 1),
(1456, 'Senator Ninoy Aquino', 71, 1),
(1457, 'Banguingui', 72, 1),
(1458, 'Hadji Panglima Tahil', 72, 1),
(1459, 'Indanan', 72, 1),
(1460, 'Jolo', 72, 1),
(1461, 'Kalingalan Caluang', 72, 1),
(1462, 'Lugus', 72, 1),
(1463, 'Luuk', 72, 1),
(1464, 'Maimbung', 72, 1),
(1465, 'Old Panamao', 72, 1),
(1466, 'Omar', 72, 1),
(1467, 'Pandami', 72, 1),
(1468, 'Panglima Estino', 72, 1),
(1469, 'Pangutaran', 72, 1),
(1470, 'Parang', 72, 1),
(1471, 'Pata', 72, 1),
(1472, 'Patikul', 72, 1),
(1473, 'Siasi', 72, 1),
(1474, 'Talipao', 72, 1),
(1475, 'Tapul', 72, 1),
(1476, 'Surigao City', 73, 1),
(1477, 'Alegria', 73, 1),
(1478, 'Bacuag', 73, 1),
(1479, 'Basilisa', 73, 1),
(1480, 'Burgos', 73, 1),
(1481, 'Cagdianao', 73, 1),
(1482, 'Claver', 73, 1),
(1483, 'Dapa', 73, 1),
(1484, 'Del Carmen', 73, 1),
(1485, 'Dinagat', 73, 1),
(1486, 'General Luna', 73, 1),
(1487, 'Gigaquit', 73, 1),
(1488, 'Libjo', 73, 1),
(1489, 'Loreto', 73, 1),
(1490, 'Mainit', 73, 1),
(1491, 'Malimono', 73, 1),
(1492, 'Pilar', 73, 1),
(1493, 'Placer', 73, 1),
(1494, 'San Benito', 73, 1),
(1495, 'San Francisco', 73, 1),
(1496, 'San IsiCityIdro', 73, 1),
(1497, 'San Jose', 73, 1),
(1498, 'Santa Monica', 73, 1),
(1499, 'Sison', 73, 1),
(1500, 'Socorro', 73, 1),
(1501, 'Tagana-an', 73, 1),
(1502, 'Tubajon', 73, 1),
(1503, 'Tubod', 73, 1),
(1504, 'Bislig City', 74, 1),
(1505, 'Tandag City', 74, 1),
(1506, 'Barobo', 74, 1),
(1507, 'Bayabas', 74, 1),
(1508, 'Cagwait', 74, 1),
(1509, 'Cantilan', 74, 1),
(1510, 'Carmen', 74, 1),
(1511, 'Carrascal', 74, 1),
(1512, 'Cortes', 74, 1),
(1513, 'Hinatuan', 74, 1),
(1514, 'Lanuza', 74, 1),
(1515, 'Lianga', 74, 1),
(1516, 'Lingig', 74, 1),
(1517, 'MadriCityId', 74, 1),
(1518, 'Marihatag', 74, 1),
(1519, 'San Agustin', 74, 1),
(1520, 'San Miguel', 74, 1),
(1521, 'Tagbina', 74, 1),
(1522, 'Tago', 74, 1),
(1523, 'Tarlac City', 75, 1),
(1524, 'Anao', 75, 1),
(1525, 'Bamban', 75, 1),
(1526, 'Camiling', 75, 1),
(1527, 'Capas', 75, 1),
(1528, 'Concepcion', 75, 1),
(1529, 'Gerona', 75, 1),
(1530, 'La Paz', 75, 1),
(1531, 'Mayantoc', 75, 1),
(1532, 'Moncada', 75, 1),
(1533, 'Paniqui', 75, 1),
(1534, 'Pura', 75, 1),
(1535, 'Ramos', 75, 1),
(1536, 'San Clemente', 75, 1),
(1537, 'San Jose', 75, 1),
(1538, 'San Manuel', 75, 1),
(1539, 'Santa Ignacia', 75, 1),
(1540, 'Victoria', 75, 1),
(1541, 'Bongao', 76, 1),
(1542, 'Languyan', 76, 1),
(1543, 'Mapun', 76, 1),
(1544, 'Panglima Sugala', 76, 1),
(1545, 'Sapa-Sapa', 76, 1),
(1546, 'Sibutu', 76, 1),
(1547, 'Simunul', 76, 1),
(1548, 'Sitangkai', 76, 1),
(1549, 'South Ubian', 76, 1),
(1550, 'Tandubas', 76, 1),
(1551, 'Turtle Islands', 76, 1),
(1552, 'Olongapo City', 77, 1),
(1553, 'Botolan', 77, 1),
(1554, 'Cabangan', 77, 1),
(1555, 'Candelaria', 77, 1),
(1556, 'Castillejos', 77, 1),
(1557, 'Iba', 77, 1),
(1558, 'Masinloc', 77, 1),
(1559, 'Palauig', 77, 1),
(1560, 'San Antonio', 77, 1),
(1561, 'San Felipe', 77, 1),
(1562, 'San Marcelino', 77, 1),
(1563, 'San Narciso', 77, 1),
(1564, 'Santa Cruz', 77, 1),
(1565, 'Subic', 77, 1),
(1566, 'Dapitan City', 78, 1),
(1567, 'Dipolog City', 78, 1),
(1568, 'Bacungan', 78, 1),
(1569, 'Baliguian', 78, 1),
(1570, 'Godod', 78, 1),
(1571, 'Gutalac', 78, 1),
(1572, 'Jose Dalman', 78, 1),
(1573, 'Kalawit', 78, 1),
(1574, 'Katipunan', 78, 1),
(1575, 'La Libertad', 78, 1),
(1576, 'Labason', 78, 1),
(1577, 'Liloy', 78, 1),
(1578, 'Manukan', 78, 1),
(1579, 'Mutia', 78, 1),
(1580, 'Piñan', 78, 1),
(1581, 'Polanco', 78, 1),
(1582, 'PresiCityIdent Manuel A. Roxas', 78, 1),
(1583, 'Rizal', 78, 1),
(1584, 'Salug', 78, 1),
(1585, 'Sergio Osmeña Sr.', 78, 1),
(1586, 'Siayan', 78, 1),
(1587, 'Sibuco', 78, 1),
(1588, 'Sibutad', 78, 1),
(1589, 'Sindangan', 78, 1),
(1590, 'Siocon', 78, 1),
(1591, 'Sirawai', 78, 1),
(1592, 'Tampilisan', 78, 1),
(1593, 'Pagadian City', 79, 1),
(1594, 'Zamboanga City', 79, 1),
(1595, 'Aurora', 79, 1),
(1596, 'Bayog', 79, 1),
(1597, 'Dimataling', 79, 1),
(1598, 'Dinas', 79, 1),
(1599, 'Dumalinao', 79, 1),
(1600, 'Dumingag', 79, 1),
(1601, 'Guipos', 79, 1),
(1602, 'Josefina', 79, 1),
(1603, 'Kumalarang', 79, 1),
(1604, 'Labangan', 79, 1),
(1605, 'Lakewood', 79, 1),
(1606, 'Lapuyan', 79, 1),
(1607, 'Mahayag', 79, 1),
(1608, 'Margosatubig', 79, 1),
(1609, 'MiCityIdsalip', 79, 1),
(1610, 'Molave', 79, 1),
(1611, 'Pitogo', 79, 1),
(1612, 'Ramon Magsaysay', 79, 1),
(1613, 'San Miguel', 79, 1),
(1614, 'San Pablo', 79, 1),
(1615, 'Sominot', 79, 1),
(1616, 'Tabina', 79, 1),
(1617, 'Tambulig', 79, 1),
(1618, 'Tigbao', 79, 1),
(1619, 'Tukuran', 79, 1),
(1620, 'Vincenzo A. Sagun', 79, 1),
(1621, 'Alicia', 80, 1),
(1622, 'Buug', 80, 1),
(1623, 'Diplahan', 80, 1),
(1624, 'Imelda', 80, 1),
(1625, 'Ipil', 80, 1),
(1626, 'Kabasalan', 80, 1),
(1627, 'Mabuhay', 80, 1),
(1628, 'Malangas', 80, 1),
(1629, 'Naga', 80, 1),
(1630, 'Olutanga', 80, 1),
(1631, 'Payao', 80, 1),
(1632, 'Roseller Lim', 80, 1),
(1633, 'Siay', 80, 1),
(1634, 'Talusan', 80, 1),
(1635, 'Titay', 80, 1),
(1636, 'Tungawan', 80, 1);

-- --------------------------------------------------------

--
-- Table structure for table `clinics`
--

CREATE TABLE IF NOT EXISTS `clinics` (
`iClinicId` int(1) unsigned NOT NULL,
  `iFloorNumber` tinyint(1) unsigned NOT NULL,
  `sRoomNumber` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sBuildingName` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `bActive` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `iDateCreated` int(1) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `clinics`
--

INSERT INTO `clinics` (`iClinicId`, `iFloorNumber`, `sRoomNumber`, `sBuildingName`, `bActive`, `iDateCreated`) VALUES
(1, 2, '4', 'ABC', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `doctors`
--

CREATE TABLE IF NOT EXISTS `doctors` (
`iDoctorId` int(1) unsigned NOT NULL,
  `sLastName` varchar(64) NOT NULL,
  `sFirstName` varchar(64) NOT NULL,
  `sMiddleName` varchar(64) DEFAULT NULL,
  `iGenderId` tinyint(1) unsigned NOT NULL,
  `sContactNumber` varchar(11) DEFAULT NULL,
  `iCivilStatusId` tinyint(1) unsigned NOT NULL,
  `bActive` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `sPhoto` varchar(64) DEFAULT NULL,
  `iDateCreated` int(1) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctors`
--

INSERT INTO `doctors` (`iDoctorId`, `sLastName`, `sFirstName`, `sMiddleName`, `iGenderId`, `sContactNumber`, `iCivilStatusId`, `bActive`, `sPhoto`, `iDateCreated`) VALUES
(1, 'Dimayuga', 'Armando', '', 1, '703-1251', 6, 1, NULL, 0),
(2, 'Dimayuga', 'Divinia', '', 2, '706-0363', 6, 1, NULL, 0),
(3, 'Ilagan', 'Jhonald', '', 1, '706-7410', 6, 1, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `doctors_clinic`
--

CREATE TABLE IF NOT EXISTS `doctors_clinic` (
  `iDoctorId` int(1) unsigned NOT NULL,
  `iClinicId` int(1) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `doctors_clinic`
--

INSERT INTO `doctors_clinic` (`iDoctorId`, `iClinicId`) VALUES
(1, 1),
(2, 1),
(3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `doctors_schedules`
--

CREATE TABLE IF NOT EXISTS `doctors_schedules` (
`iDoctorScheduleId` int(1) unsigned NOT NULL,
  `iDoctorId` int(1) unsigned NOT NULL,
  `sDay` varchar(10) NOT NULL,
  `iDateStart` int(1) unsigned NOT NULL,
  `iDateEnd` int(1) unsigned NOT NULL,
  `iPatients` tinyint(1) unsigned NOT NULL,
  `bActive` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `iDateCreated` int(1) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctors_schedules`
--

INSERT INTO `doctors_schedules` (`iDoctorScheduleId`, `iDoctorId`, `sDay`, `iDateStart`, `iDateEnd`, `iPatients`, `bActive`, `iDateCreated`) VALUES
(1, 1, 'Monday', 1473645600, 1473647400, 4, 1, 1473672909),
(2, 1, 'Monday', 1473647400, 1473649200, 4, 1, 1473672909),
(3, 1, 'Monday', 1473651000, 1473652800, 4, 1, 1473672909);

-- --------------------------------------------------------

--
-- Table structure for table `metas`
--

CREATE TABLE IF NOT EXISTS `metas` (
`iMetaId` tinyint(1) unsigned NOT NULL,
  `iMetaGroupId` tinyint(1) unsigned NOT NULL,
  `sCode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `sName` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bActive` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `iDateCreated` int(1) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `metas`
--

INSERT INTO `metas` (`iMetaId`, `iMetaGroupId`, `sCode`, `sName`, `bActive`, `iDateCreated`) VALUES
(1, 1, 'M', 'Male', 1, 1471186343),
(2, 1, 'F', 'Female', 1, 1471186351),
(3, 2, 'Single', 'Single', 1, 1471186361),
(4, 2, 'Widowed', 'Widowed', 1, 1471186380),
(5, 2, 'Separated', 'Separated', 1, 1471186391),
(6, 2, 'Married', 'Married', 1, 1472752883);

-- --------------------------------------------------------

--
-- Table structure for table `meta_groups`
--

CREATE TABLE IF NOT EXISTS `meta_groups` (
`iMetaGroupId` tinyint(1) unsigned NOT NULL,
  `sCode` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `sName` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `iDateCreated` int(1) unsigned NOT NULL,
  `bActive` tinyint(1) unsigned NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `meta_groups`
--

INSERT INTO `meta_groups` (`iMetaGroupId`, `sCode`, `sName`, `iDateCreated`, `bActive`) VALUES
(1, 'Gender', 'Gender', 1471186321, 1),
(2, 'CivilStatus', 'Civil Status', 1471186330, 1);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
`iModuleId` int(1) unsigned NOT NULL,
  `sModuleName` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `sTitle` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `bActive` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `bOnload` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `iDateCreated` int(1) unsigned NOT NULL,
  `sTableName` varchar(32) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`iModuleId`, `sModuleName`, `sTitle`, `bActive`, `bOnload`, `iDateCreated`, `sTableName`) VALUES
(1, 'Modules', 'Modules', 1, 0, 1471185733, 'modules'),
(2, 'Permissions', 'Permissions', 1, 0, 1471185774, 'permissions'),
(3, 'Roles', 'Roles', 1, 0, 1471185820, 'roles'),
(4, 'Users', 'Users', 1, 0, 1471185859, 'users'),
(5, 'Dashboard', 'Dashboard', 1, 1, 1471185927, 'transactions'),
(6, 'AppointmentTypes', 'Appointment Types', 1, 0, 1471185955, 'appointment_types'),
(7, 'Clinics', 'Clinic/Rooms', 1, 0, 1471185979, 'clinics'),
(8, 'Doctors', 'Doctors', 1, 0, 1471185994, 'doctors'),
(9, 'Metas', 'Metas', 1, 0, 1471186013, 'metas'),
(10, 'MetaGroups', 'Metas Groups', 1, 0, 1471186031, 'meta_groups');

-- --------------------------------------------------------

--
-- Table structure for table `module_permissions`
--

CREATE TABLE IF NOT EXISTS `module_permissions` (
  `iModuleId` int(1) unsigned NOT NULL,
  `iPermissionId` int(1) unsigned NOT NULL,
  `iBitIndex` tinyint(1) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `module_permissions`
--

INSERT INTO `module_permissions` (`iModuleId`, `iPermissionId`, `iBitIndex`) VALUES
(2, 1, 0),
(2, 2, 1),
(2, 3, 2),
(2, 4, 3),
(3, 1, 0),
(3, 2, 1),
(3, 3, 2),
(3, 4, 3),
(4, 1, 0),
(4, 2, 1),
(4, 3, 2),
(4, 4, 3),
(5, 1, 0),
(5, 2, 1),
(5, 3, 2),
(5, 4, 3),
(5, 5, 4),
(7, 1, 0),
(7, 2, 1),
(7, 3, 2),
(7, 4, 3),
(8, 1, 0),
(8, 2, 1),
(8, 3, 2),
(8, 4, 3),
(9, 1, 0),
(9, 2, 1),
(9, 3, 2),
(9, 4, 3),
(10, 1, 0),
(10, 2, 1),
(10, 3, 2),
(10, 4, 3);

-- --------------------------------------------------------

--
-- Table structure for table `patients`
--

CREATE TABLE IF NOT EXISTS `patients` (
`iPatientId` int(1) unsigned NOT NULL,
  `sLastName` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `sFirstName` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `sMIddleName` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iGenderId` tinyint(1) unsigned NOT NULL,
  `iCIvilStatusId` tinyint(1) unsigned NOT NULL,
  `sContactNumber` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `sAddress` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `iCityId` int(1) unsigned NOT NULL,
  `iProvinceId` int(1) unsigned NOT NULL,
  `iDateCreated` int(1) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `patients`
--

INSERT INTO `patients` (`iPatientId`, `sLastName`, `sFirstName`, `sMIddleName`, `iGenderId`, `iCIvilStatusId`, `sContactNumber`, `sAddress`, `iCityId`, `iProvinceId`, `iDateCreated`) VALUES
(26, 'Manahan', 'Liezl', 'Cortez', 2, 3, '09167217398', 'Longos', 279, 17, 1473852034),
(27, 'sample', 'sample', 'sample', 1, 4, '1111', 'wqweq', 28, 2, 1473859121);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
`iPermissionId` int(1) unsigned NOT NULL,
  `sPermissionName` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `bActive` tinyint(1) unsigned NOT NULL,
  `iDateCreated` int(1) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`iPermissionId`, `sPermissionName`, `bActive`, `iDateCreated`) VALUES
(1, 'Create', 1, 1471185789),
(2, 'Read', 1, 1471185794),
(3, 'Update', 1, 1471185799),
(4, 'Delete', 1, 1471185803),
(5, 'Notify', 1, 1473659452);

-- --------------------------------------------------------

--
-- Table structure for table `provinces`
--

CREATE TABLE IF NOT EXISTS `provinces` (
`iProvinceId` int(1) unsigned NOT NULL,
  `sProvince` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `bActive` tinyint(1) unsigned NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `provinces`
--

INSERT INTO `provinces` (`iProvinceId`, `sProvince`, `bActive`) VALUES
(1, 'Abra', 1),
(2, 'Agusan del Norte', 1),
(3, 'Agusan del Sur', 1),
(4, 'Aklan', 1),
(5, 'Albay', 1),
(6, 'Antique', 1),
(7, 'Apayao', 1),
(8, 'Aurora', 1),
(9, 'Basilan', 1),
(10, 'Bataan', 1),
(11, 'Batanes', 1),
(12, 'Batangas', 1),
(13, 'Benguet', 1),
(14, 'Biliran', 1),
(15, 'Bohol', 1),
(16, 'BukiCityIdnon', 1),
(17, 'Bulacan', 1),
(18, 'Cagayan', 1),
(19, 'Camarines Norte', 1),
(20, 'Camarines Sur', 1),
(21, 'Camiguin', 1),
(22, 'Capiz', 1),
(23, 'Catanduanes', 1),
(24, 'Cavite', 1),
(25, 'Cebu', 1),
(26, 'Compostela Valley', 1),
(27, 'Cotabato', 1),
(28, 'Davao del Norte', 1),
(29, 'Davao del Sur', 1),
(30, 'Davao Oriental', 1),
(31, 'Eastern Samar', 1),
(32, 'Guimaras', 1),
(33, 'Ifugao', 1),
(34, 'Ilocos Norte', 1),
(35, 'Ilocos Sur', 1),
(36, 'Iloilo', 1),
(37, 'Isabela', 1),
(38, 'Kalinga', 1),
(39, 'La Union', 1),
(40, 'Laguna', 1),
(41, 'Lanao del Norte', 1),
(42, 'Lanao del Sur', 1),
(43, 'Leyte', 1),
(44, 'Maguindanao', 1),
(45, 'Marinduque', 1),
(46, 'Masbate', 1),
(47, 'Metro Manila', 1),
(48, 'Misamis OcciCityIdental', 1),
(49, 'Misamis Oriental', 1),
(50, 'Mountain Province', 1),
(51, 'Negros OcciCityIdental', 1),
(52, 'Negros Oriental', 1),
(53, 'Northern Samar', 1),
(54, 'Nueva Ecija', 1),
(55, 'Nueva Vizcaya', 1),
(56, 'OcciCityIdental Mindoro', 1),
(57, 'Oriental Mindoro', 1),
(58, 'Palawan', 1),
(59, 'Pampanga', 1),
(60, 'Pangasinan', 1),
(61, 'Quezon', 1),
(62, 'Quirino', 1),
(63, 'Rizal', 1),
(64, 'Romblon', 1),
(65, 'Samar', 1),
(66, 'Sarangani', 1),
(67, 'Siquijor', 1),
(68, 'Sorsogon', 1),
(69, 'South Cotabato', 1),
(70, 'Southern Leyte', 1),
(71, 'Sultan Kudarat', 1),
(72, 'Sulu', 1),
(73, 'Surigao del Norte', 1),
(74, 'Surigao del Sur', 1),
(75, 'Tarlac', 1),
(76, 'Tawi-Tawi', 1),
(77, 'Zambales', 1),
(78, 'Zamboanga del Norte', 1),
(79, 'Zamboanga del Sur', 1),
(80, 'Zamboanga Sibugay', 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
`iRoleId` int(1) unsigned NOT NULL,
  `sRoleName` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `bActive` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `iDateCreated` int(1) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`iRoleId`, `sRoleName`, `bActive`, `iDateCreated`) VALUES
(1, 'Administrator', 1, 1471185872),
(2, 'Nurse', 1, 1471185880);

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

CREATE TABLE IF NOT EXISTS `role_permissions` (
  `iRoleId` int(1) unsigned NOT NULL,
  `iModuleId` int(1) unsigned NOT NULL,
  `iPermission` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_permissions`
--

INSERT INTO `role_permissions` (`iRoleId`, `iModuleId`, `iPermission`) VALUES
(1, 1, 31),
(1, 2, 31),
(1, 3, 31),
(1, 4, 31),
(1, 5, 31),
(2, 5, 15),
(1, 6, 31),
(1, 7, 31),
(1, 8, 31),
(2, 8, 15),
(1, 9, 31),
(1, 10, 31);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
`iTransactionId` int(1) unsigned NOT NULL,
  `sTransactionCode` varchar(7) DEFAULT NULL,
  `iAppointmentTypeId` int(1) unsigned NOT NULL,
  `iPatientId` int(1) unsigned NOT NULL,
  `iDoctorId` int(1) unsigned NOT NULL,
  `sDay` varchar(10) DEFAULT NULL,
  `itime` int(1) unsigned NOT NULL,
  `bStatus` tinyint(1) DEFAULT NULL,
  `sRemarks` varchar(100) DEFAULT NULL,
  `iUserId` int(1) unsigned DEFAULT NULL,
  `bActive` tinyint(1) unsigned NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_statuses`
--

CREATE TABLE IF NOT EXISTS `transaction_statuses` (
  `iTransactionStatusId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`iUserId` int(1) unsigned NOT NULL,
  `sUsername` varchar(45) NOT NULL,
  `sPassword` varchar(45) NOT NULL,
  `sId` varchar(45) NOT NULL,
  `sFirstName` varchar(64) NOT NULL,
  `sLastName` varchar(64) NOT NULL,
  `sMiddleName` varchar(64) DEFAULT NULL,
  `iRoleId` int(1) unsigned NOT NULL,
  `bActive` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `iDoctorId` int(1) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`iUserId`, `sUsername`, `sPassword`, `sId`, `sFirstName`, `sLastName`, `sMiddleName`, `iRoleId`, `bActive`, `iDoctorId`) VALUES
(2, 'JohnDoe', '161ebd7d45089b3446ee4e0d86dbcf92', '001', 'John', 'Doe', 'M', 1, 1, 1),
(3, 'JohnWhick', '161ebd7d45089b3446ee4e0d86dbcf92', '002', 'John', 'Whick', 'M', 2, 1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointment_schedules`
--
ALTER TABLE `appointment_schedules`
 ADD PRIMARY KEY (`iAppointmentScheduleId`);

--
-- Indexes for table `appointment_types`
--
ALTER TABLE `appointment_types`
 ADD PRIMARY KEY (`iAppointmentTypeId`), ADD UNIQUE KEY `sCode_UNIQUE` (`sCode`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
 ADD PRIMARY KEY (`iCityId`);

--
-- Indexes for table `clinics`
--
ALTER TABLE `clinics`
 ADD PRIMARY KEY (`iClinicId`);

--
-- Indexes for table `doctors`
--
ALTER TABLE `doctors`
 ADD PRIMARY KEY (`iDoctorId`), ADD KEY `Doctors_iGenderId_idx` (`iGenderId`), ADD KEY `Doctors_iCivilStatusId_idx` (`iCivilStatusId`);

--
-- Indexes for table `doctors_clinic`
--
ALTER TABLE `doctors_clinic`
 ADD PRIMARY KEY (`iDoctorId`,`iClinicId`), ADD KEY `DoctorsClinic_iClinicId_idx` (`iClinicId`);

--
-- Indexes for table `doctors_schedules`
--
ALTER TABLE `doctors_schedules`
 ADD PRIMARY KEY (`iDoctorScheduleId`), ADD KEY `DoctorSchedules_iDoctorId_idx` (`iDoctorId`);

--
-- Indexes for table `metas`
--
ALTER TABLE `metas`
 ADD PRIMARY KEY (`iMetaId`), ADD UNIQUE KEY `sCode_UNIQUE` (`sCode`), ADD KEY `Metas_iGroupId_idx` (`iMetaGroupId`);

--
-- Indexes for table `meta_groups`
--
ALTER TABLE `meta_groups`
 ADD PRIMARY KEY (`iMetaGroupId`), ADD UNIQUE KEY `sCode_UNIQUE` (`sCode`), ADD UNIQUE KEY `sMetaName_UNIQUE` (`sName`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
 ADD PRIMARY KEY (`iModuleId`), ADD UNIQUE KEY `sModuleName_UNIQUE` (`sModuleName`), ADD UNIQUE KEY `sTableName_UNIQUE` (`sTableName`);

--
-- Indexes for table `module_permissions`
--
ALTER TABLE `module_permissions`
 ADD PRIMARY KEY (`iModuleId`,`iPermissionId`), ADD KEY `ModulePermissions_iPermissionId_idx` (`iPermissionId`);

--
-- Indexes for table `patients`
--
ALTER TABLE `patients`
 ADD PRIMARY KEY (`iPatientId`), ADD KEY `Patients_iCivilStatusId_idx` (`iCIvilStatusId`), ADD KEY `Patients_iGenderId_idx` (`iGenderId`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
 ADD PRIMARY KEY (`iPermissionId`), ADD UNIQUE KEY `sPermissionName_UNIQUE` (`sPermissionName`);

--
-- Indexes for table `provinces`
--
ALTER TABLE `provinces`
 ADD PRIMARY KEY (`iProvinceId`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
 ADD PRIMARY KEY (`iRoleId`), ADD UNIQUE KEY `sName_UNIQUE` (`sRoleName`);

--
-- Indexes for table `role_permissions`
--
ALTER TABLE `role_permissions`
 ADD PRIMARY KEY (`iModuleId`,`iRoleId`), ADD KEY `RolePermissions_iRoleId_idx` (`iRoleId`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
 ADD PRIMARY KEY (`iTransactionId`), ADD KEY `Transactions_iPatientId_idx` (`iPatientId`), ADD KEY `Transactions_iTransactionTypeId_idx` (`iAppointmentTypeId`), ADD KEY `Transactions_iDoctorId_idx` (`iDoctorId`), ADD KEY `Transactions_iUserId_idx` (`iUserId`);

--
-- Indexes for table `transaction_statuses`
--
ALTER TABLE `transaction_statuses`
 ADD PRIMARY KEY (`iTransactionStatusId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`iUserId`), ADD UNIQUE KEY `sId_UNIQUE` (`sId`), ADD UNIQUE KEY `sUsername_UNIQUE` (`sUsername`), ADD KEY `iRoleId` (`iRoleId`), ADD KEY `Users_iDoctorId_idx` (`iDoctorId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointment_types`
--
ALTER TABLE `appointment_types`
MODIFY `iAppointmentTypeId` int(1) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
MODIFY `iCityId` int(1) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1637;
--
-- AUTO_INCREMENT for table `clinics`
--
ALTER TABLE `clinics`
MODIFY `iClinicId` int(1) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `doctors`
--
ALTER TABLE `doctors`
MODIFY `iDoctorId` int(1) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `doctors_schedules`
--
ALTER TABLE `doctors_schedules`
MODIFY `iDoctorScheduleId` int(1) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `metas`
--
ALTER TABLE `metas`
MODIFY `iMetaId` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `meta_groups`
--
ALTER TABLE `meta_groups`
MODIFY `iMetaGroupId` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
MODIFY `iModuleId` int(1) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `patients`
--
ALTER TABLE `patients`
MODIFY `iPatientId` int(1) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
MODIFY `iPermissionId` int(1) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `provinces`
--
ALTER TABLE `provinces`
MODIFY `iProvinceId` int(1) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
MODIFY `iRoleId` int(1) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
MODIFY `iTransactionId` int(1) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `iUserId` int(1) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `doctors`
--
ALTER TABLE `doctors`
ADD CONSTRAINT `Doctors_iCivilStatusId` FOREIGN KEY (`iCivilStatusId`) REFERENCES `metas` (`iMetaId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `Doctors_iGenderId` FOREIGN KEY (`iGenderId`) REFERENCES `metas` (`iMetaId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `doctors_clinic`
--
ALTER TABLE `doctors_clinic`
ADD CONSTRAINT `DoctorsClinic_iClinicId` FOREIGN KEY (`iClinicId`) REFERENCES `clinics` (`iClinicId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `DoctorsClinic_iDoctorid` FOREIGN KEY (`iDoctorId`) REFERENCES `doctors` (`iDoctorId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `doctors_schedules`
--
ALTER TABLE `doctors_schedules`
ADD CONSTRAINT `DoctorSchedules_iDoctorId` FOREIGN KEY (`iDoctorId`) REFERENCES `doctors` (`iDoctorId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `metas`
--
ALTER TABLE `metas`
ADD CONSTRAINT `Metas_iGroupId` FOREIGN KEY (`iMetaGroupId`) REFERENCES `meta_groups` (`iMetaGroupId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `module_permissions`
--
ALTER TABLE `module_permissions`
ADD CONSTRAINT `ModulePermissions_iModuleId` FOREIGN KEY (`iModuleId`) REFERENCES `modules` (`iModuleId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `ModulePermissions_iPermissionId` FOREIGN KEY (`iPermissionId`) REFERENCES `permissions` (`iPermissionId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `patients`
--
ALTER TABLE `patients`
ADD CONSTRAINT `Patients_iCivilStatusId` FOREIGN KEY (`iCIvilStatusId`) REFERENCES `metas` (`iMetaId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `Patients_iGenderId` FOREIGN KEY (`iGenderId`) REFERENCES `metas` (`iMetaId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `role_permissions`
--
ALTER TABLE `role_permissions`
ADD CONSTRAINT `RolePermissins_iModuleId` FOREIGN KEY (`iModuleId`) REFERENCES `modules` (`iModuleId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `RolePermissions_iRoleId` FOREIGN KEY (`iRoleId`) REFERENCES `roles` (`iRoleId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
ADD CONSTRAINT `Transactions_iDoctorId` FOREIGN KEY (`iDoctorId`) REFERENCES `doctors` (`iDoctorId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `Transactions_iPatientId` FOREIGN KEY (`iPatientId`) REFERENCES `patients` (`iPatientId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `Transactions_iTransactionTypeId` FOREIGN KEY (`iAppointmentTypeId`) REFERENCES `appointment_types` (`iAppointmentTypeId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `Transactions_iUserId` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
ADD CONSTRAINT `Users_iDoctorId` FOREIGN KEY (`iDoctorId`) REFERENCES `doctors` (`iDoctorId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `Users_iRoleId` FOREIGN KEY (`iRoleId`) REFERENCES `roles` (`iRoleId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
