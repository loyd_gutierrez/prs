<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller
{
	protected $aErrorCodes = array(
			1001 => 'Invalid Request',
			1002 => 'Method is Empty',
			1003 => 'Requested module does not exist',
			1004 => 'Username and password did not match',
			1005 => 'Session Expired. Please log in again',
			1006 => 'Access Denied'
		);

	function __construct(){
		parent::__construct();
	}

	function _remap()
	{
		if(($aData = $this->getRequest()) && isset($aData["iErr"])) return $this->response($aData);
		$this->response($this->process( $aData ));
	}

	/**
	 * Start of the process; loads the model then process what is requested
	 * @param  [array] $aData : method to be process
	 * @return [array]        : error or requested data 
	 */
	function process($aData = array())
	{
		$aMethod = explode("/",$aData['sMethod']);
		
		$sMethod = array_pop($aMethod); //get
		$sModel = implode('/',$aMethod); //datatables/model_list
		$sClass = array_pop($aMethod); //model_list
		
		$this->load->model($sModel);
		return (method_exists($this->$sClass,$sMethod) ?
						($this->$sClass->$sMethod($aData)) :
						(array('iErr' => 1003, 'sMessage'=> $this->aErrorCodes[1003]))
					);
	}

	/**
	 * Gets the request, validate then Decodes the given String
	 * @param  [type:String] $sDec 	: Data to be decoded; comes from request
	 * @return [type:JSON array]    : Request Content
	 */
	function getRequest()
	{
		return ($sContent = file_get_contents("php://input")) ?
			(
				($aDec = json_decode(base64_decode($sContent),TRUE)) ? 
				$aDec : 
				array(
					'bNotify' 	=> TRUE,
					'iErr' 		=> 1001,
					'sMessage'	=> $this->aErrorCodes[1001]	
				)
			) : 
			array(
				'bNotify' 	=> TRUE,
				'iErr' 		=> 1001,
				'sMessage'	=> $this->aErrorCodes[1001]
			);
	}

	/**
	 * constructs and prints either the requested data or error
	 * @param  array  $aArg : data to be output 
	 * @return array       	: returns either the requested data or Error
	 */
	private function response($aArg = array())
	{
		echo $aArg ? base64_encode(json_encode($aArg)) : null;
	}
}