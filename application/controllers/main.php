<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class main extends CI_Controller 
{
	protected $CI = null;

	public function _construct()
	{
		$this->CI =& get_instance();
	}
	public function index()
	{
		if($this->tools_sessions->logged())
		{
			$this->load->view('index.php');
		}
		else
		{
			$this->load->view('login.php');	
		}
	}	

}