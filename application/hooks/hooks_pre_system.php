<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hooks_pre_system
{
	public function __construct()
	{
	}

	public function init()
	{
		$this->constants();
		$this->session();
	}

	private function constants()
	{
		define('HTTP_HOST', $_SERVER['HTTP_HOST']);
		define('SUB_DOMAIN', (
			HTTP_HOST == 'localhost' ?
			'ccms' :
			substr(HTTP_HOST, 0, strpos(HTTP_HOST, '.'))
		));
		define('SITE_URL', (
			HTTP_HOST == 'localhost' ?
			'http://localhost/prs/' :
			'http://'.HTTP_HOST.'/prs/'
		)); 

		$_SERVER['HTTP_HOST'] == 'localhost' ?
		/*
				define('PATH_BASE_FOLDER',                              '/var/www/files/prs/files/') :
                define('PATH_BASE_FOLDER',                              '/var/www/files/prs/files/');	
		*/
			define('PATH_BASE_FOLDER',				'/Applications/XAMPP/xamppfiles/files/prs/') :
			define('PATH_BASE_FOLDER',				'/Applications/Xampp/xamppfiles/files/prs/');
		
		
		define('PATH_BASE_APP',					FCPATH.APPPATH);

		define('PATH_LIBRARIES_DB',				PATH_BASE_APP.'libraries/db/temp/');
		define('PATH_VIEWS_FORMS',				PATH_BASE_APP.'views/modals/');
		define('PATH_VIEWS_LIST',				PATH_BASE_APP.'views/dashboards/');
		define('PATH_ASSETS_JS',				FCPATH.'assets/js/');
	}

	private function session()
	{
		/**
		 * FOR Windows
		 */
			 //session_save_path('C:/xampp/tmp/prs/');
		/**
		 * FOR IOS & Linux
		 * Local
		 * session_save_path('/var/www/files/prs/sessions');
		*/
		
		session_save_path('/Applications/Xampp/xamppfiles/temp/prs');
		
		session_start();

	}
}


