<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_admin_modules extends Db_core
{
	
	public $CI 			= null;

	protected $sModule  = '';
	protected $sIndex 	= 'iModuleId';
	protected $sTable 	= 'modules';

	protected $aDtCols = array(
			'sTitle' 		=> 'modules.sTitle',
			'bOnload' 		=> 'modules.bOnload',
			'iDateCreated' 	=> 'modules.iDateCreated'
		);

	protected $aFields = array(
			'iModuleId' 	=> 'modules.iModuleId',
			'sModuleName' 	=> 'modules.sModuleName',
			'sTableName' 	=> 'modules.sTableName',
			'sTitle' 		=> 'modules.sTitle',
			'bActive' 		=> 'modules.bActive',
			'bOnload' 		=> 'modules.bOnload',
			'iDateCreated' 	=> 'modules.iDateCreated'
	);

	function __construct(){
		$this->CI =& get_instance();
		$this->CI->load->library('db/Admin/db_admin_modules_permissions');
	}

	public function get($aArg = array())
	{
		$aRequest = $aArg["aRequest"];
		
		$this->CI->db->select(
			$this->select( 
				$aRequest['aColumns'],
				array_merge( $this->aDtCols, $this->aFields )
			)
		);

		$this->CI->db->from($this->sTable);

		$this->where($aRequest, 'iModuleId','modules');
		$this->where($aRequest, 'sModuleName','modules');
		$this->where($aRequest, 'bOnload','modules');

		$this->order($aRequest);
		$this->limit($aRequest);

		$this->search($this->aDtCols,$aRequest);

		$aData 		= $this->CI->db->get();
		$aFoundRows = $this->getFoundRows();
		$aResult	= $this->getResult($aData->result_array(),$aRequest);
		
		if(isset($aArg['aRequest']['iPermissionId'])) $aResult['iPermissionId'] = $this->CI->db_admin_modules_permissions->get($aArg['aRequest']['iPermissionId']);

		return (isset($aArg['sType']) && $aArg['sType'] == 'datatables') ? 
			(
				array(
					'data'	 			=> $aResult,
					'recordsTotal' 		=> $aFoundRows['iTotalRows'],
					'recordsFiltered' 	=> $aFoundRows['iFilteredRows']
				)
			) : $aResult;
	}

	public function add($aArg)
	{
		$aRequest = $aArg['aRequest'];

		$this->onload($aRequest);
		$aData = $this->insert($aRequest);
		isset($aRequest['iPermissionId']) && $this->CI->db_admin_modules_permissions->add(array_merge($aArg['aRequest'],array('iModuleId'=>$aData['id'])));

		return $aData['id'];
	}

	public function edit($aArg = array())
	{
		$aRequest = $aArg['aRequest'];
		
		isset($aRequest['iPermissionId']) && $this->CI->db_admin_modules_permissions->add($aRequest);
		$this->onload($aRequest);
		$this->where($aRequest, 'iModuleId','modules');
		$this->update($aRequest);
	}

	private function onload($aArg = array())
	{
		return (isset($aArg['bOnload']) && intVal($aArg['bOnload']) > 0) ? $this->update(array('bOnload' => 0)) : null;
	}
}
