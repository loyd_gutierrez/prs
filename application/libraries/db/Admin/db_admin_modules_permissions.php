<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_admin_modules_permissions extends Db_core
{
	
	public $CI 			= null;

	protected $sModule  = '';
	protected $sTable 	= 'module_permissions';

	protected $aFields = array(
			'iModuleId' 			=> 'module_permissions.iModuleId',
			'iPermissionId'		 	=> 'module_permissions.iPermissionId',
			'iBitIndex' 			=> 'module_permissions.iBitIndex'
	);

	function __construct(){
		$this->CI =& get_instance();
	}

	public function get($aArg = array())
	{
		$aRequest = $aArg["aRequest"];
			
		$this->CI->db->select(
			$this->select( 
				$aRequest['aColumns'],
				$this->aFields 
			)
		);

		$this->CI->db->from($this->sTable);

		$this->where($aRequest, 'iModuleId','module_permissions');

		$this->order($aRequest);
		$this->limit($aRequest);

		$aData 		= $this->CI->db->get();
		$aFoundRows = $this->getFoundRows();
		$aResult	= $this->getResult($aData->result_array(),$aRequest);

		return $aResult;
	}

	public function add($aArg)
	{
		$this->where($aArg,'iModuleId','module_permissions');
		$this->delete($aArg);

		$aModulePermissions = array();
		foreach($aArg['iPermissionId'] as $iBitIndex => $iPermissionId)
		{
			$this->insert(array( 'iModuleId' => $aArg['iModuleId'], 'iPermissionId' => $iPermissionId, 'iBitIndex' => $iBitIndex ));
		}
	}
}