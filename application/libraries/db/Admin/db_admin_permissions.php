<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_admin_permissions extends Db_core
{
	
	public $CI 			= null;

	protected $sModule  = '';
	protected $sIndex 	= 'iPermissionId';
	protected $sTable 	= 'permissions';

	protected $aDtCols = array(
			'sPermissionName' 	=> 'permissions.sPermissionName',
			'iDateCreated' 		=> 'permissions.iDateCreated'
		);

	protected $aFields = array(
			'iPermissionId' 	=> 'permissions.iPermissionId',
			'sPermissionName' 	=> 'permissions.sPermissionName',
			'bActive' 			=> 'permissions.bActive',
			'iDateCreated' 		=> 'permissions.iDateCreated'
	);

	function __construct(){
		$this->CI =& get_instance();
	}

	public function get($aArg = array())
	{
		$aRequest = $aArg["aRequest"];
		
		$this->CI->db->select(
			$this->select( 
				$aRequest['aColumns'],
				array_merge( $this->aDtCols, $this->aFields )
			)
		);

		$this->CI->db->from($this->sTable);

		$this->where($aRequest, 'iPermissionId','permissions');
		$this->where($aRequest, 'sPermissionName','permissions');

		$this->order($aRequest);
		$this->limit($aRequest);

		$this->search($this->aDtCols,$aRequest);

		$aData 		= $this->CI->db->get();
		$aFoundRows = $this->getFoundRows();
		$aResult	= $this->getResult($aData->result_array(),$aRequest);

		return (isset($aArg['sType']) && $aArg['sType'] == 'datatables') ? 
			(
				array(
					'data'	 			=> $aResult,
					'recordsTotal' 		=> $aFoundRows['iTotalRows'],
					'recordsFiltered' 	=> $aFoundRows['iFilteredRows']
				)
			) : $aResult;
	}

	public function add($aArg)
	{
		$aData = $this->insert($aArg['aRequest']);
		return $aData['id'];
	}

	public function edit($aArg = array())
	{
		$this->where($aArg['aRequest'], 'iPermissionId','permissions');
		$this->update($aArg['aRequest']);
	}
}