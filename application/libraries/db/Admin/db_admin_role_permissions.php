<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_admin_role_permissions extends Db_core
{
	
	public $CI 			= null;

	protected $sModule  = '';
	protected $sTable 	= 'role_permissions';

	protected $aFields = array(
			'iModuleId' 			=> 'role_permissions.iModuleId',
			'iRoleId'		 		=> 'role_permissions.iRoleId',
			'iPermission' 			=> 'role_permissions.iPermission',
			'sModuleName' 			=> 'modules.sModuleName',
			'sRoleName' 			=> 'roles.sRoleName'
	);

	function __construct(){
		$this->CI =& get_instance();
		$this->CI->load->library('db/Admin/db_admin_permissions');
	}

	public function get($aArg = array())
	{
		$aRequest = $aArg["aRequest"];
			
		$this->CI->db->select(
			$this->select(
				$aRequest['aColumns'],
				$this->aFields 
			)
		);

		$this->CI->db->from($this->sTable);
		$this->CI->db->join('roles', 'roles.iRoleId = role_permissions.iRoleId', 'left');
		$this->CI->db->join('modules', 'modules.iModuleId = role_permissions.iModuleId', 'left');

		$this->where($aRequest, 'iRoleId','role_permissions');

		$this->order($aRequest);
		$this->limit($aRequest);

		$aData 		= $this->CI->db->get();
		$aFoundRows = $this->getFoundRows();
		$aResult	= $this->getResult($aData->result_array(),$aRequest);

		if(isset($aRequest['bPermissions']) && $aRequest['bPermissions']){
			$aPermissions = $this->CI->db_admin_permissions->get(
				array('aRequest' => 
					array(
						'bActive'  => 1,
						'aOrder'   => 'iDateCreated ASC',
						'sColumn'   => 'sPermissionName',
						'aColumns' => array(
							'sPermissionName',
						)
					)
				)
			);
			
			foreach($aResult as $sPermission => $iPermission){
				$aResult[$sPermission] = array_filter(
					array_combine(array_values($aPermissions),array_values(str_split( str_pad(decbin($iPermission),5,0,STR_PAD_LEFT) ))),function($i){ return $i == 1; });

			}
		}

		return $aResult;
	}

	public function add($aArg)
	{
		$this->where($aArg,'iRoleId','role_permissions');
		$this->delete($aArg);

		$iPermission = bindec('1111');

		foreach($aArg['iModuleId'] as $iRoleId)
		{
			$this->insert(array( 'iModuleId' => $iRoleId, 'iRoleId' => $aArg['iRoleId'], 'iPermission' => $iPermission ));
		}
	}
}
