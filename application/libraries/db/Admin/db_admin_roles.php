<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_admin_roles extends Db_core
{
	
	public $CI 			= null;

	protected $sModule  = '';
	protected $sIndex 	= 'iRoleId';
	protected $sTable 	= 'roles';

	protected $aDtCols = array(
			'sPermissionName' 	=> 'roles.sPermissionName',
			'iDateCreated' 		=> 'roles.iDateCreated'
		);

	protected $aFields = array(
			'iRoleId' 			=> 'roles.iRoleId',
			'sRoleName'		 	=> 'roles.sRoleName',
			'bActive' 			=> 'roles.bActive',
			'iDateCreated' 		=> 'roles.iDateCreated'
	);

	function __construct(){
		$this->CI =& get_instance();
		$this->CI->load->library('db/Admin/db_admin_role_permissions');
	}

	public function get($aArg = array())
	{
		$aRequest = $aArg["aRequest"];
		
		$this->CI->db->select(
			$this->select( 
				$aRequest['aColumns'],
				array_merge( $this->aDtCols, $this->aFields )
			)
		);

		$this->CI->db->from($this->sTable);

		$this->where($aRequest, 'iRoleId','roles');
		$this->where($aRequest, 'sRoleName','roles');

		$this->order($aRequest);
		$this->limit($aRequest);

		$this->search($this->aDtCols,$aRequest);

		$aData 		= $this->CI->db->get();
		$aFoundRows = $this->getFoundRows();
		$aResult	= $this->getResult($aData->result_array(),$aRequest);

		if(isset($aArg['aRequest']['iModuleId'])) $aResult['iModuleId'] = $this->CI->db_admin_role_permissions->get($aRequest['iModuleId']);
		
		return (isset($aArg['sType']) && $aArg['sType'] == 'datatables') ? 
			(
				array(
					'data'	 			=> $aResult,
					'recordsTotal' 		=> $aFoundRows['iTotalRows'],
					'recordsFiltered' 	=> $aFoundRows['iFilteredRows']
				)
			) : $aResult;
	}

	public function add($aArg)
	{
		$aRequest = $aArg['aRequest'];
		$aData = $this->insert($aRequest);
		
		isset($aRequest['iModuleId']) && $this->CI->db_admin_role_permissions->add(array_merge($aRequest,array('iRoleId'=>$aData['id'])));

		return $aData['id'];
	}

	public function edit($aArg = array())
	{
		$aRequest = $aArg['aRequest'];

		isset($aRequest['iModuleId']) && $this->CI->db_admin_role_permissions->add($aRequest);

		$this->where($aRequest, 'iRoleId','roles');
		$this->update($aRequest);
	}
}
