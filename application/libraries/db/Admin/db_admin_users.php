<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_admin_users extends Db_core
{
	
	public $CI 			= null;

	protected $sModule  = '';
	protected $sIndex 	= 'iModuleId';
	protected $sTable 	= 'users';

	protected $aDtCols = array(
			'iUserId' 			=> 'users.iUserId',
			'sId' 				=> 'users.sId',
			'sFullName' 		=> 'CONCAT( users.sFirstName, CONCAT( " " ), users.sMiddleName, CONCAT( " " ), users.sLastName ) sFullName',
			'sRoleName' 		=> 'roles.sRoleName',
			'sDoctorsFullName' 	=> 'doctors.sFullName as sDoctorsFullName',	
			'bActive' 			=> 'users.bActive'
		);

	protected $aFields = array(
			'iUserId' 			=> 'users.iUserId',
			'sId' 				=> 'users.sId',
			'sUsername' 		=> 'users.sUsername',
			'sPassword' 		=> 'users.sPassword',
			'sFirstName' 		=> 'users.sFirstName',
			'sLastName' 		=> 'users.sLastName',
			'sMiddleName' 		=> 'users.sMiddleName',
			'iRoleId' 			=> 'users.iRoleId',
			'iDoctorId' 		=> 'users.iDoctorId'
	);

	function __construct(){
		$this->CI =& get_instance();
	}

	public function get($aArg = array())
	{
		$aRequest = $aArg["aRequest"];
		
		$this->CI->db->select(
			$this->select( 
				$aRequest['aColumns'],
				array_merge( $this->aDtCols, $this->aFields )
			)
		);

		$this->CI->db->from($this->sTable);
		
		if(!isset($aRequest['sType'])){
			$this->CI->db->join('roles', 'roles.iRoleId = users.iRoleId', 'left');
			$this->CI->db->join('doctors', 'doctors.iDoctorId = users.iDoctorId', 'left');	
		}
		

		$this->where($aRequest, 'iUserId','users');
		$this->where($aRequest, 'sId','users');
		$this->where($aRequest, 'sUsername','users');
		$this->where($aRequest, 'sPassword','users');

		$this->order($aRequest);
		$this->limit($aRequest);

		$this->search($this->aDtCols,$aRequest);

		$aData 		= $this->CI->db->get();
		$aFoundRows = $this->getFoundRows();
		$aResult	= $this->getResult($aData->result_array(),$aRequest);

		return (isset($aArg['sType']) && $aArg['sType'] == 'datatables') ? 
			(
				array(
					'data'	 			=> $aResult,
					'recordsTotal' 		=> $aFoundRows['iTotalRows'],
					'recordsFiltered' 	=> $aFoundRows['iFilteredRows']
				)
			) : $aResult;
	}

	public function add($aArg)
	{

		$aArg['aRequest']['sPassword'] = md5($aArg['aRequest']['sPassword']);
		$aRequest = $aArg['aRequest'];

		$aData = $this->insert($aRequest);
		return $aData['id'];
	}

	public function edit($aArg = array())
	{
		if(isset($aArg['aRequest']['sPassword'])) $aArg['aRequest']['sPassword'] = md5($aArg['aRequest']['sPassword']);

		$aRequest = $aArg['aRequest'];
		$this->where($aRequest, 'iUserId','users');
		$this->update($aRequest);
	}
}