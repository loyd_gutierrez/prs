<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_main_dashboard extends Db_core
{
	
	public $CI 			= null;

	protected $sModule  = '';
	protected $sIndex 	= 'iTransactionId';
	protected $sTable 	= 'transactions';

	protected $aDtCols = array(
			'iTransactionId' 		=> 'transactions.iTransactionId',
			'sPatientName' 				=> 'CONCAT( patients.sLastName, CONCAT(", "), patients.sFirstName, CONCAT(" "), patients.sMiddleName )',
			'sDoctorName' 				=> 'CONCAT( doctors.sLastName, CONCAT(", "), doctors.sFirstName, CONCAT(" "), doctors.sMiddleName )',
			'iTransactionDate' 			=> 'transactions.iTransactionDate',
			'bActive' 					=> 'transactions.bActive',
			'iDateCreated' 				=> 'transactions.iDateCreated'
		);

	protected $aFields = array(
			'iTransactionId' 	=> 'transactions.iTransactionId',
			'sCode' 				=> 'transactions.sCode',
			'sDescription' 			=> 'transactions.sDescription',
			'bActive' 				=> 'transactions.bActive',
			'iDateCreated' 			=> 'transactions.iDateCreated'
	);

	function __construct(){
		$this->CI =& get_instance();
	}

	public function get($aArg = array())
	{
		$aRequest = $aArg["aRequest"];
		
		$this->CI->db->select(
			$this->select( 
				$aRequest['aColumns'],
				array_merge( $this->aDtCols, $this->aFields )
			)
		);

		$this->CI->db->from($this->sTable);

		$this->where($aRequest, 'iTransactionId','transactions');
		$this->where($aRequest, 'sCode','transactions');
		$this->where($aRequest, 'bActive','transactions');
		$this->where($aRequest, 'iDateCreated','transactions');

		$this->order($aRequest);
		$this->limit($aRequest);

		$this->search($this->aDtCols,$aRequest);

		$aData 		= $this->CI->db->get();
		$aFoundRows = $this->getFoundRows();
		$aResult	= $this->getResult($aData->result_array(),$aRequest);

		return (isset($aArg['sType']) && $aArg['sType'] == 'datatables') ? 
			(
				array(
					'data'	 			=> $aResult,
					'recordsTotal' 		=> $aFoundRows['iTotalRows'],
					'recordsFiltered' 	=> $aFoundRows['iFilteredRows']
				)
			) : $aResult;
	}

	public function add($aArg)
	{
		$aRequest = $aArg['aRequest'];
		$aData = $this->insert($aRequest);
		return $aData['id'];
	}

	public function edit($aArg = array())
	{
		$aRequest = $aArg['aRequest'];
		$this->where($aRequest, 'iTransactionId','transactions');
		$this->update($aRequest);
	}
}