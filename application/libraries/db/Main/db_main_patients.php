<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_main_patients extends Db_core
{
	
	public $CI 			= null;

	protected $sModule  = '';
	protected $sIndex 	= 'iTransactionId';
	protected $sTable 	= 'patients';

	protected $aDtCols = array(
			'iPatientId' 				=> 'patients.iPatientId',
			'sPatientName' 				=> 'CONCAT( patients.sLastName, CONCAT(", "), patients.sFirstName, CONCAT(" "), patients.sMiddleName )',
			'sDoctorName' 				=> 'CONCAT( doctors.sLastName, CONCAT(", "), doctors.sFirstName, CONCAT(" "), doctors.sMiddleName )',
			'iTransactionDate' 			=> 'CONCAT(from_unixtime(doctors_schedules.iDate,"%b %d"),CONCAT(" "),from_unixtime(itime,"%r"))',
			'iDateCreated' 				=> 'patients.iDateCreated',
			'bActive' 					=> 'patients.bActive'
		);

	protected $aFields = array(
			'iPatientId' 			=> 'patients.iPatientId',
			'sLastName' 			=> 'patients.sLastName',
			'sFirstName' 			=> 'patients.sFirstName',
			'sMiddleName' 			=> 'patients.sMiddleName',
			'iGenderId' 			=> 'patients.iGenderId',
			'iCivilStatusId' 		=> 'patients.iCivilStatusId',
			'sContactNumber' 		=> 'patients.sContactNumber',
			'sAddress' 				=> 'patients.sAddress',
			'iCityId' 				=> 'patients.iCityId',
			'iProvinceId' 			=> 'patients.iProvinceId',
			'iDateCreated' 			=> 'patients.iDateCreated',
	);

	function __construct(){
		$this->CI =& get_instance();
	}

	public function get($aArg = array())
	{
		$aRequest = $aArg["aRequest"];

		$this->CI->db->select(
			$this->select( 
				$aRequest['aColumns'],
				array_merge( $this->aDtCols, $this->aFields )
			)
		);

		$this->CI->db->from($this->sTable);

		if(!isset($aRequest['sType']))
			$this->CI->db->join('doctors_schedules','doctors_schedules.iDoctorScheduleId = doctors_schedules.iDoctorScheduleId','left');

		$this->where($aRequest, 'iTransactionId','patients');
		$this->where($aRequest, 'sCode','patients');
		$this->where($aRequest, 'bActive','patients');
		$this->where($aRequest, 'iDateCreated','patients');

		$this->order($aRequest);
		$this->limit($aRequest);

		$this->search($this->aDtCols,$aRequest);

		$aData 		= $this->CI->db->get();
		$aFoundRows = $this->getFoundRows();
		$aResult	= $this->getResult($aData->result_array(),$aRequest);

		return (isset($aArg['sType']) && $aArg['sType'] == 'datatables') ? 
			(
				array(
					'data'	 			=> $aResult,
					'recordsTotal' 		=> $aFoundRows['iTotalRows'],
					'recordsFiltered' 	=> $aFoundRows['iFilteredRows']
				)
			) : $aResult;
	}

	public function add($aArg)
	{
		$aRequest = $aArg['aRequest'];
		
		$aData = $this->insert($aRequest);
		return $aData['id'];
	}

	public function edit($aArg = array())
	{
		$aRequest = $aArg['aRequest'];
		$this->where($aRequest, 'iPatientId','patients');
		$this->update($aRequest);
	}
}