<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_main_transactions extends Db_core
{
	
	public $CI 			= null;

	protected $sModule  = '';
	protected $sIndex 	= 'iTransactionId';
	protected $sTable 	= 'transactions';

	protected $aDtCols = array(
			'iTransactionId' 			=> 'transactions.iTransactionId',
			// 'sPatientName' 				=> 'CONCAT( patients.sLastName, CONCAT( ", " ), patients.sFirstName, CONCAT(" "), patients.sMiddleName ) as sPatientName',
			// 'sDoctorName' 				=> 'CONCAT( doctors.sLastName, CONCAT( ", " ), doctors.sFirstName, CONCAT(" "), doctors.sMiddleName ) as sDoctorName',
			'iTransactionDate' 			=> 'CONCAT( transactions.sDay," - ",from_unixtime(transactions.iTime,"%h:%i %p")) as iTransactionDate',
			'bActive' 					=> 'transactions.bActive',
			'sAppointmentType' 			=> 'appointment_types.sCode as sAppointmentType',
			'iDateCreated' 				=> 'patients.iDateCreated',

			'sDoctorLastName' 			=> 'doctors.sLastName as sDoctorLastName',
			'sDoctorFirstName' 			=> 'doctors.sFirstName as sDoctorFirstName',
			'sDoctorMiddleName' 		=> 'doctors.sMiddleName as sDoctorMiddleName',

			'sPatientLastName' 		=> 'patients.sLastName as sPatientLastName',
			'sPatientFirstName' 	=> 'patients.sFirstName as sPatientFirstName',
			'sPatientMiddleName' 	=> 'patients.sMiddleName as sPatientMiddleName',

			'iGenderId' 				=> 'patients.iGenderId',
			'iCivilStatusId' 			=> 'patients.iCivilStatusId',
			'sContactNumber' 			=> 'patients.sContactNumber',
			'iProvinceId' 				=> 'patients.iProvinceId',
			'iCityId' 					=> 'patients.iCityId',
			'sAddress' 					=> 'patients.sAddress',
		);

	protected $aFields = array(
			'iTransactionId' 		=> 'transactions.iTransactionId',
			'sTransactionCode' 		=> 'transactions.sTransactionCode',
			'iAppointmentTypeId' 	=> 'transactions.iAppointmentTypeId',
			'iPatientId' 			=> 'transactions.iPatientId',
			'sLastName' 			=> 'patients.sLastName as sLastName',
			'sFirstName' 			=> 'patients.sFirstName as sFirstName',
			'sMiddleName' 			=> 'patients.sMiddleName as sMiddleName',
			'iDoctorId' 			=> 'transactions.iDoctorId',
			'sDay' 					=> 'transactions.sDay',
			'iTime' 				=> 'transactions.iTime',
			'bStatus' 				=> 'transactions.bStatus',
			'sRemarks' 				=> 'transactions.sRemarks',
			'iUserId' 				=> 'transactions.iUserId',
			'bActive' 				=> 'transactions.bActive',
			'sSchedule' 			=> 'CONCAT(from_unixtime(doctors_schedules.iDate,"(%a)%b/%d")," ",from_unixtime(doctors_schedules.iDateStart,"%h:%i %p")," - ",from_unixtime(doctors_schedules.iDateEnd,"%h:%i %p")) as sSchedule',
	);

	function __construct(){
		$this->CI =& get_instance();
		$this->CI->load->library('db/Main/db_main_patients');
		$this->CI->load->library('db/Management/db_management_doctors');
		$this->CI->load->library('db/Admin/db_admin_users');
	}

	public function get($aArg = array())
	{
		$aRequest = $aArg["aRequest"];
		
		$this->CI->db->_protect_identifiers = FALSE;

		$this->CI->db->select(
			$this->select( 
				$aRequest['aColumns'],
				array_merge( $this->aDtCols, $this->aFields )
			)
		);

		$this->CI->db->from($this->sTable);

		if(!isset($aRequest['sType'])){
			$this->CI->db->join('patients','patients.iPatientId = transactions.iPatientId','left');
			$this->CI->db->join('doctors','doctors.iDoctorId = transactions.iDoctorId','left');
			$this->CI->db->join('appointment_types','appointment_types.iAppointmentTypeId = transactions.iAppointmentTypeId','left');	
		}

		$this->where($aRequest, 'iTransactionId','transactions');
		$this->where($aRequest, 'sCode','transactions');
		$this->where($aRequest, 'bActive','transactions');
		$this->where($aRequest, 'iDateCreated','transactions');
		$this->where($aRequest, 'sTransactionCode','transactions');

		if($this->CI->tools_sessions->get('iDoctorId')){
			$this->where(array('iDoctorId' => $this->CI->tools_sessions->get('iDoctorId')), 'iDoctorId','transactions');
		}
		

		if($this->CI->tools_sessions->get('sRole') != 'Administrator'){
			$this->where( array('bActive' => 1), 'bActive','transactions' );
		}

		$this->order($aRequest);
		$this->limit($aRequest);
		
		$this->search($this->aDtCols,$aRequest);

		$aData 		= $this->CI->db->get();
		$aFoundRows = $this->getFoundRows();
		$aResult	= $this->getResult($aData->result_array(),$aRequest);

		return (isset($aArg['sType']) && $aArg['sType'] == 'datatables') ? 
			(
				array(
					'data'	 			=> $aResult,
					'recordsTotal' 		=> $aFoundRows['iTotalRows'],
					'recordsFiltered' 	=> $aFoundRows['iFilteredRows'],
					'iDashboardCounts' 	=> $this->getCounts() 
				)
			) : $aResult;
	}

	
	public function add($aArg)
	{
		$iUserId = $this->CI->tools_sessions->get('iUserId');
		if($iUserId > 0) {
			$aArg['aRequest']['iUserId'] = $iUserId;
		}
		$aArg['aRequest']['sTransactionCode'] = $this->TransactionCode();

		$aRequest = $aArg['aRequest'];
		
		unset($aRequest['sLastName']);
		unset($aRequest['sFirstName']);
		unset($aRequest['sMiddleName']);

		$iPatient = $this->CI->db_main_patients->add($aArg);
		$aData = $this->insert( array_merge($aRequest,array('iPatientId' => $iPatient)) );
		$aRecord =  $this->get( 
				array(
					'aRequest' => array(
						'iTransactionId' => $aData['id'],
						'aColumns' => array('sTransactionCode')
					)
				)
			);


		return array('id' => $aData['id'],'sTransactionCode' => $aRecord[0]['sTransactionCode']);
	}

	public function edit($aArg = array())
	{
		$aRequest = $aArg['aRequest'];
		
		if(isset($aRequest['bActive']) && $aRequest['bActive'] == 0)
		{
			$this->remove($aRequest);
		}
		else
		{
			$iPatient = $this->CI->db_main_patients->edit($aArg);

			unset($aRequest['sLastName']);
			unset($aRequest['sFirstName']);
			unset($aRequest['sMiddleName']);
			
			$this->where($aRequest, 'iTransactionId','transactions');
			$this->update($aRequest);
		}
	}

	public function remove($aArg = array())
	{
		$this->where($aArg, 'iTransactionId','transactions');
		$this->CI->db->delete($this->sTable);
	}

	private function TransactionCode(){
		$code = substr(str_shuffle(md5(time().rand())),0,7); 

		if( (count($this->get(array('aRequest' => array('sTransactionCode' => $code,'aColumns' => array('sTransactionCode')))))) > 0 ) return $this->TransactionCode();

		return $code;
	}

	private function getCounts()
	{
		return array(
			'iScheduledCount' => count($this->CI->db->query("select iTransactionId from transactions where bActive = 1")),
			'iDoctorsCount' => count($this->CI->db_management_doctors->get(array('aRequest' => array('sType' => 'count','aColumns' => array('iDoctorId'))))),
			'iNursesCount' => count($this->CI->db_admin_users->get(array('aRequest' => array('sType' => 'count','aColumns' => array('iUserId'))))),
			'iPatientCount' => count($this->CI->db_main_patients->get(array('aRequest' => array('sType' => 'count','aColumns' => array('iPatientId')))))
		);
		

	}
}
