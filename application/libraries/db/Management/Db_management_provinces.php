<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_management_provinces extends Db_core
{
	
	public $CI 			= null;

	protected $sModule  = '';
	protected $sIndex 	= 'iprovincesId';
	protected $sTable 	= 'provinces';

	protected $aDtCols = array(
		'iprovincesId' 			=> 'provinces.iprovincesId',
		'bActive' 				=> 'provinces.bActive',
		'sprovinces' 			=> 'provinces.sprovinces'
	);

	function __construct(){
		$this->CI =& get_instance();
	}

	public function get($aArg = array())
	{
		$aRequest = $aArg["aRequest"];
		
		$this->CI->db->select(
			$this->select( 
				$aRequest['aColumns'],
				array_merge( $this->aDtCols )
			)
		);

		$this->CI->db->from($this->sTable);

		$this->where($aRequest, 'iprovincesId','provinces');

		$this->order($aRequest);
		$this->limit($aRequest);

		$this->search($this->aDtCols,$aRequest);

		$aData 		= $this->CI->db->get();
		$aFoundRows = $this->getFoundRows();
		$aResult	= $this->getResult($aData->result_array(),$aRequest);

		return (isset($aArg['sType']) && $aArg['sType'] == 'datatables') ? 
			(
				array(
					'data'	 			=> $aResult,
					'recordsTotal' 		=> $aFoundRows['iTotalRows'],
					'recordsFiltered' 	=> $aFoundRows['iFilteredRows']
				)
			) : $aResult;
	}
}