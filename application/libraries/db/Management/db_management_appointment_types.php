<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_management_appointment_types extends Db_core
{
	
	public $CI 			= null;

	protected $sModule  = '';
	protected $sIndex 	= 'iAppointmentTypeId';
	protected $sTable 	= 'appointment_types';

	protected $aDtCols = array(
			'iAppointmentTypeId' 	=> 'appointment_types.iAppointmentTypeId',
			'sCode' 				=> 'appointment_types.sCode',
			'bActive' 				=> 'appointment_types.bActive',
			'iDateCreated' 			=> 'appointment_types.iDateCreated'
		);

	protected $aFields = array(
			'iAppointmentTypeId' 	=> 'appointment_types.iAppointmentTypeId',
			'sCode' 				=> 'appointment_types.sCode',
			'sDescription' 			=> 'appointment_types.sDescription',
			'bActive' 				=> 'appointment_types.bActive',
			'iDateCreated' 			=> 'appointment_types.iDateCreated'
	);

	function __construct(){
		$this->CI =& get_instance();
	}

	public function get($aArg = array())
	{
		$aRequest = $aArg["aRequest"];
		
		$this->CI->db->select(
			$this->select( 
				$aRequest['aColumns'],
				array_merge( $this->aDtCols, $this->aFields )
			)
		);

		$this->CI->db->from($this->sTable);

		$this->where($aRequest, 'iAppointmentTypeId','appointment_types');
		$this->where($aRequest, 'sCode','appointment_types');
		$this->where($aRequest, 'bActive','appointment_types');
		$this->where($aRequest, 'iDateCreated','appointment_types');

		$this->order($aRequest);
		$this->limit($aRequest);

		$this->search($this->aDtCols,$aRequest);

		$aData 		= $this->CI->db->get();
		$aFoundRows = $this->getFoundRows();
		$aResult	= $this->getResult($aData->result_array(),$aRequest);

		return (isset($aArg['sType']) && $aArg['sType'] == 'datatables') ? 
			(
				array(
					'data'	 			=> $aResult,
					'recordsTotal' 		=> $aFoundRows['iTotalRows'],
					'recordsFiltered' 	=> $aFoundRows['iFilteredRows']
				)
			) : $aResult;
	}

	public function add($aArg)
	{
		$aRequest = $aArg['aRequest'];
		$aData = $this->insert($aRequest);
		return $aData['id'];
	}

	public function edit($aArg = array())
	{
		$aRequest = $aArg['aRequest'];
		$this->where($aRequest, 'iAppointmentTypeId','appointment_types');
		$this->update($aRequest);
	}
}