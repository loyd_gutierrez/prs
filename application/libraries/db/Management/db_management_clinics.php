<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_management_clinics extends Db_core
{
	
	public $CI 			= null;

	protected $sModule  = '';
	protected $sIndex 	= 'iClinicId';
	protected $sTable 	= 'clinics';

	protected $aDtCols = array(
		'iClinicId' 			=> 'clinics.iClinicId',
		'bActive' 				=> 'clinics.bActive',
		'iDateCreated' 			=> 'clinics.iDateCreated',
		'sRoom' 				=> 'CONCAT( clinics.iFloorNumber , CONCAT("-") , clinics.sRoomNumber ) sRoom',
		'sBuildingName' 		=> 'clinics.sBuildingName'

	);

	protected $aFields = array(
		'iClinicId' 			=> 'clinics.iClinicId',
		'bActive' 				=> 'clinics.bActive',
		'iFloorNumber' 			=> 'clinics.iFloorNumber',
		'sRoomNumber' 			=> 'clinics.sRoomNumber',
		'sBuildingName' 		=> 'clinics.sBuildingName'
	);

	function __construct(){
		$this->CI =& get_instance();
	}

	public function get($aArg = array())
	{
		$aRequest = $aArg["aRequest"];
		
		$this->CI->db->select(
			$this->select( 
				$aRequest['aColumns'],
				array_merge( $this->aDtCols, $this->aFields )
			)
		);

		$this->CI->db->from($this->sTable);

		$this->where($aRequest, 'iClinicId','clinics');

		$this->order($aRequest);
		$this->limit($aRequest);

		$this->search($this->aDtCols,$aRequest);

		$aData 		= $this->CI->db->get();
		$aFoundRows = $this->getFoundRows();
		$aResult	= $this->getResult($aData->result_array(),$aRequest);

		return (isset($aArg['sType']) && $aArg['sType'] == 'datatables') ? 
			(
				array(
					'data'	 			=> $aResult,
					'recordsTotal' 		=> $aFoundRows['iTotalRows'],
					'recordsFiltered' 	=> $aFoundRows['iFilteredRows']
				)
			) : $aResult;
	}

	public function add($aArg)
	{
		$aRequest = $aArg['aRequest'];
		$aData = $this->insert($aRequest);
		return $aData['id'];
	}

	public function edit($aArg = array())
	{
		$aRequest = $aArg['aRequest'];
		$this->where($aRequest, 'iClinicId','clinics');
		$this->update($aRequest);
	}
}