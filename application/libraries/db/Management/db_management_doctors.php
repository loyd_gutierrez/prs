<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_management_doctors extends Db_core
{
	
	public $CI 			= null;

	protected $sModule  = '';
	protected $sIndex 	= 'iDoctorId';
	protected $sTable 	= 'doctors';

	protected $aDtCols = array(
		'iDoctorId' 			=> 'doctors.iDoctorId',
		'sFullName' 			=> 'CONCAT( doctors.sFirstName, CONCAT( " " ), doctors.sMiddleName, CONCAT( " " ), doctors.sLastName ) sFullName',
		'sRoom' 				=> 'CONCAT( clinics.iFloorNumber , CONCAT("-") , clinics.sRoomNumber ) sRoom',
		'iClinicId' 			=> 'doctors_clinic.iClinicId',
		'iDateCreated' 			=> 'doctors.iDateCreated'

	);

	protected $aFields = array(
		'iDoctorId' 			=> 'doctors.iDoctorId',
		'sLastName' 			=> 'doctors.sLastName',
		'sFirstName' 			=> 'doctors.sFirstName',
		'sMiddleName' 			=> 'doctors.sMiddleName',
		'sContactNumber' 		=> 'doctors.sContactNumber',
		'iGenderId' 			=> 'doctors.iGenderId',
		'iCivilStatusId' 		=> 'doctors.iCivilStatusId',
		'bActive' 				=> 'doctors.bActive',
		'sPhoto' 				=> 'doctors.sPhoto'
	);

	function __construct(){
		$this->CI =& get_instance();
		$this->CI->load->library('db/Management/db_management_doctors_schedules');
		$this->CI->load->library('db/Management/db_management_doctors_clinic');
	}

	public function get($aArg = array())
	{
		$aRequest = $aArg["aRequest"];
		
		$this->CI->db->select(
			$this->select( 
				$aRequest['aColumns'],
				array_merge( $this->aDtCols, $this->aFields )
			)
		);

		$this->CI->db->from($this->sTable);

		if(!isset($aRequest['sType'])){
			$this->CI->db->join('doctors_clinic','doctors_clinic.iDoctorId = doctors.iDoctorId','left');
			$this->CI->db->join('clinics','clinics.iClinicId = doctors_clinic.iClinicId','left');

		}
		
		$this->where($aRequest, 'iDoctorId','doctors');
		if(isset($aRequest["bTransaction"]) && $aRequest["bTransaction"]){
			// $this->where( array('iDoctorId' => $this->CI->tools_sessions->get('iDoctorId')), 'iDoctorId','doctors');	
		} 

		$this->order($aRequest);
		$this->limit($aRequest);

		$this->search($this->aDtCols,$aRequest);

		$aData 		= $this->CI->db->get();
		$aFoundRows = $this->getFoundRows();
		$aResult	= $this->getResult($aData->result_array(),$aRequest);

		if(isset($aArg['aRequest']['iDoctorScheduleId'])) $aResult['iDoctorScheduleId'] = $this->CI->db_management_doctors_schedules->get($aRequest['iDoctorScheduleId']);

		return (isset($aArg['sType']) && $aArg['sType'] == 'datatables') ? 
			(
				array(
					'data'	 			=> $aResult,
					'recordsTotal' 		=> $aFoundRows['iTotalRows'],
					'recordsFiltered' 	=> $aFoundRows['iFilteredRows']
				)
			) : $aResult;
	}

	public function add($aArg)
	{

		$aRequest = $aArg['aRequest'];
		
		$aData = $this->insert($aRequest);
		$aRequest['iDoctorId'] = $aData['id'];
		$this->DoctorSchedule($aRequest);

		return $aData['id'];
	}

	public function edit($aArg = array())
	{
		$aRequest = $aArg['aRequest'];

		$this->where($aRequest, 'iDoctorId','doctors');
		$this->update($aRequest);

		$this->DoctorSchedule($aRequest);
	}

	private function DoctorSchedule($aArg)
	{
		$this->CI->db_management_doctors_clinic->add($aArg);
		$this->CI->db_management_doctors_schedules->add($aArg);
	}
}
