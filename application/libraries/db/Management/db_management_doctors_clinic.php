<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_management_doctors_clinic extends Db_core
{
	
	public $CI 			= null;

	protected $sModule  = '';
	protected $sIndex 	= 'iDoctorId';
	protected $sTable 	= 'doctors_clinic';

	protected $aDtCols = array(
	);

	protected $aFields = array(
		'iDoctorId' 			=> 'doctors_clinic.iDoctorId',
		'iClinicId' 			=> 'doctors_clinic.iClinicId'
	);

	function __construct(){
		$this->CI =& get_instance();
	}

	public function get($aArg = array())
	{
		$aRequest = $aArg["aRequest"];
		
		$this->CI->db->select(
			$this->select( 
				$aRequest['aColumns'],
				array_merge( $this->aDtCols, $this->aFields )
			)
		);

		$this->CI->db->from($this->sTable);

		$this->where($aRequest, 'iClinicId','doctors_clinic');
		$this->where($aRequest, 'iDoctorId','doctors_clinic');

		$this->order($aRequest);
		$this->limit($aRequest);

		$this->search($this->aDtCols,$aRequest);

		$aData 		= $this->CI->db->get();
		$aFoundRows = $this->getFoundRows();
		$aResult	= $this->getResult($aData->result_array(),$aRequest);

		return (isset($aArg['sType']) && $aArg['sType'] == 'datatables') ? 
			(
				array(
					'data'	 			=> $aResult,
					'recordsTotal' 		=> $aFoundRows['iTotalRows'],
					'recordsFiltered' 	=> $aFoundRows['iFilteredRows']
				)
			) : $aResult;
	}

	public function add($aArg)
	{

		$this->where($aArg,'iDoctorId','doctors_clinic');
		$this->where($aArg,'iClinicId','doctors_clinic');
		$this->delete($aArg);

		$this->insert($aArg);
	}
}