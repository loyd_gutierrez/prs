<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_management_doctors_schedules extends Db_core
{
	
	public $CI 			= null;

	protected $sModule  = '';
	protected $sIndex 	= 'iDoctorScheduleId';
	protected $sTable 	= 'doctors_schedules';

	protected $bConvertSchedule = false;

	protected $aDtCols = array(
		'sSchedule' 				=> 'CONCAT(doctors_schedules.sDay," ",from_unixtime(doctors_schedules.iDateStart,"%h:%i%p")," - ",from_unixtime(doctors_schedules.iDateEnd,"%h:%i%p")) as sSchedule',
		'iPatients' 				=> 'doctors_schedules.iPatients'
	);

	protected $aFields = array(
		'iDoctorScheduleId' 	=> 'doctors_schedules.iDoctorScheduleId',
		'iDoctorId' 			=> 'doctors_schedules.iDoctorId',
		'sDay' 					=> 'doctors_schedules.sDay',
		'iDateStart' 			=> 'doctors_schedules.iDateStart',
		'iDateEnd' 				=> 'doctors_schedules.iDateEnd',
		'iPatients' 			=> 'doctors_schedules.iPatients',
		'bActive' 				=> 'doctors_schedules.bActive',
		'iDateCreated' 			=> 'doctors_schedules.iDateCreated'
	);

	function __construct(){
		$this->CI =& get_instance();
	}

	public function get($aArg = array())
	{
		$aRequest = $aArg["aRequest"];

		$this->CI->db->_protect_identifiers=false;
		
		if(isset($aRequest['sColumn']) && $aRequest['sColumn'] == 'sSchedule'){
			$this->bConvertSchedule = true;

			$aRequest['aColumns'] = array_merge(
					$aRequest['aColumns'],
					array('iSlots','iPatients')
				);

			$this->CI->db->select(
				$this->select( 
					$aRequest['aColumns'],
					array_merge(
						array( 
							'iSlots' 	=> 'slots.iSlots',
							'sSchedule' => 'CONCAT(doctors_schedules.sDay," ",from_unixtime(doctors_schedules.iDateStart,"%h:%i %p")," - ",from_unixtime(doctors_schedules.iDateEnd,"%h:%i %p")) as sSchedule',
							'iPatients' => 'doctors_schedules.iPatients'
						),
						$this->aFields
					)
				)
			);	
			$this->CI->db->join('(SELECT sDay,count(*) as iSlots FROM transactions WHERE iDoctorId = "'. $aRequest['iDoctorId'] .'" GROUP BY sDay) as slots','slots.sDay = doctors_schedules.sDay','left');

			$aRequest['aGroup'] = 'sSchedule';
			$aRequest['aOrder'] = 'doctors_schedules.iDoctorScheduleId';	
		} 
		else
		{
			$this->CI->db->select(
				$this->select( 
					$aRequest['aColumns'],
					$this->aFields
				)
			);
		}
		$this->CI->db->from($this->sTable);
		
		$this->CI->db->join('transactions','doctors_schedules.iDoctorId = transactions.iDoctorId','left');

		$this->where($aRequest, 'iDoctorId','doctors_schedules');
		$this->where($aRequest, 'iDoctorScheduleId','doctors_schedules');

		$this->order($aRequest);
		$this->group($aRequest);
		$this->limit($aRequest);

		$this->search($this->aDtCols,$aRequest);

		$aData 		= $this->CI->db->get();
		$aFoundRows = $this->getFoundRows();
		// $aResult	= $this->bConvertSchedule ? $this->schedule($aData->result_array()) : $this->getResult($aData->result_array(),$aRequest);
		$aResult	= $aData->result_array();	

		return (isset($aArg['sType']) && $aArg['sType'] == 'datatables') ? 
			(
				array(
					'data'	 			=> $aResult,
					'recordsTotal' 		=> $aFoundRows['iTotalRows'],
					'recordsFiltered' 	=> $aFoundRows['iFilteredRows']
				)
			) : $aResult;
	}

	public function add($aArg)
	{
		if( !isset($aArg['iDateStart']) && !count($this->get(array('aRequest' => array('aColumns' => array('iDoctorScheduleId')) ) ) )) return;

		$this->where($aArg,'iDoctorId','doctors_schedules');
		$this->delete($aArg);

		if(isset($aArg['iDateStart']) && count($aArg['iDateStart']) > 0)
		{
			foreach($aArg['iDateStart'] as $sKey => $sVal)
			{
				if($aArg['sDay'][$sKey] && $aArg['iDateEnd'][$sKey] && $sVal)
				{
					$aData = $this->insert(
						array_merge(
							$aArg,
							array(
								'sDay' 			=> $aArg['sDay'][$sKey], 
								'iDateEnd' 		=> $aArg['iDateEnd'][$sKey], 
								'iPatients' 	=> $aArg['iPatients'][$sKey],
								'iDateStart' 	=> $sVal
							)
						)
					);
				}
			}
		}
	}

	private function schedule($aArg)
	{
		$aReturn = array();
		
		foreach($aArg as $sKey => $aValues)
		{
			(intVal($aValues['iSlots']) < $aValues['iPatients']) && $aReturn[] = $aValues['sSchedule'];
		}	
		
		return $aReturn;
	}
}
