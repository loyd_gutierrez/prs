<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_management_meta_groups extends Db_core
{
	
	public $CI 			= null;

	protected $sModule  = '';
	protected $sIndex 	= 'iMetaGroupId';
	protected $sTable 	= 'meta_groups';

	protected $aDtCols = array(
		'iMetaGroupId' 			=> 'meta_groups.iMetaGroupId',
		'bActive' 				=> 'meta_groups.bActive',
		'iDateCreated' 			=> 'meta_groups.iDateCreated',
		'sCode' 				=> 'meta_groups.sCode',
		'sName' 				=> 'meta_groups.sName'

	);

	protected $aFields = array(
		'iMetaGroupId' 			=> 'meta_groups.iMetaGroupId',
		'sCode' 				=> 'meta_groups.sCode',
		'sName' 				=> 'meta_groups.sName',
		'bActive' 				=> 'meta_groups.bActive',
		'iDateCreated' 			=> 'meta_groups.iDateCreated'
	);

	function __construct(){
		$this->CI =& get_instance();
	}

	public function get($aArg = array())
	{
		$aRequest = $aArg["aRequest"];
		
		$this->CI->db->select(
			$this->select( 
				$aRequest['aColumns'],
				array_merge( $this->aDtCols, $this->aFields )
			)
		);

		$this->CI->db->from($this->sTable);

		$this->where($aRequest, 'iMetaGroupId','meta_groups');
		$this->where($aRequest, 'sCode','meta_groups');

		$this->order($aRequest);
		$this->limit($aRequest);

		$this->search($this->aDtCols,$aRequest);

		$aData 		= $this->CI->db->get();
		$aFoundRows = $this->getFoundRows();
		$aResult	= $this->getResult($aData->result_array(),$aRequest);

		return (isset($aArg['sType']) && $aArg['sType'] == 'datatables') ? 
			(
				array(
					'data'	 			=> $aResult,
					'recordsTotal' 		=> $aFoundRows['iTotalRows'],
					'recordsFiltered' 	=> $aFoundRows['iFilteredRows']
				)
			) : $aResult;
	}

	public function add($aArg)
	{
		$aRequest = $aArg['aRequest'];
		$aData = $this->insert($aRequest);
		return $aData['id'];
	}

	public function edit($aArg = array())
	{
		$aRequest = $aArg['aRequest'];
		$this->where($aRequest, 'iMetaGroupId','meta_groups');
		$this->update($aRequest);
	}
}