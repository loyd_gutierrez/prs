<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_management_metas extends Db_core
{
	
	public $CI 			= null;

	protected $sModule  = '';
	protected $sIndex 	= 'iMetaGroupId';
	protected $sTable 	= 'metas';

	protected $aDtCols = array(
		'iMetaId' 				=> 'metas.iMetaId',
		'sMetaGroupName' 		=> 'meta_groups.sCode as sMetaGroupName',
		'sCode' 				=> 'metas.sCode',
		'bActive' 				=> 'metas.bActive',
		'iDateCreated' 			=> 'metas.iDateCreated',
	);

	protected $aFields = array(
		'iMetaId' 				=> 'metas.iMetaId',
		'iMetaGroupId' 			=> 'metas.iMetaGroupId',
		'sCode' 				=> 'metas.sCode',
		'sName' 				=> 'metas.sName',
		'bActive' 				=> 'metas.bActive',
		'iDateCreated' 			=> 'metas.iDateCreated'
	);

	function __construct(){
		$this->CI =& get_instance();
	}

	public function get($aArg = array())
	{
		$aRequest = $aArg["aRequest"];
		
		$this->CI->db->select(
			$this->select( 
				$aRequest['aColumns'],
				array_merge( $this->aDtCols, $this->aFields )
			)
		);

		$this->CI->db->from($this->sTable);
		$this->CI->db->join('meta_groups', 'meta_groups.iMetaGroupId = metas.iMetaGroupId', 'left');

		$this->where($aRequest, 'iMetaId','metas');
		$this->where($aRequest, 'sCode','meta_groups');
		
		$this->order($aRequest);
		$this->limit($aRequest);

		$this->search($this->aDtCols,$aRequest);

		$aData 		= $this->CI->db->get();
		$aFoundRows = $this->getFoundRows();
		$aResult	= $this->getResult($aData->result_array(),$aRequest);

		return (isset($aArg['sType']) && $aArg['sType'] == 'datatables') ? 
			(
				array(
					'data'	 			=> $aResult,
					'recordsTotal' 		=> $aFoundRows['iTotalRows'],
					'recordsFiltered' 	=> $aFoundRows['iFilteredRows']
				)
			) : $aResult;
	}

	public function add($aArg)
	{
		$aRequest = $aArg['aRequest'];
		$aData = $this->insert($aRequest);
		return $aData['id'];
	}

	public function edit($aArg = array())
	{
		$aRequest = $aArg['aRequest'];
		$this->where($aRequest, 'iMetaId','metas');
		$this->update($aRequest);
	}
}