<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @name        db core controller
 * @author      Lloyd Gutierrez <jlgutierrez24@gmail.com>
 * @version		1.0.0
 */

class db_core
{
	var $CI			= NULL;

	public function __construct()
	{
		$this->CI =& get_instance();
	}

	/**
	 * [select description]
	 * @param  array  $aArg [description]
	 * @return [type]       [description]
	 */
	
	protected function select($aArg = array(), $aColumns = array())
	{
		$aSelect = array();
		foreach($aColumns as $key => $val) if(in_array($key, array_values($aArg) )) array_push($aSelect, $aColumns[$key] );
		return implode((count($aSelect) > 1 ? ',' : ''), array_values($aSelect) );
	}

	protected function construct($aArg = array(),$sColumns = array())
	{

		//array_merge(array_flip($aArg['aColumns']),$this->aColumns)
	}
	
	protected function where($aArg = array(),$sKey, $sTable = NULL)
	{
		if(!isset($aArg[$sKey])) return;
		
		$mKey = $aArg[$sKey];
		$sAction = is_array($mKey) ? 'where_in' : 'where';

		$this->CI->db->$sAction($sTable.'.'.$sKey,$mKey);
	}

	/**
	 * [order description]
	 * @param  array  $aArg [description]
	 * @return [type]       [description]
	 */
	protected function order($aArg = array())
	{
		isset($aArg['aOrder']) && $this->CI->db->order_by(is_array($aArg['aOrder']) ? implode(', ', $aArg['aOrder']) : $aArg['aOrder']);
	}
	/**
	 * [order description]
	 * @param  array  $aArg [description]
	 * @return [type]       [description]
	 */
	protected function group($aArg = array())
	{
		isset($aArg['aGroup']) && $this->CI->db->group_by(is_array($aArg['aGroup']) ? implode(', ', $aArg['aGroup']) : $aArg['aGroup']);
	}

	/**
	 * [limit description]
	 * @param  array  $aArg [description]
	 * @return [type]       [description]
	 */
	protected function limit($aArg = array())
	{
		if(isset( $aArg['aLimit']) ) $this->CI->db->limit( $aArg['aLimit']['length'], $aArg['aLimit']['iStart'] );
	}
	/**
	 * [search description]
	 * @param  array  $aDtCols [description]
	 * @param  array  $aArg    [description]
	 * @return [type]          [description]
	 */
	protected function search($aDtCols = array(),$aArg = array())
	{
		if( !isset($aArg['sSearch']) || (isset($aArg['sSearch']) && $aArg['sSearch']['value'] == "") ) return;

		$aLike = array();
		foreach( $aDtCols as $key => $sVal ){
			if( !strpos( $sVal,'as') ) {
				array_push($aLike, $sVal.' LIKE "%'. $aArg['sSearch']['value'] .'%"' );
			}
		}
			
		$this->CI->db->where('('. implode(' OR ',$aLike) .')');
	}

	/**
	 * Compose the result from query
	 * @param  array  $aRes : Query result
	 * @param  array  $aArg : Request
	 * @return array        : return depends on request format
	 */
	protected function getResult($aRes = array(),$aArg = array())
	{
		$aReturn = array();

		foreach($aRes as $aRows)
		{
			if(isset($aArg['sColumn'])) 
			{	
				if(isset($aArg['sIndex'])) $aReturn[$aRows[$aArg['sIndex']]] = $aRows[$aArg['sColumn']];
				else $aReturn[] = $aRows[$aArg['sColumn']];
			}
			else
			{
				if(isset($aArg['sIndex'])) $aReturn[$aRows[$aArg['sIndex']]] = $aRows;
				else $aReturn[] = $aRows;
			}
		}
		
		return $aReturn;
	}

	/**
	 * Gets row count and all the filtered rows of the last query 
	 * @return Array 	: total rows and filtered rows
	 */
	protected function getFoundRows()
	{
		$aRows = array();

		$oQuery			= $this->CI->db->query('SELECT FOUND_ROWS() iTotalRows');
		$aTotalRows		= $oQuery->first_row();
		$oQuery->free_result();

		$oQuery			= $this->CI->db->query('SELECT FOUND_ROWS() iFilteredRows');
		$aFilteredRows	= $oQuery->first_row();

		return array( 'iTotalRows' => $aTotalRows->iTotalRows, 'iFilteredRows' => $aFilteredRows->iFilteredRows );
	}

	protected function insert($aArg = array())
	{
		$this->CI->db->insert(
			$this->sTable,
			array_intersect_key(
				array_merge(
					$this->defaults(), 
					$aArg
				),
				$this->aFields
			)
		);

		return array('id' => $this->CI->db->insert_id());
	}

	protected function update($aArg = array())
	{
		if(isset($aArg[$this->sIndex])) unset($aArg[$this->sIndex]);
		if( count(array_intersect_key($aArg,$this->aFields)) == 0) return ;

        $this->CI->db->update(
        	$this->sTable,
        	array_intersect_key($aArg,$this->aFields)
		);
	}

	protected function delete($aArg = array())	
	{
		$this->CI->db->delete($this->sTable);
	}

	private function defaults()
	{
		return array(
				'iDateCreated' => (new DateTime())->getTimestamp(),
				'iLastUpdate'  => (new DateTime())->getTimestamp()				
			);
	}
}