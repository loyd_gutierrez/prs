<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tools_access
{
	public $aAccess	= array();
	public $iUserId	= null;

	public function __construct($aArg = array())
	{
		$this->CI	=& get_instance();
		$this->set($aArg);
	}


	public function get()
	{
		return ($this->aAccess = $this->CI->config->item('aAccess'));
	}

	public function set($aArg)
	{
		// $this->CI->config->set_item('aAccess', $this->CI->db_main_access->get(array(
		// 	'iUserId'	=> (
		// 		$this->iUserId	= $aArg['iUserId']
		// 	)
		// )));

		// return $this->get($aArg);
	}

	public function check($aArg)
	{
		//$aAccess = ;



		return (
			$this->valid($aArg) ?
			array(
				'iErr'	=> 0,
				'aData'	=> array(
					'aAccess'	=> $this->aAccess[$aArg['sModule']]
				)
			) :
			array(
				'iErr'		=> 2001,
				'sTitle'	=> ucfirst($aArg['sPermission']).' '.$aArg['sModule'],
				'sMsg'		=> 'Pemission Denied'
			)
		);
	}
	
	public function valid($aArg)
	{
		return isset($this->aAccess[$aArg['sModule']][$aArg['sPermission']]) && (int)$this->aAccess[$aArg['sModule']][$aArg['sPermission']];
	}
}