<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tools_sessions
{

	public function __construct($aArg = array())
	{
		$this->CI	=& get_instance();
	}

	public function get($sSession)
	{
		return $this->CI->session->userdata($sSession);
	}

	public function set($aSession)
	{
		if(!$aSession) return null;

		$aSession['logged_in'] = TRUE;
		$this->CI->session->set_userdata($aSession);

		return $aSession;
	}

	public function logout()
	{
		$this->CI->session->sess_destroy();
	}

	public function logged($sRemote = null)
	{
		return $this->CI->session->userdata('logged_in');
	}
}