<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Backend for Datatables
 */

class Model_list extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	function get($aArg = array())
	{
		/*Check if still Logged*/
		
		if(!$this->tools_sessions->logged())
		{
			return array(
				'bNotify' 	=> TRUE,
				'iError'   	=> 1005,
				'sMessage' 	=> 'Session Expired. Please log in again'
			);
		}	
		
		/*Check Permission*/
		$aPermissions = $this->tools_sessions->get('aPermissions');
		
		if(!in_array($aArg['sModule'],array_keys($aPermissions)) || (in_array($aArg['sModule'],array_keys($aPermissions)) && !in_array('Read',array_keys($aPermissions[$aArg['sModule']]))) ){
			return array(
				'bNotify' 	=> TRUE,
				'iError'   	=> 1006,
				'sMessage' 	=> 'Access is denied from the server'
			);
		}
		
		$this->load->library('db/'.$aArg['sGroup'].'/'.($sClass = 'db_'.strtolower($aArg['sGroup']).'_'.strtolower($aArg['sTableName']) ));
		
		return (
			array(
				'aData' => $this->$sClass->get($aArg)
			)
		);
	}

	function getAll($aArg = array())
	{

		$this->load->library('db/'.$aArg["sGroup"]."/".($sClass = $aArg["sTableName"]));
		//$this->$sClass->getAll($aArg);
	}
}