<?php defined('BASEPATH') OR exit('No direct script access allowed');

class main extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function get($aArg = array())
	{
		if(!isset($aArg['sType']) || (isset($aArg['sType']) && $aArg['sType'] != 'init' && $aArg['sType'] != 'Public' )){
			$aPermissions = $this->tools_sessions->get('aPermissions');
		
			if(!in_array($aArg['sModule'],array_keys($aPermissions)) || (in_array($aArg['sModule'],array_keys($aPermissions)) && !in_array('Read',array_keys($aPermissions[$aArg['sModule']]))) ){
				return array(
					'bNotify' 	=> TRUE,
					'iError'   	=> 1006,
					'sMessage' 	=> 'Access is denied from the server'
				);
			}
		}
		/*PERMISSION CHECK*/
		$aReturn = array();

		if(isset($aArg['aRequest']) && $aRecords = $this->process('db/'.$aArg['sGroup'].'/'.($sClass = 'db_'.strtolower($aArg['sGroup']).'_'.strtolower($aArg['sTableName']) ), $sClass, $aArg))
				$aReturn['aRequest'] = $aRecords;

		if(isset($aArg['aBundles']) && $aBundles = $this->bundles($aArg['aBundles']))
			$aReturn['aBundles'] = isset($aArg['aBundles']) ? $aBundles : null;

		$aContents = (isset($aArg['sType']) && $aArg['sType'] == 'init') ? $this->getContents($aRecords) : null;
		if($aContents) $aReturn['aContents'] = $aContents;

		return $aReturn;
	}

	/**
	 * Add
	 * @param array $aArg [description]
	 */
	function save($aArg = array())
	{
		$aPermissions = $this->tools_sessions->get('aPermissions');
		
		if(!isset($aArg['sType']) || (isset($aArg['sType']) &&  $aArg['sType'] != 'Public')){
			if(!in_array($aArg['sModule'],array_keys($aPermissions)) || (in_array($aArg['sModule'],array_keys($aPermissions)) && !in_array( ($aArg['aRequest']['mode'] == 'edit' ? 'Update' : 'Create') ,array_keys($aPermissions[$aArg['sModule']]))) ){
				return array(
					'bNotify' 	=> TRUE,
					'iError'   	=> 1006,
					'sMessage' 	=> 'Access is denied from the server'
				);
			}
		}

		$this->load->library('db/'.$aArg['sGroup'].'/'.($sClass = 'db_'.strtolower($aArg['sGroup']).'_'.strtolower($aArg['sTableName']) ));

		$aArg = $this->$sClass->$aArg['aRequest']['mode']($aArg);
		
		return array(
				'aData' 	=> $aArg ? $aArg : null,
				'iErr' 		=> 0,
				'sMessage' 	=> 'Success' 
			);		
	}
	
	function sms($aArg = array())
	{
		/**
		 * Get From DB: Doctor's name and contact, Patient's name and contact, schedule time.
		 * 
		 */
		
		// $this->tools_chikka_sms->sendText('','','Good day! this is to remind you for your schedule ');
		// ('1234561', '6391561866732', 'tests')
	}

	/**
	 * Loads library then gets the requested action from it
	 * @param  [string] $sLibrary : Library name to be loaded
	 * @param  [string] $sClass   : class name under the library
	 * @param  [array]  $aArg     : arguments that contains requested data
	 * @return array              : sql result
	 */
	protected function process($sLibrary, $sClass, $aArg = array())
	{
	
		if(!class_exists($sClass)) $this->load->library($sLibrary);

		return $this->$sClass->get($aArg);
	}

	protected function bundles($aArg = array())
	{
		if(!$aArg) return;

		$aBundles = array();
		
		foreach($aArg as $key => $arr){
			array_push($aBundles, $this->process('db/'.$arr['sGroup'].'/'.($sClass = 'db_'.strtolower($arr['sGroup']).'_'.strtolower($arr['sTableName']) ), $sClass, $arr));
		}
		return $aBundles;
	}

	/**
	 * Get's the forms and dashboards
	 * @param  array  $aArg : contains models which will be used in getting forms and dashboards
	 * @return array       : arrays of forms and dashboard html elements
	 */
	protected function getContents($aArg = array())
	{
		$aResponse = array();

		foreach($aArg as $key => $aArr)
		{
			file_exists('application/views/modals/modals_'. strtolower($aArr['sTableName']) .'.php') &&
				$aResponse[$aArr['sModuleName']]['form'] = $this->load->view('modals/modals_'. strtolower($aArr['sTableName']),null,true);

			file_exists('application/views/dashboards/dashboards_'. strtolower($aArr['sTableName']) .'.php') && 
				$aResponse[$aArr['sModuleName']]['dashboard'] = $this->load->view('dashboards/dashboards_'. strtolower($aArr['sTableName']),null,true);
			$aResponse[$aArr['sModuleName']]['sTableName'] = $aArr['sTableName'];
		}
		
		return $aResponse;
	}

	private function getPermissions($iModuleId)
	{

	}
}