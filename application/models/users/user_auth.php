<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User_auth extends Api
{

	function __construct()
	{
		parent::__construct();
		$this->load->library('db/Admin/db_admin_role_permissions');
	}

	function info(){
		return array(
			'iUserId'   	=> $this->tools_sessions->get('iUserId'),
			'username'  	=> $this->tools_sessions->get('username'),
			'sFullName'  	=> $this->tools_sessions->get('sFullName'),
			'sRole'  		=> $this->tools_sessions->get('sRole'),
		);		
	}

	function login($aArg = array())
	{
		if(!$aArg) return;

		$aData = $this->db_admin_users->get(
			array('aRequest' => array(
				'sUsername' => $aArg['aRequest']['sUsername'],
				'sPassword' => md5($aArg['aRequest']['sPassword']),
				'bActive'  => 1,
				'aColumns' => array('iUserId','sUsername','sFullName','sRoleName','iRoleId','iDoctorId')
			))
		);

		if($aData)
		{
			$this->tools_sessions->set(
				array(
					'iUserId'   	=> $aData[0]['iUserId'],
					'iDoctorId'   	=> $aData[0]['iDoctorId'],
					'username'  	=> $aData[0]['sUsername'],
					'sFullName'  	=> $aData[0]['sFullName'],
					'sRole'  		=> $aData[0]['sRoleName'],
					'logged_in' 	=> TRUE,
					'iDateTime' 	=> strtotime(date('Y-m-d')),
					'aPermissions' 	=> $this->db_admin_role_permissions->get(
						array('aRequest' => 
							array(
								'iRoleId' 		=> $aData[0]['iRoleId'],
								'bActive'  		=> 1,
								'bPermissions' 	=> TRUE,
								'sIndex'   		=> 'sModuleName',
								'sColumn'  		=> 'iPermission',
								'aColumns' 		=> array(
									'sModuleName','iPermission'
								)
							)
						)
					)
				)
			);

			return array(
				'sUserName'  	=> $aData[0]['sUsername'],
				'sRole'  		=> $aData[0]['sRoleName']
			);
		}
		else
		{
			return array(
				'bNotify'  => TRUE,
				'iError'   => 1004,
				'sMessage' => 'Username and Password did not match'
			);
		}
	}

	function logout()
	{	
		$this->tools_sessions->logout();
		return array(
				'sMessage' => 'success'
			);
	}
}