<div name="Roles" class="row" role="list" style="display: none;">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Roles</h3>
			</div>
			<div class="box-body">
				<table name="Roles_Datatables" class="col-sm-12 table table-striped table-hover dataTable no-footer"></table>
			</div>
		</div>
	</div>
</div>
