<div name="Dashboard" class="row" role="list" style="display: none;">
	<div class="row" name="DashboardCounts">
		<div class="col-xs-12">
        	<div class="col-lg-3 col-xs-6">
	          <!-- small box -->
	          <div class="small-box bg-aqua">
	            <div class="inner" name="iScheduledCount">
	              <h3>150</h3>
	              <p>Scheduled</p>
	            </div>
	            <div class="icon">
	              <i class="fa fa-calendar-check-o"></i>
	            </div>
	            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	          </div>
	        </div>
	        <!-- ./col -->
	        <div class="col-lg-3 col-xs-6">
	          <!-- small box -->
	          <div class="small-box bg-green">
	            <div class="inner"  name="iDoctorsCount">
	              <h3>53<sup style="font-size: 20px">%</sup></h3>

	              <p>Doctors</p>
	            </div>
	            <div class="icon">
	              <i class="fa fa-user-md"></i>
	            </div>
	            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	          </div>
	        </div>
	        <!-- ./col -->
	        <div class="col-lg-3 col-xs-6">
	          <!-- small box -->
	          <div class="small-box bg-yellow">
	            <div class="inner" name="iNursesCount">
	              <h3>44</h3>

	              <p>Users/Nurses</p>
	            </div>
	            <div class="icon">
	              <i class="fa fa-users"></i>
	            </div>
	            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	          </div>
	        </div>
	        <!-- ./col -->
	        <div class="col-lg-3 col-xs-6">
	          <!-- small box -->
	          <div class="small-box bg-red">
	            <div class="inner" name="iPatientsCount">
	              <h3>65</h3>

	              <p>Patients</p>
	            </div>
	            <div class="icon">
	              <i class="fa fa-wheelchair"></i>
	            </div>
	            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	          </div>
	        </div>
        	<!-- ./col -->
      	</div>
	</div>
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Transactions </h3>
			</div>
			<div class="box-body">
				<table name="Transactions_Datatables" class="col-sm-12 table table-striped table-hover dataTable no-footer"></table>
			</div>
		</div>
	</div>
</div>
