<div name="Users" class="row" role="list" style="display: none;">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Users</h3>
			</div>
			<div class="box-body">
				<table name="Users_Datatables" class="col-sm-12 table table-striped table-hover dataTable no-footer"></table>
			</div>
		</div>
	</div>
</div>
