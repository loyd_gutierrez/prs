<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Divine Love General Hospital </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="assets/plugins/admin_lte/bootstrap/css/bootstrap.min.css">
  <!-- Datatables -->
  <link rel="stylesheet" href="assets/plugins/admin_lte/plugins/datatables/dataTables.bootstrap.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="assets/fonts/font_awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="assets/fonts/ion_icons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="assets/plugins/admin_lte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="assets/plugins/admin_lte/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="assets/plugins/admin_lte/plugins/iCheck/all.css">
  <!-- Toggle master -->
  <link rel="stylesheet" href="assets/plugins/bootstrap-toggle-master/css/bootstrap-toggle.min.css">
  <!-- Toaster Master -->
  <link rel="stylesheet" href="assets/plugins/toastr-master/build/toastr.min.css">
  
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="assets/plugins/admin_lte/plugins/timepicker/bootstrap-timepicker.min.css">
  <!--  -->
  <link rel="stylesheet" href="assets/css/body.css">
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-black sidebar-mini" name="main">
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><img src="assets/img/logo.png"></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg" style="font-size:18px"> <b>Divine Love</b> <span style="font-size:12px;">General Hospital</span></span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <li class="user user-menu">
              <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                <img src="assets/img/avatar.png" class="user-image" alt="User Image">
                <span class="hidden-xs" name="sFullName"></span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  <img src="assets/img/avatar.png" class="img-circle" alt="User Image">
                  <p name="sFullName"></p>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-right">
                    <a name="logout" href="javascript:void(0)" class="btn btn-default btn-flat">Sign out</a>
                  </div>
                </li>
              </ul>
            </li> 
          </ul>
        </div>
      </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">

          <li>
            <a name="Dashboard" href="javascript:void(0)">
              <i class="fa fa-tachometer"></i> <span>Dashboard</span>
            </a>
          </li>
          <li class="treeview">
            <a href="javascript:void(0)">
              <i class="fa fa-hospital-o"></i>
              <span>Management</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li><a name="AppointmentTypes" href="javascript:void(0)"><i class="fa fa-stethoscope"></i> Appointment Types </a></li>
              <li><a name="Clinics" href="javascript:void(0)"><i class="fa fa-h-square"></i> Clinics/Rooms </a></li>
              <li><a name="Doctors" href="javascript:void(0)"><i class="fa fa-user-md"></i> Doctors </a></li>
              <li><a name="Metas" href="javascript:void(0)"><i class="fa fa-cog"></i> Metas </a></li>
              <li><a name="MetaGroups" href="javascript:void(0)"><i class="fa fa-cogs"></i> Meta Groups </a></li>
            </ul>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-lock"></i>
              <span>Administration</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li><a name="Users" href="javascript:void(0)"><i class="fa fa-users"></i> Users</a></li>
              <li><a name="Roles" href="javascript:void(0)"><i class="fa fa-black-tie"></i> Roles</a></li> 
              <li><a name="Modules" href="javascript:void(0)"><i class="fa fa-puzzle-piece"></i> Modules</a></li>
              <li><a name="Permissions" href="javascript:void(0)"><i class="fa fa-key"></i> Permissions</a></li>
            </ul>
          </li>
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Dashboard
        </h1>
      </section>

      <!-- Main content -->
      <section class="content widget">
        <div class="col-xs-12" name="Modules"></div>
      </section>
      <!-- /.content -->
      
      <!-- Modals Forms-->
      <div name="forms"></div>
      <!-- /.Modals -->
      
      <!-- Confirm -->
      <div name="confirm" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <form class="modal-content" onsubmit="return false">
            <div class="modal-header customhdr">
              <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
              <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
              <button name="Cancel" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button name="Confirm" type="submit" class="btn btn-primary">Confirm</button>
            </div>
          </form>
        </div>
      </div>
      <!-- /.Confirm -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
      </div>
      <strong>Copyright &copy; 2016 </strong> All rights
      reserved.
    </footer>
  </div>
  <!-- ./wrapper -->

  <!-- <!-- Bootstrap 3.3.6 
  <script src="assets/plugins/admin_lte/bootstrap/js/bootstrap.min.js"></script> -->
  <!-- jQuery 2.2.0 -->
  <script src="assets/plugins/admin_lte/plugins/jQuery/jQuery-2.1.4.min.js"></script>
  <!-- Datatables -->
  <script src="assets/plugins/admin_lte/plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="assets/plugins/admin_lte/plugins/datatables/dataTables.bootstrap.min.js"></script>
  <!-- daterangepicker -->
  <script src="assets/plugins/admin_lte/bootstrap/js/bootstrap.min.js"></script>
  <!-- Slimscroll -->
  <script src="assets/plugins/admin_lte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="assets/plugins/admin_lte/plugins/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="assets/plugins/admin_lte/dist/js/app.min.js"></script>
  <!-- Toggle Master -->
  <script src="assets/plugins/bootstrap-toggle-master/js/bootstrap-toggle.min.js"></script>
  <!-- Validation -->
  <script src="assets/plugins/validation/dist/jquery.validate.min.js"></script>
  <!-- iCheck -->
  <script src="assets/plugins/admin_lte/plugins/iCheck/iCheck.min.js"></script>
  <!-- bootstrap Date picker -->
  <script src="assets/plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
  <!-- bootstrap time picker -->
  <script src="assets/plugins/admin_lte/plugins/timepicker/bootstrap-timepicker.min.js"></script>
  <!-- Toastr Master -->
  <script src="assets/plugins/toastr-master/build/toastr.min.js"></script>
  <!-- php.js -->
  <script src="assets/js/oPhp.js"></script>
  <!-- PRS App -->
  <script src="assets/js/app.js"></script>
</body>
</html>