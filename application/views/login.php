<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PRS </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="assets/plugins/admin_lte/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="assets/fonts/font_awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="assets/fonts/ion_icons/css/ionicons.min.css">
  <!-- Toaster Master -->
  <link rel="stylesheet" href="assets/plugins/toastr-master/build/toastr.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="assets/plugins/admin_lte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="assets/plugins/admin_lte/dist/css/skins/_all-skins.min.css">
  
  <!--  -->
  <link rel="stylesheet" href="assets/css/body.css">
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page" name="login">
  <div class="login-box">
    <div class="login-logo">
      <img src="assets/img/logo.png">
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
      <p class="login-box-msg">Sign in to start your session</p>

      <form action="index.html" method="post">
        <div class="form-group has-feedback">
          <input name = "sUsername" type="text" class="form-control" placeholder="Email">
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input name = "sPassword" type="password" class="form-control" placeholder="Password">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
          <div class="col-xs-8"></div>
          <!-- /.col -->
          <div class="col-xs-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
  <!-- /.login-box-body -->
  </div>
  <!-- /.login-box -->

  <!-- <!-- Bootstrap 3.3.6 
  <script src="assets/plugins/admin_lte/bootstrap/js/bootstrap.min.js"></script> -->
  <!-- jQuery 2.2.0 -->
  <script src="assets/plugins/admin_lte/plugins/jQuery/jQuery-2.1.4.min.js"></script>
  <!-- Validation -->
  <script src="assets/plugins/validation/dist/jquery.validate.min.js"></script>
  <!-- Toastr Master -->
  <script src="assets/plugins/toastr-master/build/toastr.min.js"></script>
  <!-- php.js -->
  <script src="assets/js/oPhp.js"></script>
  <!-- PRS App -->
  <script src="assets/js/app.js"></script>
</body>
</html>