<div name="Doctors" path="Doctors/Doctors" class="modal fade DataTableModal" role="dialog" style="display: none;">
	<div class="modal-dialog" style="width:1000px">
		<form class="modal-content" onsubmit="return false">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
				<h4 class="modal-title"><span></span> Doctors</h4>
			</div>
			<div class="modal-body">
				<div class="form-horizontal">
					<input name="iDoctorId" type="hidden">
					<div class="box">
						<div class="box-header with-border">Doctors Information</div>
						<div class="box-body">
							<div class="row">
								<div class="col-sm-3">
									<div class="form-group col-sm-12">
										<label class="control-label">Active</label>
										<select name="bActive" class="form-control" field-type="select" field-default="1">
											<option value="1">Active</option>
											<option value="0">Inactive</option>
										</select>	
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group col-sm-12">
										<label class="control-label">First Name</label>
										<input name="sFirstName" class="form-control" type="text">	
									</div>	
								</div>
								<div class="col-sm-4">
									<div class="form-group col-sm-12">
										<label class="control-label">Middle Name </label>
										<input name="sMiddleName" class="form-control" type="text">
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group col-sm-12">
										<label class="control-label">Last Name</label>
										<input name="sLastName" class="form-control" type="text">	
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group col-sm-12">
										<label class="control-label">Gender</label>
										<select name="iGenderId" class="form-control" field-type="select" field-default="1"></select>
									</div>	
								</div>
								<div class="col-sm-6">
									<div class="form-group col-sm-12">
										<label class="control-label">Civil Status</label>
										<select name="iCivilStatusId" class="form-control" field-type="select" field-default="1"></select>
									</div>	
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group col-sm-12">
										<label class="control-label">Room</label>
										<select name="iClinicId" class="form-control" field-type="select" field-default="1"></select>
									</div>	
								</div>
								<div class="col-sm-6">
									<div class="form-group col-sm-12">
										<label class="control-label">Contact Number</label>
										<input name="sContactNumber" class="form-control" type="text">	
									</div>	
								</div>
							</div>
						</div>
					</div>
					<div  name="Schedules" class="box">
						<div class="box-header with-border">
							<label>Schedule</label>
							<div class="box-button pull-right">
								<button name="oAddSchedule" action="add" type="button" class="btn btn-default btn-sm fa" field-target="iSchedule">&#xf067;</button>
							</div>
						</div>
						<div class="box-body"></div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button action="cancel" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button action="save" type="submit" class="btn btn-primary">Save Changes</button>
			</div>
		</form>
	</div>
</div>
