<div name="MetaGroups" path="MetaGroups/MetaGroups" class="modal fade DataTableModal" role="dialog" style="display: none;">
	<div class="modal-dialog">
		<form class="modal-content" onsubmit="return false">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
				<h4 class="modal-title"><span></span> Meta Groups</h4>
			</div>
			<div class="modal-body">
				<div class="form-horizontal">
					<input name="iMetaGroupId" type="hidden">
					<div class="form-group">
						<label class="col-sm-3 control-label">Active</label>
						<div class="col-sm-3">
							<select name="bActive" class="form-control" field-type="select" field-default="1">
								<option value="1">Active</option>
								<option value="0">Inactive</option>
							</select>	
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Code</label>
						<div class="col-sm-9">
							<input name="sCode" class="form-control" type="text">	
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Name</label>
						<div class="col-sm-9">
							<input name="sName" class="form-control" type="text">	
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button action="cancel" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button action="save" type="submit" class="btn btn-primary">Save Changes</button>
			</div>
		</form>
	</div>
</div>
