<div name="Modules" path="AddressTables/AddressTables" class="modal fade DataTableModal" role="dialog" style="display: none;">
	<div class="modal-dialog">
		<form class="modal-content" onsubmit="return false">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
				<h4 class="modal-title"><span></span> Modules</h4>
			</div>
			<div class="modal-body">
				<div class="form-horizontal">
					<div class="box">
						<div class="box-header"> Modules </div>
						<div class="box-body">
							<input name="iModuleId" type="hidden">
						
							<div class="row">
									<div class="form-group">
									<label class="col-sm-3 control-label">Active</label>
									<div class="col-sm-3">
										<select name="bActive" class="form-control" field-type="select" field-default="1">
											<option value="1">Active</option>
											<option value="0">Inactive</option>
										</select>	
									</div>

									<label class="col-sm-3 control-label">Onload</label>
									<div class="col-sm-3">
										<select name="bOnload" class="form-control" field-type="select"  field-default="1">
											<option value="1">Yes</option>
											<option value="0">No</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group">
									<label class="col-sm-3 control-label">Module Name</label>
									<div class="col-sm-9">
										<input name="sModuleName" class="form-control" type="text" required>	
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group">
									<label class="col-sm-3 control-label fa">Module Title</label>
									<div class="col-sm-9">
										<input name="sTitle" class="form-control" type="text">	
									</div>
									
								</div>
							</div>
							<div class="row">
								<div class="form-group">
									<label class="col-sm-3 control-label fa">Table Name</label>
									<div class="col-sm-9">
										<input name="sTableName" class="form-control" type="text">	
									</div>
									
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<input name="iPermissionId" type="hidden" field-module="Permissions" field-name="cbPermissions" field-type="Checkbox" field-default="0">
						<div class="box">
							<div class="box-header with-border">
								<h3 class="box-title"> Permissions </h3>
							</div>
							<div class="box-body"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button action="cancel" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button action="save" type="submit" class="btn btn-primary">Save Changes</button>
			</div>
		</form>
	</div>
</div>
