<div name="Roles" path="Roles/Roles" class="modal fade DataTableModal" role="dialog" style="display: none;">
	<div class="modal-dialog">
		<form class="modal-content" onsubmit="return false">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
				<h4 class="modal-title"><span></span> Roles</h4>
			</div>
			<div class="modal-body">
				<div class="form-horizontal">
					<input name="iRoleId" type="hidden">
					<div class="form-group">
						<label class="col-sm-3 control-label fa">Active</label>
						<div class="col-sm-9">
							<select name="bActive" class="form-control" field-type="select" field-default="1">
								<option value="1">Active</option>
								<option value="0">Inactive</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Name</label>
						<div class="col-sm-9">
							<input name="sRoleName" class="form-control" type="text" required>
						</div>
					</div>
				</div>
				<div class="form-group">
					<input name="iModuleId" type="hidden" field-module="Modules" field-name="cbModules" field-type="Checkbox" field-default="0">
					<div class="box box-default">
						<div class="box-header with-border">
							<h3 class="box-title"> Modules </h3>
						</div>
						<div class="box-body"></div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button action="cancel" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button action="save" type="submit" class="btn btn-primary">Save Changes</button>
			</div>
		</form>
	</div>
</div>
