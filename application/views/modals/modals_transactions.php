<div name="Dashboard" path="Dashboard/Dashboard" class="modal fade DataTableModal" role="dialog" style="display: none;">
	<div class="modal-dialog" style="width:1000px !important">
		<form class="modal-content" onsubmit="return false">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
				<h4 class="modal-title"><span></span> Clinics</h4>
			</div>
			<div class="modal-body">
				<div class="form-horizontal">
					<input name="iPatientId" class="form-control" type="hidden">
					<input name="iTransactionId" class="form-control" type="hidden">
					<div class="box">
						<div class="box-header with-border">
							<h4>Main Information</h4>
						</div>
						<div class="box-body">
							<div class="row">
								<div class="col-sm-4">
									<label class="control-label">Last Name</label>
									<input name="sLastName" class="form-control" type="text">	
								</div>
								<div class="col-sm-4">
									<label class="control-label">First Name</label>
									<input name="sFirstName" class="form-control" type="text">
								</div>
								<div class="col-sm-4">
									<label class="control-label">Middle Name</label>
									<input name="sMiddleName" class="form-control" type="text">	
								</div>
							</div>

							<div class="row">
								<div class="col-sm-4">
									<label class="control-label"> Gender </label>
									<select name="iGenderId" class="form-control" field-type="select"></select>
								</div>
								<div class="col-sm-4">
									<label class="control-label"> Civil Status </label>
									<select name="iCivilStatusId" class="form-control" field-type="select"></select>
								</div>
								<div class="col-sm-4">
									<label class="control-label">Contact Number</label>
									<div class="input-group">
										<div class="input-group-addon">
											<i class="fa fa-phone"></i>
										</div>
										<input name="sContactNumber" class="form-control" type="text">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-4">
									<label class="control-label"> Municipality </label>
									<select name="iProvinceId" chain-module="Cities" chain-name="iCityId" class="form-control" field-type="select"></select>
								</div>
								<div class="col-sm-4">
									<label class="control-label"> City </label>
									<select name="iCityId" class="form-control" field-type="select"></select>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-12">
									<label class="control-label"> Address </label>
									<textarea name="sAddress" class="form-control"></textarea>
								</div>
							</div>
						</div>
					</div>

					<div class="box">
						<div class="box-header"> <h4>Reservation</h4> </div>
						<div class="box-body">
							<div class="row">
								<div class="col-sm-3">
									<label class="control-label">Appointment</label>
									<select name="iAppointmentTypeId" class="form-control" field-type="select"></select>
								</div>
								<div class="col-sm-3">
									<label class="control-label">Doctor</label>
									<select name="iDoctorId" class="form-control" field-type="select" chain-module="DoctorSchedules" chain-name="sDay"></select>
								</div>
								<div class="col-sm-3">
									<label class="control-label">Doctor's Schedule</label>
									<select name="sDay" class="form-control" field-type="select"></select>
									<input name="sDayValue" type="hidden">
								</div>
								<div class="col-sm-3">
									<label class="control-label">Time</label>
									<div class="input-group bootstrap-timepicker">
 	 									<input name="iTime" type="text" class="form-control timepicker" field-type="Timepicker" readonly="readonly">
										<div class="input-group-addon">
						                   	<i class="fa fa-clock-o"></i>
						                </div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<label class="control-label">Remarks</label>
									<textarea name="sRemarks" class="form-control"></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button action="cancel" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button action="save" type="submit" class="btn btn-primary">Save Changes</button>
			</div>
		</form>
	</div>
</div>
