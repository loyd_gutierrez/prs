<div name="Users" path="Users/Users" class="modal fade DataTableModal" role="dialog" style="display: none;">
	<div class="modal-dialog">
		<form class="modal-content" onsubmit="return false">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
				<h4 class="modal-title"><span></span> Users</h4>
			</div>
			<div class="modal-body">
				<div class="form-horizontal">
					<input name="iUserId" type="hidden">

					<div class="form-group">
						<label class="col-sm-3 control-label">Username</label>
						<div class="col-sm-9">
							<input name="sUsername" class="form-control" type="text">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Password</label>
						<div class="col-sm-9">
							<input name="sPassword" class="form-control" type="password">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Roles</label>
						<div class="col-sm-9">
							<select name="iRoleId" class="form-control" field-type="select" field-default="1"></select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Assigned to</label>
						<div class="col-sm-9">
							<select name="iDoctorId" class="form-control" field-type="select" field-default="1"></select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">ID</label>
						<div class="col-sm-9">
							<input name="sId" class="form-control" type="text">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label fa">Last Name</label>
						<div class="col-sm-9">
							<input name="sLastName" class="form-control" type="text" required>	
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">First Name</label>
						<div class="col-sm-9">
							<input name="sFirstName" class="form-control" type="text" required>	
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Middle Name</label>
						<div class="col-sm-9">
							<input name="sMiddleName" class="form-control" type="text" required>	
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button action="cancel" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button action="save" type="submit" class="btn btn-primary">Save Changes</button>
			</div>
		</form>
	</div>
</div>
