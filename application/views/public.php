<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Divine Love General Hospital </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="assets/plugins/admin_lte/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="assets/fonts/font_awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="assets/fonts/ion_icons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="assets/plugins/admin_lte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="assets/plugins/admin_lte/dist/css/skins/_all-skins.min.css">
  <!-- Toggle master -->
  <link rel="stylesheet" href="assets/plugins/bootstrap-toggle-master/css/bootstrap-toggle.min.css">
  <!-- Toaster Master -->
  <link rel="stylesheet" href="assets/plugins/toastr-master/build/toastr.min.css">
  
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="assets/plugins/admin_lte/plugins/timepicker/bootstrap-timepicker.min.css">
  <!--  -->
  <link rel="stylesheet" href="assets/css/body.css">
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition" name="public">
  <div class="wrapper">
    <div class="register-box" style="width:900px;margin:2% auto;margin-bottom:0px;">
      <div class="register-logo" style="margin-bottom:10px;">
        <a href="javascript:void(0)">
          <b>Divine Love</b> General <br>Hospital
        </a>
      </div>
      

      <div class="register-box-body" name="Public" path="Public/Public" mode="add">
        <p class="login-box-msg">Complete these fields to process your reservation.</p>

        <form action="reserve" method="post" onsubmit="return false" name="reserve">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"> Main Information </h3>
              <div class="input-group col-sm-4 pull-right">
                <input name="sTransactionCode" type="text" class="form-control" placeholder="Transaction Code">
                <span class="input-group-btn">
                  <button name="SearchTransctionBtn" type="button" class="btn btn-info btn-flat">Search</button>
                </span>
              </div>
            </div>
            <div class="box-body">
              <div class="row">
                <input name="iPatientId" class="form-control" type="hidden">
                <input name="iTransactionId" class="form-control" type="hidden">
                <div class="col-sm-4">
                  <label class="control-label">Last Name</label>
                  <input name="sLastName" class="form-control" type="text"> 
                </div>
                <div class="col-sm-4">
                  <label class="control-label">First Name</label>
                  <input name="sFirstName" class="form-control" type="text">
                </div>
                <div class="col-sm-4">
                  <label class="control-label">Middle Name</label>
                  <input name="sMiddleName" class="form-control" type="text"> 
                </div>
              </div>

              <div class="row">
                <div class="col-sm-4">
                  <label class="control-label"> Gender </label>
                  <select name="iGenderId" class="form-control" field-type="select"></select>
                </div>
                <div class="col-sm-4">
                  <label class="control-label"> Civil Status </label>
                  <select name="iCivilStatusId" class="form-control" field-type="select"></select>
                </div>
                <div class="col-sm-4">
                  <label class="control-label">Contact Number</label>
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-phone"></i>
                    </div>
                    <input name="sContactNumber" class="form-control" type="text">
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-4">
                  <label class="control-label"> Municipality </label>
                  <select name="iProvinceId" chain-module="Cities" chain-name="iCityId" class="form-control" field-type="select"></select>
                </div>
                <div class="col-sm-4">
                  <label class="control-label"> City </label>
                  <select name="iCityId" class="form-control" field-type="select"></select>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-12">
                  <label class="control-label"> Address </label>
                  <textarea name="sAddress" class="form-control"></textarea>
                </div>
              </div>
            </div>
          </div>  
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"> Reservation </h3>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-sm-3">
                  <label class="control-label">Appointment</label>
                  <select name="iAppointmentTypeId" class="form-control" field-type="select"></select>
                </div>
                <div class="col-sm-3">
                  <label class="control-label">Doctor</label>
                  <select name="iDoctorId" class="form-control" field-type="select" chain-module="DoctorSchedules" chain-name="sDay"></select>
                </div>
                <div class="col-sm-3">
                  <label class="control-label">Doctor's Schedule</label>
                  <select name="sDay" class="form-control" field-type="select"></select>
                  <input name="sDayValue" type="hidden">
                </div>
                <div class="col-sm-3">
                  <label class="control-label">Time</label>
                  <div class="input-group bootstrap-timepicker">
                    <input name="iTime" type="text" class="form-control timepicker" field-type="Timepicker" readonly="readonly">
                    <div class="input-group-addon">
                        <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="box-footer">
              <button name="cancel" action="delete" type="button" class="btn btn-danger" data-field="bActive">Cancel Reservation</button>
              <button type="submit" class="btn btn-primary pull-right">Submit</button> 
            </div>
          </div>  
        </form>
      </div>
      <!-- /.form-box -->

      <!-- Confirm -->
      <div name="confirm" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <form class="modal-content" onsubmit="return false">
            <div class="modal-header customhdr">
              <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
              <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
              <button name="CancelReservation" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button name="Confirm" type="submit" class="btn btn-primary">Confirm</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- <!-- Bootstrap 3.3.6 
  <script src="assets/plugins/admin_lte/bootstrap/js/bootstrap.min.js"></script> -->
  <!-- jQuery 2.2.0 -->
  <script src="assets/plugins/admin_lte/plugins/jQuery/jQuery-2.1.4.min.js"></script>
  <!-- daterangepicker -->
  <script src="assets/plugins/admin_lte/bootstrap/js/bootstrap.min.js"></script>
  <!-- Validation -->
  <script src="assets/plugins/validation/dist/jquery.validate.min.js"></script>
  <!-- bootstrap Date picker -->
  <script src="assets/plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
  <!-- bootstrap time picker -->
  <script src="assets/plugins/admin_lte/plugins/timepicker/bootstrap-timepicker.min.js"></script>
  <!-- Toastr Master -->
  <script src="assets/plugins/toastr-master/build/toastr.min.js"></script>
  <!-- php.js -->
  <script src="assets/js/oPhp.js"></script>
  <!-- PRS App -->
  <script src="assets/js/app.js"></script>
</body>
</html>