$(document).ready(function(){
	$('body[name="login"]').length && oLogin.init();
	$('body[name="main"]').length && app.init();
	$('body[name="public"]').length && public.init();
});

window.oLogin = {
	init : function()
	{
		oTools.Toastr.init();

		$('body[name="login"] form').submit(function(e){
			var sPassword = $(this.sPassword).val();
			$(this.sPassword).val("");

			e.preventDefault();
			var oSelf = this;
			oData.send(oData.sUrl,{
				sMethod: 'users/User_auth/login',
				aRequest : {
					sUsername : $(this.sUsername).val(),
					sPassword : sPassword,
				}
			},function(aResp){
				
				if(!aResp.iError) location.reload();

				return false;
			});
		});
	},

	getPage : function()
	{
		this.isLogged();
	}
};


window.app = {
	init: function(){
		oTools.init();
		oModules.init();
	}
};

window.public = {
	init: function(){
		oTools.init();
		oModules.oWidgets.Public.aWidgets.Public.init( {sModuleName : 'Public'} );
	}
};

/**
 * [oModules description]
 * @type {Object}
 */
window.oModules = {

	/**
	 * Initialize function of the module; only runs once..
	 */
	init : function(){
		var oSelf = this;

		oData.send(oData.sUrl,{
			sModule: 'Modules',
			sMethod: 'main/get',
			sTableName: 'modules',
			sGroup: 'Admin',
			sType: 'init',
			aRequest: {
				aColumns : [
					'bOnload',
					'iModuleId',
					'sModuleName',
					'sTableName',
				]
			}
		},
		function(aResp){
			var oForms = $('div[name="forms"]');
			var oWidget = $('div[name="Modules"]');
			
			$.each(aResp.aContents, function(sKey,aRows){
				oForms.append(aRows['form']);
				oWidget.append(aRows['dashboard']);

				var aModule = (aResp.aRequest.find(function(aModule){
					return aModule.sModuleName === sKey && aModule.bOnload == 1;
				})); 
				
				oSelf.oWidgets[sKey].aWidgets[sKey]['sTableName'] = aRows['sTableName'];
				if(aModule){
					oSelf.oWidgets[sKey].init({ sModuleName : sKey });
				} 

			});
		});
	},

	load : function(aArg)
	{
		oModules.oWidgets[aArg.sModuleName].aWidgets[aArg.sModuleName].init(aArg);
	},

	oWidgets :
	{
		sTitle 		: null,

		reset : function()
		{
			$(oModules.sParent).find('.row').css('display','none');
		},

		Public : {
			aWidgets : {
				Public : {
					bInit 	: false,
					sPath 	: 'Public/Public',
					sIndex 	: 'iTransactionId',
					sGroup 	: 'Main',
					sModule : 'Dashboard',
					sTableName : 'transactions',
					sForm	: 'form[name="reserve"]',
					
					init 	: function(aArg,fCb)
					{
						var oSelf = this;

						oForms.init(this,aArg);
						this.form();

						$(oModules.oWidgets.Public.aWidgets.Public.oForm.SearchTransctionBtn).click(function(){
							$(oModules.oWidgets.Public.aWidgets.Public.oForm.sTransactionCode).val() && oSelf.form({ sTransactionCode:  $(oModules.oWidgets.Public.aWidgets.Public.oForm.sTransactionCode).val(), bActive : 1 });
						});

						$('div[name="Public"] button[name="cancel"]').click(function(){
							oForms.confirm('Public/Public',{ bActive: 1, iTransactionId: $(oModules.oWidgets.Public.aWidgets.Public.oForm.iTransactionId).val() },this);
						});
					},
					aValidation :
 	 				{
 	 					rules : {
 	 						iTransactionId			: {},
 	 						iPatientId	 			: {},
 	 						iDoctorId 				: { required:true, digits:true },
	 						iAppointmentTypeId 		: { required:true, digits:true },
	 						iDoctorScheduleId 		: { required: true, digits: true },
	 						iTransactionDate 		: { required: true, digits: true },

	 						bStatus 				: {},
	 						bActive 				: { required: true, digits: true },

	 						sLastName		 		: { required: true, maxlength: 64 },
	 						sFirstName		 		: { required: true, maxlength: 64 },
	 						sMiddleName		 		: { required: true, maxlength: 64 },
	 						sAddress		 		: { required: true, maxlength: 100 },
	 						iGenderId		 		: { required: true, digits: true },
	 						
	 						iCivilStatusId			: { required: true, digits: true },
	 						sContactNumber		 	: { required: true, maxlength: 15 },
	 						
	 						iProvinceId		 		: { required: true, digits: true },
	 						iCityId		 			: { required: true, digits: true },
	 						iTime 					: { required: true, bBetweenSchedule: true },

	 						sDay		 			: {}

 	 					}
 	 				},

 	 				form : function(aArg)
 	 				{
 	 					var oSelf = this;

 	 					var aBundles = [
 	 						[
 	 							{
 	 								sModule: 'Metas',
									sGroup: 'Management',
									sTableName: 'metas',
									aRequest	: {
										sCode : 'Gender',
										sIndex : 'iMetaId',
										sColumn : 'sName',
										aColumns	: [
											'iMetaId',
											'sName'
										]
									}
								},
								{
 	 								sModule: 'Metas',
									sGroup: 'Management',
									sTableName: 'metas',
									aRequest	: {
										sCode : 'CivilStatus',
										sIndex : 'iMetaId',
										sColumn : 'sName',
										aColumns	: [
											'iMetaId',
											'sName'
										]
									}
								},
								{
 	 								sModule: 'Provinces',
									sGroup: 'Management',
									sTableName: 'provinces',
									aRequest	: {
										sIndex : 'iProvinceId',
										sColumn : 'sProvince',
										aColumns	: [
											'iProvinceId',
											'sProvince'
										]
									}
								},
								{
 	 								sModule: 'Cities',
									sGroup: 'Management',
									sTableName: 'cities',
									aRequest	: {
										sIndex : 'iCityId',
										sColumn : 'sCity',
										aColumns	: [
											'iCityId',
											'sCity'
										]
									}
								},
								{
 	 								sModule: 'AppointmentTypes',
									sGroup: 'Management',
									sTableName: 'appointment_types',
									aRequest	: {
										sIndex : 'iAppointmentTypeId',
										sColumn : 'sCode',
										aColumns	: [
											'iAppointmentTypeId',
											'sCode'
										]
									}
								},
								{
 	 								sModule: 'Doctors',
									sGroup: 'Management',
									sTableName: 'doctors',
									aRequest	: {
										bTransaction : true,
										sIndex : 'iDoctorId',
										sColumn : 'sFullName',
										aColumns	: [
											'iDoctorId',
											'sFullName'
										]
									}
								}
 	 						],[
 	 							'iGenderId',
 	 							'iCivilStatusId',
 	 							'iProvinceId',
 	 							'iCityId',
 	 							'iAppointmentTypeId',
 	 							'iDoctorId'
 	 						]
 	 					];
 	 					
 	 					if(aArg)
 	 					{
 	 						var aRequest = {
	 	 						sModule: this.sModule,
								aRequest: {
									sTransactionCode : aArg.sTransactionCode,
									bActive 		 : aArg.bActive
								},
								aBundles: aBundles
	 	 					}
 	 					}

						oForms.populate(oSelf,aRequest,aBundles,function(aResp){
							$('div[name="Public"] .login-box-msg').empty().append('Complete these fields to process your reservation.').css('color','#777');
							aResp && aResp.aRequest && $('div[name="Public"] input[name="iTime"]').val(oPhp.date('g:i A',aResp.aRequest[0].iTime));
							
							if(aArg){
								if(aRequest && aResp.aRequest)
								{
									$('div[name="Public"]').attr('mode','edit').find('button[name="cancel"]').attr('style','display:block !important');	
								}
								else
								{
									$('div[name="Public"] .login-box-msg').empty().append('Transaction code not found!').css('color','red');
								}
								
							} else {
								$('div[name="Public"]').attr('mode','add').find('button[name="cancel"]').attr('style','display:none !important');
							}
						},true);
 	 				}
				}
			}

		},

		Dashboard : 
		{
			init : function(aArg,fCb)
			{
				oModules.load(aArg);
			},

			aWidgets : {

				Dashboard : {
					bInit 	: false,
					sPath 	: 'Dashboard/Dashboard',
					sIndex 	: 'iTransactionId',
					sGroup 	: 'Main',
					sModule : 'Dashboard',
					sForm	: 'div[name="Dashboard"][role="dialog"] form',
					
					init 	: function(aArg,fCb)
					{
						if(this.bInit)
						{
							oTools.reset(this,aArg);
						}
						else
						{
							oTables.init(this,aArg);
							oForms.init(this,aArg);
							this.bInit = true;
						}
					},

					aDatatable : 
					{
						sName 		: 'Transactions_Datatables',
						aColumns 	:
						[
							{
								bVisible	: false,
								mData		: 'iTransactionId'
				            },
				            {
				            	bVisible 	: false,
				            	mData 		: 'bActive'
				            },
				            {
				            	bVisible 	: false,
				            	mData 		: 'sPatientMiddleName'
				            },
				            {
				            	bVisible 	: false,
				            	mData 		: 'sPatientFirstName'
				            },
				            {
				            	bVisible 	: false,
				            	mData 		: 'sDoctorMiddleName'
				            },
				            {
				            	bVisible 	: false,
				            	mData 		: 'sDoctorFirstName'
				            },
				            {
				            	sTitle 		: 'Code',
				            	mData 		: 'sTransactionCode'
				            },
				            {
				            	sTitle 		: 'Date',
				            	mData 		: 'iTransactionDate'
				            },
				            {
				            	sTitle 		: 'Patient',
				            	mData 		: 'sPatientLastName',
				            	render 		: function(data,type,row){
				            		return row.sPatientLastName+', '+row.sPatientFirstName+' '+row.sPatientMiddleName;
				            	}
				            },
				            {
				            	sTitle 		: 'Doctor',
				            	mData 		: 'sDoctorLastName',
				            	render 		: function(data,type,row){
				            		return row.sDoctorLastName+', '+row.sDoctorFirstName+' '+row.sDoctorMiddleName;
				            	}	
				            },
				            {
				            	sTitle 		: 'Appointment',
				            	mData 		: 'sAppointmentType'
				            }
						],
						order : [[3,'asc']]
 	 				},
 	 				
 	 				aValidation :
 	 				{
 	 					rules : {
 	 						iTransactionId			: {},
 	 						iPatientId	 			: {},
 	 						iDoctorId 				: { required:true, digits:true },
	 						iAppointmentTypeId 		: { required:true, digits:true },
	 						iDoctorScheduleId 		: { required: true, digits: true },
	 						iTransactionDate 		: { required: true, digits: true },

	 						bStatus 				: {},
	 						bActive 				: { required: true, digits: true },
	 						sRemarks 				: { maxlength: 100 },

	 						sLastName		 		: { required: true, maxlength: 64 },
	 						sFirstName		 		: { required: true, maxlength: 64 },
	 						sMiddleName		 		: { required: true, maxlength: 64 },
	 						sAddress		 		: { required: true, maxlength: 100 },
	 						iGenderId		 		: { required: true, digits: true },
	 						
	 						iCivilStatusId			: { required: true, digits: true },
	 						sContactNumber		 	: { required: true, maxlength: 15 },
	 						
	 						iProvinceId		 		: { required: true, digits: true },
	 						iCityId		 			: { required: true, digits: true },
	 						iTime 					: { required: true, bBetweenSchedule: true },

	 						sDay		 			: {}

 	 					}
 	 				},

 	 				form : function(aArg)
 	 				{
 	 					var oSelf = this;

 	 					var aBundles = [
 	 						[
 	 							{
 	 								sModule: 'Metas',
									sGroup: 'Management',
									sTableName: 'metas',
									aRequest	: {
										sCode : 'Gender',
										sIndex : 'iMetaId',
										sColumn : 'sName',
										aColumns	: [
											'iMetaId',
											'sName'
										]
									}
								},
								{
 	 								sModule: 'Metas',
									sGroup: 'Management',
									sTableName: 'metas',
									aRequest	: {
										sCode : 'CivilStatus',
										sIndex : 'iMetaId',
										sColumn : 'sName',
										aColumns	: [
											'iMetaId',
											'sName'
										]
									}
								},
								{
 	 								sModule: 'Provinces',
									sGroup: 'Management',
									sTableName: 'provinces',
									aRequest	: {
										sIndex : 'iProvinceId',
										sColumn : 'sProvince',
										aColumns	: [
											'iProvinceId',
											'sProvince'
										]
									}
								},
								{
 	 								sModule: 'Cities',
									sGroup: 'Management',
									sTableName: 'cities',
									aRequest	: {
										sIndex : 'iCityId',
										sColumn : 'sCity',
										aColumns	: [
											'iCityId',
											'sCity'
										]
									}
								},
								{
 	 								sModule: 'AppointmentTypes',
									sGroup: 'Management',
									sTableName: 'appointment_types',
									aRequest	: {
										sIndex : 'iAppointmentTypeId',
										sColumn : 'sCode',
										aColumns	: [
											'iAppointmentTypeId',
											'sCode'
										]
									}
								},
								{
 	 								sModule: 'Doctors',
									sGroup: 'Management',
									sTableName: 'doctors',
									aRequest	: {
										bTransaction : true,
										sIndex : 'iDoctorId',
										sColumn : 'sFullName',
										aColumns	: [
											'iDoctorId',
											'sFullName'
										]
									}
								}
 	 						],[
 	 							'iGenderId',
 	 							'iCivilStatusId',
 	 							'iProvinceId',
 	 							'iCityId',
 	 							'iAppointmentTypeId',
 	 							'iDoctorId'
 	 						]
 	 					];
 	 					
 	 					if(aArg)
 	 					{
 	 						var aRequest = {
	 	 						sModule: this.sModule,
								aRequest: {
									iTransactionId : aArg.iTransactionId
								},
								aBundles: aBundles
	 	 					}
 	 					}

						oForms.populate(oSelf,aRequest,aBundles,function(aResp){
							aResp && aResp.aRequest && $('div[name="Dashboard"] input[name="iTime"]').val(oPhp.date('g:i A',aResp.aRequest[0].iTime));
							// aResp && aResp.aRequest && $('div[name="Dashboard"] input[name="iCityId"]').val(aResp.aRequest[0]['iCityId']);
						},true);
 	 				},
				}
			}
		},

		AppointmentTypes : 
		{
			init : function(aArg,fCb)
			{
				oModules.load(aArg);
			},

			aWidgets : {

				AppointmentTypes : {
					bInit 	: false,
					sPath 	: 'AppointmentTypes/AppointmentTypes',
					sIndex 	: 'iAppointmentTypeId',
					sGroup 	: 'Management',
					sModule : 'AppointmentTypes',
					sForm	: 'div[name="AppointmentTypes"][role="dialog"] form',
					init 	: function(aArg)
					{
						if(this.bInit)
						{
							oTools.reset(this,aArg);
						}
						else
						{
							oTables.init(this,aArg);
							oForms.init(this,aArg);
							this.bInit = true;
						}
					},

					aDatatable : 
					{
						sName 		: 'AppointmentTypes_Datatables',
						aColumns 	:
						[
							{
								bVisible	: false,
								mData		: 'iAppointmentTypeId'
				            },
				            {
				            	bVisible 	: false,
				            	mData 		: 'bActive'
				            },
				            {
				            	sTitle 		: 'Code',
				            	mData 		: 'sCode',
				            	render 		: function(data,type,row){
									return (row['bActive'] > 0 ? '<i class="fa fa-check-circle-o FontGreen"></i> ' : '<i class="fa fa-times-circle-o FontRed"></> ')+data;
								}
				            },
				            {
				            	sTitle 		: 'Date Created',
				            	mData 		: 'iDateCreated',
				            	render		: function(data,type,row){
				            		return oPhp.date('F j, Y',data);
				            	}
				            }
						],
						order : [[3,'asc']]
 	 				},
 	 				
 	 				aValidation :
 	 				{
 	 					rules : {
 	 						iAppointmentTypeId	: {},
	 						iDateCreated 		: {},

 	 						sCode	 			: { required: true, maxlength: 10 },
	 						sDescription 		: { maxlength: 100 },
	 						bActive 			: { required: true, digits: true },
 	 					}
 	 				},

 	 				form : function(aArg)
 	 				{
 	 					var oSelf = this;

 	 					var aBundles = [
 	 						[],[]
 	 					];
 	 					
 	 					if(aArg)
 	 					{
 	 						var aRequest = {
	 	 						sModule: 'AppointmentTypes',
								aRequest: {
									iAppointmentTypeId : aArg.iAppointmentTypeId
								},
								aBundles: aBundles
	 	 					}
 	 					}

						oForms.populate(oSelf,aRequest,aBundles,function(aResp){
						},true);
 	 				}
				}
			}
		},

		Clinics : 
		{
			init : function(aArg,fCb)
			{
				oModules.load(aArg);
			},

			aWidgets : {

				Clinics : {
					bInit 	: false,
					sPath 	: 'Clinics/Clinics',
					sIndex 	: 'iClinicId',
					sGroup 	: 'Management',
					sModule : 'Clinics',
					sForm	: 'div[name="Clinics"][role="dialog"] form',
					init 	: function(aArg)
					{
						if(this.bInit)
						{
							oTools.reset(this,aArg);
						}
						else
						{
							oTables.init(this,aArg);
							oForms.init(this,aArg);
							this.bInit = true;
						}
					},

					aDatatable : 
					{
						sName 		: 'Clinics_Datatables',
						aColumns 	:
						[
							{
				            	bVisible 	: false,	
				            	mData 		: 'iClinicId'
				            },
				            {
				            	bVisible 	: false,	
				            	mData 		: 'bActive'
				            },
				            {
				            	sTitle 		: 'Floor - Room',
				            	mData 		: 'sRoom',
				            	render 		: function(data,type,row){
									return ( row['bActive'] > 0 ? '<i class="fa fa-check-circle-o FontGreen"></i> ' : '<i class="fa fa-times-circle-o FontRed"></> ') + data;
								}
				            },
				            {
				            	sTitle 		: 'Building',
				            	mData 		: 'sBuildingName'
				            },
				            {
				            	sTitle 		: 'Date Created',
				            	mData 		: 'iDateCreated',
				            	render		: function(data,type,row){
				            		return oPhp.date('F j, Y',data);
				            	}
				            }
						],
						order : [[3,'asc']]
 	 				},
 	 				
 	 				aValidation :
 	 				{
 	 					rules : {
 	 						iClinicId 			: {},
	 						bActive 			: { required: true, digits: true },
 	 						iFloorNumber		: { digits: true},
 	 						sRoomNumber			: { required: true, maxlength: 5},
 	 						sBuildingName	 	: { maxlength: 64 }
 	 					}
 	 				},

 	 				form : function(aArg)
 	 				{
 	 					var oSelf = this;
 	 					var aBundles = [[],[]];
 	 					
 	 					if(aArg)
 	 					{
 	 						var aRequest = {
	 	 						sModule: 'Clinics',
								aRequest: { iClinicId : aArg.iClinicId },
								aBundles: aBundles
	 	 					}
 	 					}

						oForms.populate(oSelf,aRequest,aBundles,function(aResp){
						},true);
 	 				}
				}
			}
		},

		Doctors : 
		{
			init : function(aArg,fCb)
			{
				oModules.load(aArg);
			},

			aWidgets : {

				Doctors : {
					bInit 	: false,
					sPath 	: 'Doctors/Doctors',
					sIndex 	: 'iDoctorId',
					sGroup 	: 'Management',
					sModule : 'Doctors',
					sForm	: 'div[name="Doctors"][role="dialog"] form',
					init 	: function(aArg)
					{
						if(this.bInit)
						{
							oTools.reset(this,aArg);
							this.DoctorSchedules.init(this);
						}
						else
						{
							var oSelf = this;

							oTables.init(oSelf,aArg);
							oForms.init(oSelf,aArg);
							oSelf.bInit = true;

							$(oSelf.oForm.oAddSchedule).click(function(){
								oSelf.DoctorSchedules.add(oSelf,this);
							});

							$(oSelf.oModal).on('hidden.bs.modal',function(){
								$(oSelf.oForm).find('div[name="Schedules"] .box-body').empty();
							});
						}
					},

					aDatatable : 
					{
						sName 		: 'Doctors_Datatables',
						aColumns 	:
						[
							{
				            	bVisible 	: false,	
				            	mData 		: 'iDoctorId'
				            },
				            {
				            	bVisible 	: false,	
				            	mData 		: 'bActive'	
				            },
				            {
				            	sTitle 		: 'Name',
				            	mData 		: 'sFullName'
				            },
				            {
				            	sTitle 		: 'Floor - Room',
				            	mData 		: 'sRoom',
				            	render 		: function(data,type,row){
									return ( row['bActive'] > 0 ? '<i class="fa fa-check-circle-o FontGreen"></i> ' : '<i class="fa fa-times-circle-o FontRed"></> ') + data;
								}
				            },
				            {
				            	sTitle 		: 'Date Created',
				            	mData 		: 'iDateCreated',
				            	render		: function(data,type,row){
				            		return oPhp.date('F j, Y',data);
				            	}
				            }
						],
						order : [[1,'asc']]
 	 				},
 	 				
 	 				aValidation :
 	 				{
 	 					rules : {
 	 						iDoctorId 			: {},
	 						bActive 			: { required: true, digits: true },
 	 						sLastName			: { required:true, maxlength: 64},
 	 						sMiddleName			: { maxlength: 64},
 	 						sFirstName			: { required: true, maxlength: 64},
 	 						iGenderId	 		: { required: true, digits: true },
 	 						iCivilStatusId	 	: { required: true, digits: true },
 	 						iClinicId	 		: { required: true, digits: true },
 	 						sContactNumber	 	: { required: true, maxlength: 11 },
 	 						sPhoto	 			: { maxlength: 64 },

 	 						sDay				: {},
 	 						iPatients			: {},
 	 						iDateStart 			: {},
 	 						iDateEnd 			: {}
 	 					}
 	 				},

 	 				form : function(aArg)
 	 				{
 	 					var oSelf = this;
 	 					var aBundles = [
 	 						[
 	 							{
 	 								sModule: 'Metas',
									sGroup: 'Management',
									sTableName: 'metas',
									aRequest	: {
										sCode : 'Gender',
										sIndex : 'iMetaId',
										sColumn : 'sName',
										aColumns	: [
											'iMetaId',
											'sName'
										]
									}
								},
								{
 	 								sModule: 'Metas',
									sGroup: 'Management',
									sTableName: 'metas',
									aRequest	: {
										sCode : 'CivilStatus',
										sIndex : 'iMetaId',
										sColumn : 'sName',
										aColumns	: [
											'iMetaId',
											'sName'
										]
									}
								},
								{
 	 								sModule: 'Metas',
									sGroup: 'Management',
									sTableName: 'clinics',
									aRequest	: {
										sIndex : 'iClinicId',
										sColumn : 'sRoom',
										aColumns	: [
											'iClinicId',
											'sRoom'
										]
									}
								}
 	 						],[
 	 							'iGenderId',
 	 							'iCivilStatusId',
 	 							'iClinicId'
 	 						]
 	 					];
 	 					
 	 					if(aArg)
 	 					{
 	 						var aRequest = {
	 	 						sModule: 'Clinics',
								aRequest: { 
									iDoctorId : aArg.iDoctorId,
									iDoctorScheduleId : {
										aRequest	: {
											iDoctorId : aArg.iDoctorId,
											aColumns	: [
												'sDay',
												'iPatients',
												'iDateStart',
												'iDateEnd',
												'iDoctorScheduleId'
											]
										}
									}
								},
								aBundles: aBundles
	 	 					}
 	 					}

						oForms.populate(oSelf,aRequest,aBundles,function(aResp){
							if(aRequest) oSelf.DoctorSchedules.populate(oSelf,aResp.aRequest.iDoctorScheduleId);
						},true);
 	 				},

 	 				DoctorSchedules : 
 	 				{

 	 					init : function(oWidget)
 	 					{
 	 						$(oWidget.oForm).find('div[name="Schedules"] .box-body').empty();
 	 					},

 	 					add: function(oWidget,oField,aValue)
 	 					{
 	 						var oContainer = $(oWidget.oForm).find('div[name="Schedules"] .box-body');
 	 						oContainer.append(
 	 							'<div class="form-group">'+
 	 								'<div class="col-sm-3">'+
 	 									'<label class="control-label">Date</label>'+
 	 									'<div class="input-group date">'+
 	 										'<div class="input-group-addon">'+
 	 											'<i class="fa fa-calendar"></i>'+
 	 										'</div>'+
											// '<input name="iDate" field-type="Datepicker" type="text" class="form-control datepicker">'+
											'<select name="sDay" class="form-control" field-type="select">'+
												'<option>Monday</option>'+
												'<option>Tuesday</option>'+
												'<option>Wednesday</option>'+
												'<option>Thursday</option>'+
												'<option>Friday</option>'+
												'<option>Saturday</option>'+
											'</select>'+
										'</div>'+
 	 								'</div>'+
 	 								'<div class="col-sm-3">'+
 	 									'<label class="control-label">From</label>'+
 	 									'<div class="input-group bootstrap-timepicker">'+
 	 										'<input name="iDateStart" type="text" class="form-control timepicker" field-type="Timepicker" field-default="10:00 AM">'+
											'<div class="input-group-addon">'+
						                   		'<i class="fa fa-clock-o"></i>'+
						                    '</div>'+	
										'</div>'+
 	 								'</div>'+
 	 								'<div class="col-sm-3">'+
 	 									'<label class="control-label">To</label>'+
 	 									'<div class="input-group bootstrap-timepicker">'+
 	 										'<input name="iDateEnd" type="text" class="form-control timepicker" field-type="Timepicker" field-default="5:00 PM">'+
											'<div class="input-group-addon">'+
						                   		'<i class="fa fa-clock-o"></i>'+
						                    '</div>'+	
										'</div>'+
 	 								'</div>'+
 	 								'<div class="col-sm-2">'+
 	 									'<label class="control-label">Patients Count</label>'+
 	 									'<div class="input-group">'+
 	 										'<input name="iPatients" type="text" class="form-control" value="'+ ( aValue ? aValue.iPatients : "" ) +'">'+
											'<div class="input-group-addon">'+
						                   		'<i class="fa fa-users"></i>'+
						                    '</div>'+	
										'</div>'+
 	 								'</div>'+
 	 								'<div class="col-sm-1">'+
 	 									'<label class="control-label col-sm-12"> &nbsp; </label>'+
										'<input onclick= "oModules.oWidgets.Doctors.aWidgets.Doctors.DoctorSchedules.delete(this)"  type="button" class="btn btn-danger btn-sm fa" value="remove">'+
 	 								'</div>'+
 	 							'</div>'
 	 						);
 	 						
 	 						aValue && oContainer.find('.form-group:last').find('select[name="sDay"]').val(aValue.sDay);

 	 						oForms.aFields.Timepicker.init(oContainer.find('.form-group:last .timepicker'));
  	 						
  	 						aValue && oForms.aFields.Timepicker.populate( oContainer.find('.form-group:last').find('input[name="iDateStart"]') , aValue.iDateStart );
 	 						aValue && oForms.aFields.Timepicker.populate( oContainer.find('.form-group:last').find('input[name="iDateEnd"]'), aValue.iDateEnd );
 	 					},

 	 					populate : function(oWidget,aData,bInit)
 	 					{
 	 						if(!aData) return;

 	 						$(oWidget.oForm).find('div[name="Schedules"] .box-body').empty();

 	 						var oSelf = this;
 	 						$.each(aData,function(iKey,aVal){
 	 							oSelf.add(oWidget,null,aVal);
 	 						});
 	 					},
 	 					delete : function(oSelf)
 	 					{
 	 						$(oSelf).closest('.form-group').remove();
 	 					},
 	 				}
				}
			}
		},

		Metas : 
		{
			init : function(aArg,fCb)
			{
				oModules.load(aArg);
			},

			aWidgets : {

				Metas : {
					bInit 	: false,
					sPath 	: 'Metas/Metas',
					sIndex 	: 'iMetaId',
					sGroup 	: 'Management',
					sModule : 'Metas',
					sForm	: 'div[name="Metas"][role="dialog"] form',
					init 	: function(aArg)
					{
						if(this.bInit)
						{
							oTools.reset(this,aArg);
						}
						else
						{
							oTables.init(this,aArg);
							oForms.init(this,aArg);
							this.bInit = true;
						}
					},

					aDatatable : 
					{
						sName 		: 'Metas_Datatables',
						aColumns 	:
						[
							{
				            	bVisible 	: false,	
				            	mData 		: 'iMetaId'
				            },
				            {
				            	bVisible 	: false,	
				            	mData 		: 'bActive'
				            },
				            {
				            	sTitle 		: 'Name',
				            	mData 		: 'sName',
				            	render 		: function(data,type,row){
									return (row['bActive'] > 0 ? '<i class="fa fa-check-circle-o FontGreen"></i> ' : '<i class="fa fa-times-circle-o FontRed"></> ')+data;
								}
				            },
				            {
				            	sTitle 		: 'Code',
				            	mData 		: 'sCode'
				            },
				            {
				            	sTitle 		: 'Group',
				            	mData 		: 'sMetaGroupName'
				            },
				            {
				            	sTitle 		: 'Date Created',
				            	mData 		: 'iDateCreated',
				            	render		: function(data,type,row){
				            		return oPhp.date('F j, Y',data);
				            	}
				            }
						],
						order : [[3,'asc']]
 	 				},
 	 				
 	 				aValidation :
 	 				{
 	 					rules : {
 	 						iMetaId 			: {},
 	 						iMetaGroupId		: { required: true, digits: true},
	 						bActive 			: { required: true, digits: true },
 	 						sCode	 			: { required: true, maxlength: 10 },
 	 						sName	 			: { required: true, maxlength: 32 }
 	 					}
 	 				},

 	 				form : function(aArg)
 	 				{
 	 					var oSelf = this;
 	 					var aBundles = [
 	 						[
 	 							{
 	 								sModule: 'MetaGroups',
									sGroup: 'Management',
									sTableName: 'meta_groups',
									aRequest	: {
										sIndex : 'iMetaGroupId',
										sColumn : 'sName',
										aColumns	: [
											'iMetaGroupId',
											'sName'
										]
									}
								}
 	 						],[
 	 							'iMetaGroupId'
 	 						]
 	 					];
 	 					
 	 					if(aArg)
 	 					{
 	 						var aRequest = {
	 	 						sModule: 'Metas',
								aRequest: { iMetaId : aArg.iMetaId },
								aBundles: aBundles
	 	 					}
 	 					}

						oForms.populate(oSelf,aRequest,aBundles,function(aResp){
						},true);
 	 				}
				}
			}
		},

		MetaGroups : 
		{
			init : function(aArg,fCb)
			{
				oModules.load(aArg);
			},

			aWidgets : {

				MetaGroups : {
					bInit 	: false,
					sPath 	: 'MetaGroups/MetaGroups',
					sIndex 	: 'iMetaGroupId',
					sGroup 	: 'Management',
					sModule : 'MetaGroups',
					sForm	: 'div[name="MetaGroups"][role="dialog"] form',
					init 	: function(aArg)
					{
						if(this.bInit)
						{
							oTools.reset(this,aArg);
						}
						else
						{
							oTables.init(this,aArg);
							oForms.init(this,aArg);
							this.bInit = true;
						}
					},

					aDatatable : 
					{
						sName 		: 'MetaGroups_Datatables',
						aColumns 	:
						[
							{
								bVisible	: false,
								mData		: 'iMetaGroupId'
				            },
				            {
				            	bVisible 	: false,	
				            	mData 		: 'bActive'
				            },
				            {
				            	sTitle 		: 'Code',
				            	mData 		: 'sCode',
				            	render 		: function(data,type,row){
									return (row['bActive'] > 0 ? '<i class="fa fa-check-circle-o FontGreen"></i> ' : '<i class="fa fa-times-circle-o FontRed"></> ')+data;
								}
				            },
				            {
				            	sTitle 		: 'Name',	
				            	mData 		: 'sName'
				            },
				            {
				            	sTitle 		: 'Date Created',
				            	mData 		: 'iDateCreated',
				            	render		: function(data,type,row){
				            		return oPhp.date('F j, Y',data);
				            	}
				            }
						],
						order : [[3,'asc']]
 	 				},
 	 				
 	 				aValidation :
 	 				{
 	 					rules : {
 	 						iMetaGroupId		: {},
	 						bActive 			: { required: true, digits: true },
 	 						sCode	 			: { required: true, maxlength: 15 },
 	 						sName	 			: { required: true, maxlength: 45 }
 	 					}
 	 				},

 	 				form : function(aArg)
 	 				{
 	 					var oSelf = this;

 	 					var aBundles = [
 	 						[],[]
 	 					];
 	 					
 	 					if(aArg)
 	 					{
 	 						var aRequest = {
	 	 						sModule: 'MetaGroups',
								aRequest: {
									iMetaGroupId : aArg.iMetaGroupId
								},
								aBundles: aBundles
	 	 					}
 	 					}

						oForms.populate(oSelf,aRequest,aBundles,function(aResp){
							
						},true);
 	 				}
				}
			}
		},

		Modules : 
		{
			init : function(aArg,fCb)
			{
				oModules.load(aArg);
			},

			aWidgets : {

				Modules : {
					bInit 	: false,
					sPath 	: 'Modules/Modules',
					sIndex 	: 'iModuleId',
					sGroup 	: 'Admin',
					sModule : 'Modules',
					sForm	: 'div[name="Modules"][role="dialog"] form',
					init 	: function(aArg)
					{
						if(this.bInit)
						{
							oTools.reset(this,aArg);
						}
						else
						{
							oTables.init(this,aArg);
							oForms.init(this,aArg);
							this.bInit = true;
						}
					},

					aDatatable : 
					{
						sName 		: 'Modules_Datatables',
						aColumns 	:
						[
							{
								bVisible	: false,
								mData		: 'iModuleId'
				            },
							{
								sTitle		: 'Module',
								mData		: 'sTitle',
								render 		: function(data,type,row){
									return (row['bActive'] > 0 ? '<i class="fa fa-check-circle-o FontGreen"></i> ' : '<i class="fa fa-times-circle-o FontRed"></> ') + 
									data + (row['bOnload'] == 1 ? '	<li class="fa fa-hourglass-start" area-hidden="true"></li>' : '' );
								}
							},
							{
								bVisible	: false,
								mData		: 'bActive'
				            },
				            {
				            	bVisible 	: false,
								mData		: 'bOnload',
				            },
				            {
				            	sTitle 		: 'Date Created',
				            	mData 		: 'iDateCreated',
				            	render		: function(data,type,row){
				            		return oPhp.date('F j, Y',data);
				            	}
				            }
						],
						order : [[1,'asc']]
 	 				},
 	 				
 	 				aValidation :
 	 				{
 	 					rules : {
 	 						iModuleId 		: { digits : true },
	 						sModuleName 	: { required: true, maxlength: 20 },
	 						sTitle 			: { maxlength: 32},
	 						sTableName 		: { maxlength: 32},	
	 						bActive 		: { required: true, digits: true },
	 						bOnload 		: { digits: true },
	 						iPermissionId 	: {}
 	 					}
 	 				},

 	 				form : function(aArg)
 	 				{
 	 					var oSelf = this;
 	 					var aRequest;

 	 					var aBundles = [
 	 						[
 	 							{
 	 								sModule: 'Permissions',
 	 								sTableName: 'permissions',
									sGroup: 'Admin',
									aRequest	: {
										sIndex : 'iPermissionId',
										sColumn : 'sPermissionName',
										aColumns	: [
											'iPermissionId',
											'sPermissionName'
										]
									}
								},
 	 						],[
 	 							'iPermissionId'
 	 						]
 	 					];

 	 					if(aArg){
 	 						aRequest = {
	 	 						sModule: 'Modules',
								aRequest: {
									iModuleId : aArg.iModuleId,
									iPermissionId : {
										aRequest	: {
											iModuleId : aArg.iModuleId,
											sIndex : 'iPermissionId',
											sColumn : 'iBitIndex',
											aColumns	: [
												'iBitIndex',
												'iPermissionId',
												'iModuleId'
											]
										}
									}
								}
	 	 					};
 	 					}
 	 					
						oForms.populate(oSelf,aRequest,aBundles,function(aResp){
							
						},true);
 	 				}
				}
			}
		},
		
		Roles : 
		{
			init : function(aArg,fCb)
			{
				oModules.load(aArg);
			},

			aWidgets : {

				Roles : {
					bInit 	: false,
					sPath 	: 'Roles/Roles',
					sIndex 	: 'iRoleId',
					sGroup 	: 'Admin',
					sModule : 'Roles',
					sForm	: 'div[name="Roles"][role="dialog"] form',
					init 	: function(aArg)
					{
						if(this.bInit)
						{
							oTools.reset(this,aArg);
						}
						else
						{
							oTables.init(this,aArg);
							oForms.init(this,aArg);
							this.bInit = true;
						}
					},

					aDatatable : 
					{
						sName 		: 'Roles_Datatables',
						aColumns 	:
						[
							{
								bVisible	: false,
								mData		: 'iRoleId'
				            },
							{
								sTitle		: 'Roles Name',
								mData		: 'sRoleName',
								render 		: function(data,type,row){
									return (row['bActive'] > 0 ? '<i class="fa fa-check-circle-o FontGreen"></i> ' : '<i class="fa fa-times-circle-o FontRed"></> ') + data;
								}
							},
							{
								bVisible	: false,
								mData		: 'bActive'
				            },
				            {
				            	sTitle 		: 'Date Created',
				            	mData 		: 'iDateCreated',
				            	render		: function(data,type,row){
				            		return oPhp.date('F j, Y',data);
				            	}
				            }
						],
						order : [[1,'asc']]
 	 				},
 	 				
 	 				aValidation :
 	 				{
 	 					rules : {
 	 						iRoleId 		: { digits : true },
	 						sRoleName 		: { required: true, maxlength: 15 },
	 						bActive 		: { required: true, digits: true },

	 						iPermissionId 	: {},
	 						iModuleId 		: {}
 	 					}
 	 				},

 	 				form : function(aArg)
 	 				{
 	 					var oSelf = this;

 	 					var aBundles = [
 	 						[
 	 							{
 	 								sModule: 'Modules',
									sGroup: 'Admin',
									sTableName: 'modules',
									aRequest	: {
										sIndex : 'iModuleId',
										sColumn : 'sModuleName',
										aColumns	: [
											'iModuleId',
											'sModuleName'
										]
									}
								}
 	 						],[
 	 							'iModuleId',
 	 						]
 	 					];

 	 					if(aArg)
 	 					{
 	 						var aRequest = {
	 	 						sModule: 'Roles',
								aRequest: {
									iRoleId : aArg.iRoleId,
									iModuleId : {
										aRequest	: {
											iRoleId : aArg.iRoleId,
											sIndex : 'iModuleId',
											sColumn : 'iRoleId',
											aColumns	: [
												'iModuleId',
												'iRoleId'
											]
										}
									}
								},
								aBundles: aBundles
	 	 					}
 	 					}

						oForms.populate(oSelf,aRequest,aBundles,function(aresp){
						},true);
 	 				}
				}
			}
		},

		Permissions : 
		{
			init : function(aArg,fCb)
			{
				oModules.load(aArg);
			},

			aWidgets : {

				Permissions : {
					bInit 	: false,
					sPath 	: 'Permissions/Permissions',
					sIndex 	: 'iPermissionId',
					sGroup 	: 'Admin',
					sModule : 'Permissions',
					sForm	: 'div[name="Permissions"][role="dialog"] form',
					init 	: function(aArg)
					{
						if(this.bInit)
						{
							oTools.reset(this,aArg);
						}
						else
						{
							oTables.init(this,aArg);
							oForms.init(this,aArg);
							this.bInit = true;
						}
					},

					aDatatable : 
					{
						sName 		: 'Permissions_Datatables',
						aColumns 	:
						[
							{
								bVisible	: false,
								mData		: 'iPermissionId'
				            },
							{
								sTitle		: 'Permission Name',
								mData		: 'sPermissionName',
								render 		: function(data,type,row){
									return (row['bActive'] > 0 ? '<i class="fa fa-check-circle-o FontGreen"></i> ' : '<i class="fa fa-times-circle-o FontRed"></> ') + data;
								}
							},
							{
								bVisible	: false,
								mData		: 'bActive'
				            },
				            {
				            	sTitle 		: 'Date Created',
				            	mData 		: 'iDateCreated',
				            	render		: function(data,type,row){
				            		return oPhp.date('F j, Y',data);
				            	}
				            }
						],
						order : [[1,'asc']]
 	 				},
 	 				
 	 				aValidation :
 	 				{
 	 					rules : {
 	 						iPermissionId 		: { digits : true },
	 						sPermissionName 	: { required: true, maxlength: 15 },
	 						bActive 			: { required: true, digits: true }
 	 					}
 	 				},

 	 				form : function(aArg)
 	 				{
 	 					var oSelf = this;

 	 					var aBundles = [[],[]];
 	 					if(aArg)
 	 					{
 	 						var aRequest = {
	 	 						sModule: 'Permissions',
								aRequest: {
									iPermissionId : aArg.iPermissionId
								},
								aBundles: aBundles
	 	 					}
 	 					}

						oForms.populate(oSelf,aRequest,aBundles,function(){

						},true);
 	 				}
				}
			}
		},

		Users : 
		{
			init : function(aArg,fCb)
			{
				oModules.load(aArg);
			},

			aWidgets : {

				Users : {
					bInit 	: false,
					sPath 	: 'Users/Users',
					sIndex 	: 'iUserId',
					sGroup 	: 'Admin',
					sModule : 'Users',
					sForm	: 'div[name="Users"][role="dialog"] form',
					init 	: function(aArg)
					{
						if(this.bInit)
						{
							oTools.reset(this,aArg);
						}
						else
						{
							oTables.init(this,aArg);
							oForms.init(this,aArg);
							this.bInit = true;
						}
					},

					aDatatable : 
					{
						sName 		: 'Users_Datatables',
						aColumns 	:
						[
							{
								bVisible	: false,
								mData		: 'iUserId'
				            },
				            {
								bVisible	: false,
								mData		: 'bActive'
				            },
							{
								sTitle		: 'ID',
								mData		: 'iUserId',
								render 		: function(data,type,row){
									return (row['bActive'] > 0 ? '<i class="fa fa-check-circle-o FontGreen"></i> ' : '<i class="fa fa-times-circle-o FontRed"></> ')+data;
								}
							},
				            {
				            	sTitle 		: 'Name',
				            	mData 		: 'sFullName'
				            },
				            {
				            	sTitle 		: 'Role',
				            	mData 		: 'sRoleName'
				            }
						],
						order : [[1,'asc']]
 	 				},
 	 				
 	 				aValidation :
 	 				{
 	 					rules : {
 	 						iRoleId 			: { required: true},
 	 						iDoctorId 			: { required: true},
 	 						iUserId	 			: {},
 	 						sUsername	 		: { required: true, maxlength: 45 },
 	 						sPassword	 		: { required: true, maxlength: 45 },
 	 						sId	 				: { required: true, digits: true },
	 						sFirstName 			: { required: true, maxlength: 64 },
	 						sLastName 			: { required: true, maxlength: 64 },
	 						sMiddleName 		: { maxlength: 64 },
 	 					}
 	 				},

 	 				form : function(aArg)
 	 				{
 	 					var oSelf = this;

 	 					var aBundles = [
 	 						[
 	 							{
 	 								sModule: 'Roles',
									sGroup: 'Admin',
									sTableName: 'roles',
									aRequest	: {
										sIndex : 'iRoleId',
										sColumn : 'sRoleName',
										aColumns	: [
											'iRoleId',
											'sRoleName'
										]
									}
								},
								{
									sModule: 'Doctors',
									sGroup: 'Management',
									sTableName: 'doctors',
									aRequest	: {
										sIndex : 'iDoctorId',
										sColumn : 'sFullName',
										aColumns	: [
											'iDoctorId',
											'sFullName'
										]
									}
								}
 	 						],[
 	 							'iRoleId',
 	 							'iDoctorId'
 	 						]
 	 					];
 	 					
 	 					if(aArg)
 	 					{
 	 						var aRequest = {
	 	 						sModule: 'Users',
								aRequest: {
									iUserId : aArg.iUserId
								},
								aBundles: aBundles
	 	 					}
 	 					}

						oForms.populate(oSelf,aRequest,aBundles,function(aResp){
						},true);
 	 				}
				}
			}
		}
	}
};

window.oTools = 
{	
	aArg : {},
	init : function()
	{
		/** LOGOUT */
		$('header a[name="logout"]').click(function(){
			oData.send(oData.sUrl,{
				sMethod: 'users/User_auth/logout',
				aRequest : {}
			}, function(aResp){
				if(!aResp.iErr) location.reload();
			});
		});

		/** MODAL **/
		oTools.confirm = $('div[name="confirm"][role="dialog"]');

		$(oTools.confirm).find('form').submit(function(){

			var aPath = ($(oTools.confirm).find('form').attr('path')).split('/');
			var oWidget = oModules.oWidgets[aPath[0]].aWidgets[aPath[1]];
			var sAction = null;
			
			if($(oTools.confirm).find('form').attr('data-field') == 'bActive'){
				oTools.aArg[$(oTools.confirm).find('form').attr('data-field')] = parseInt( oTools.aArg[$(oTools.confirm).find('form').attr('data-field')] ) ? 0 : 1 ;
				oTools.aArg['mode'] = 'edit';	
				sAction = 'save';
			} 
			else
			{
				sAction = 'notify';
				oTools.aArg['mode'] = 'sms';
			} 

			oActions[sAction](oWidget, oTools.aArg ,function(aResp){
				oTools.aArg = {};
				oTools.confirm.modal('hide');
			});

		});
		
		oTools.oConfirm = this.modal(oTools.confirm,function(){
			var aPath = ($(oTools.confirm).find('form').attr('path')).split('/');
			var oWidget = oModules.oWidgets[aPath[0]].aWidgets[aPath[1]];

			oWidget.oTable && oWidget.oTable.ajax.reload();
			$(oTools.confirm).find('form').attr({ 'data-field':null, 'path':null });
			oForms.close(oWidget);
		});
		
		/** NAVIGATION **/
		$('aside.main-sidebar > section.sidebar > ul.sidebar-menu li a').click(function(){
			return (
				$(this).attr('name') ? ( 
					oModules.oWidgets[$(this).attr('name')] ? 
					oModules.oWidgets[$(this).attr('name')].init({ sModuleName : $(this).attr('name') }) : null
				) : null )
		});

		oData.send(oData.sUrl,{
			sMethod: 'users/User_auth/info',
			aRequest : {}
		},function(aResp){
			$('.user span[name="sFullName"]').empty().append(aResp.sFullName);
			$('.user li.user-header').append('<p>'+aResp.sFullName+' - '+ aResp.sRole +'</p>');
		});
	},

	Toastr : {
		init: function(){
			toastr.options = {
				"closeButton"		: true,
				"debug"				: false,
				"newestOnTop"		: true,
				"progressBar"		: true,
				"positionClass"		: "toast-top-right",
				"preventDuplicates"	: false,
				"onclick"			: null,
				"showDuration"		: "300",
				"hideDuration"		: "1000",
				"timeOut"			: "5000",
				"extendedTimeOut"	: "1000",
				"showEasing"		: "swing",
				"hideEasing"		: "linear",
				"showMethod"		: "fadeIn",
				"hideMethod"		: "fadeOut"
			};
		},
		notify : function(aArg){
			if(aArg.iError && aArg.iError > 0) toastr.error(aArg.sMessage);
		}
	},

	modal : function(oModal,fOnClose)
	{
		return (
			$(oModal).modal({
				keyboard: false,
				backdrop: 'static',
				show: false
			}).on('hidden.bs.modal',function(){
				fOnClose && fOnClose();
			})
		);
	},

	/**
	 * - reloads module's datatable
	 * @param  {[array]} aArg : contains module information
	 * @return {[function]}   : reset's and load the module 
	 */
	reset : function(oWidget, aArg)
	{
		
		oWidget.oTable.ajax.reload();
	}
}

window.oTables = 
{
	sTableName : null,

	init: function(oWidget,aArg)
	{
		var bState = false;

		this.sTableName = oWidget.aDatatable.sName;
		oWidget.aDatatable.aColumns.push({
			bSortable : false,
			mData: null,
			sWidth: 100,
			sTitle: (
					'<div class="btn-group col-sm-12">'+
						'<button action="add" type="button" onclick=oTables.action(this,"'+oWidget.sPath+'"); class="btn btn-default btn-sm fa">&#xf067;</button>'+
					'</div>'
				),
			render: function(data,type,row){

				var oButtons = [];
				var aButtons = [];

				oButtons.unshift( parseInt(row.bActive) ? {
					sPermission	: 'delete',
					sAction		: 'delete',
					sClass		: 'btn btn-default btn-sm',
					sIcon		: 'fa fa-trash',
					sText		: 'Delete',
					sField		: 'bActive'
				} :
				{
					sPermission	: 'revive',
					sAction		: 'revive',
					sClass		: 'btn btn-default btn-sm',
					sIcon		: 'fa fa-undo',
					sText		: 'Revive',
					sField		: 'bActive'
				});

				oButtons.unshift({
					sPermission	: 'update',
					sAction		: 'edit',
					sClass		: 'btn btn-default btn-sm',
					sIcon		: 'fa fa-pencil-square-o',
					sText		: 'Revive',
					sField		: 'bActive'
				});

				if(oWidget.sModule == 'Dashboard'){
					oButtons.unshift({
						sPermission	: 'notify',
						sAction		: 'notify',
						sClass		: 'btn btn-default btn-sm',
						sIcon		: 'fa fa-comment-o',
						sText		: 'Notify',
						sField		: ''
					});
				}

				$.each(oButtons, function(key,arr){
					aButtons.push(
						(oButtons.length > 3) ? 
						'<li action="'+ arr.sAction +'" data-field="'+ arr.sField +'"><a href="javascript:void(0)"><i class="'+ arr.sIcon +'"></i>'+ arr.sText +'</a></li>' :
						'<button action = "'+ arr.sAction +'" type = "button"  data-field="'+ arr.sField +'" class="'+ arr.sClass +'"><i class="'+ arr.sIcon +'"></i></button>');
				});
					
				return (
					(oButtons.length > 3) ? 
						'<div class="btn-group">'+
							'<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">'+
						  		'<span class="caret"></span>'+
							'</button>'+
							'<ul class="dropdown-menu" role="menu">'+
								aButtons.join('') +
							'</ul>'+
						'</div>' : '<div class="btn-group">'+aButtons.join('')+'</div>'
				);
			},
			createdCell : function(oTd, cellData, rowData, row, col)
			{
				$(oTd).find('[action]').click(function(){

					var sData = {
						sField: $(this)
					};

					oTables.action(this,oWidget.sPath,rowData);
				});
			}
		});

		var oSelf = this;
		var oTable = $('div[name="Modules"]').find('div[name="'+ aArg.sModuleName +'"]').find('table[name="'+ this.sTableName +'"]');
		
		oWidget.oTable = oTable.DataTable({
			serverSide : true,
			oLanguage : {
				sInfo : 'Showing page _END_ Source',
				oPaginate	: {
					sPrevious	: '<i class="ion-chevron-left"></i>',
					sNext		: '<i class="ion-chevron-right"></i>'
				}
			},
			processing: false,
			columns : oWidget.aDatatable.aColumns,
			ajax : function(aRequest, fCallback, aSettings)
			{
				var aColumns = oTables.aRequest( aRequest.columns );
				window.oData.send(oData.sUrl,
				{
					sModule: oWidget.sModule,
					sMethod: 'datatables/model_list/get',
					sTableName: oWidget.sTableName,
					sGroup: oWidget.sGroup,
					sType: 'datatables',
					aRequest: {
						aColumns: aColumns,
						aOrder: oTables.aOrder(aColumns,aRequest.order),
						aLimit : {
							iStart: aRequest.start,
							length: aRequest.length,
						},
						sSearch: aRequest.search
					}
				},
				function(aResp){
					
					if(aResp.iError && aResp.iError > 0) return false;
					
					if(aResp.aData.iDashboardCounts) {
						console.log(aResp);
						$('div[name="DashboardCounts"] div[name="iScheduledCount"] h3').html( aResp.aData.iDashboardCounts.iScheduledCount );
						$('div[name="DashboardCounts"] div[name="iDoctorsCount"] h3').html( aResp.aData.iDashboardCounts.iDoctorsCount );
						$('div[name="DashboardCounts"] div[name="iNursesCount"] h3').html( aResp.aData.iDashboardCounts.iNursesCount );
						$('div[name="DashboardCounts"] div[name="iPatientsCount"] h3').html( aResp.aData.iDashboardCounts.iPatientCount );
					}

					$('section.widget > div[name="Modules"]').children('div.row').css('display','none');	
					$('section.widget > div[name="Modules"]').children('div.row[name="'+ aArg.sModuleName +'"]').css('display','block');	
					$('div.content-wrapper section.content-header h1').empty().append( $('section.sidebar ul.sidebar-menu li a[name="'+ aArg.sModuleName +'"]').html() );

					fCallback(aResp.aData);
				});
			}
		});	
	},

	aRequest : function(aArg)
	{
		var aColumns = [];
		$.each(aArg, function(iKey,aRow){ 
			aColumns.push(aRow.data);
		});

		return aColumns;
	},

	aOrder : function(aColumns,aArg)
	{
		var aOrder = [];
		$.each(aArg,function(iKey,aRow){
			aOrder.push(aColumns[aRow.column]+' '+aRow.dir);
		});

		return aOrder;
	},

	/**
	 * Manages all actions from datatables
	 * @param  {object} oButton : Button that is responsible for the command.
	 * @param  {string} sPath   : Module location
	 * @return {[type]}         [description]
	 */
	action: function(oButton,sPath,aArg)
	{
		var aPath = sPath.split('/');
		var sAction = ($(oButton).attr('action').toLowerCase()).trim();

		if(sAction == 'edit' || sAction == 'add')
		{
			return oForms.modal(oModules.oWidgets[aPath[0]].aWidgets[aPath[1]],$(oButton).attr('action'),aArg);
		}
		else if(sAction == 'notify')
		{
			return oForms.notify(sPath,aArg,oButton)	
		}
		else
		{
			return oForms.confirm(sPath,aArg,oButton)
		}
	}
}

window.oForms = 
{
	/**
	 * Initializes the modal
	 * @param  {Array} aArg    : module's config
	 * @param  {array} aModule : module's info
	 * @return {[type]}        : null
	 */
	
	init : function(oWidget,aModule)
	{
		var oSelf = this;
		var oModal = $(oWidget.sForm).parents('div[name="'+ aModule.sModuleName +'"]');
		
		oWidget.oForm = $(oWidget.sForm).get(0);

		oWidget.oModal = oTools.modal(oModal, function(){
			oForms.reset(oWidget,oWidget.aValidation.rules)
		});
		
		$(oWidget.oForm).submit(function(){
			$(oWidget.oForm).valid() &&
			oActions.save(oWidget, null,function(aResp){
				oWidget.oTable && oWidget.oTable.ajax.reload();
				oForms.close(oWidget);
			});
		});
		
		$.each(oWidget.aValidation.rules, function(sIndex){
			var oField = $(oWidget.oForm).find('[name="'+ sIndex +'"]');
			if(oField) oWidget.oForm[oField] = oField && oForms.aFields[ $(oField).attr('field-type') ? $(oField).attr('field-type') : 'default' ].init(oField);
		});

		oSelf.validation(oWidget);
	},
	/**
	 * show or hide the modal
	 * @param  {Array} aArg    : module's config
	 * @param  {array} aModule : module's info
	 * @return {[type]}        : null
	 */
	modal : function(oWidget,sAction,aArg)
	{
		if(!oWidget.oModal) return;

		oWidget.form(aArg);
		$(oWidget.oModal.get(0)).attr('mode',sAction).find('.modal-title').html((sAction+' '+oWidget.sModule).toUpperCase());

		oWidget.oModal.modal('show');
	},
	/**
	 * Handles population of fields before modal pops out. Loops fields
	 * @param  {[type]} oWidget  [description]
	 * @param  {[type]} aRequest [description]
	 * @param  {[type]} aBundles [description]
	 * @param  {[type]} fCb      [description]
	 * @return {[type]}          [description]
	 */
	populate: function(oWidget,aRequest, aBundles, fCb, bInit)
	{
		if(aRequest) aRequest.aRequest.aColumns = oPhp.array_keys(oWidget.aValidation.rules);

		var aTempRequest = {
			sModule: oWidget.sModule,
			sMethod: 'main/get',
			sTableName: oWidget.sTableName,
			sGroup: oWidget.sGroup,
			aRequest: aRequest ? aRequest.aRequest : null,
			aBundles: aBundles[0]
		}

		if( $('div[name="Public"]') ){
			aTempRequest['sType'] = 'Public';	
		} 
 		
		oData.send(
			oData.sUrl, 
			aTempRequest,
			function(aResp){
				if(!aResp) return;
				
				if(oWidget.sModule == 'Dashboard'){

					aResp && aResp.aRequest && $(oWidget.oForm.sDayValue).val(aResp.aRequest[0]['iTransactionDate']);
				}
				
				$.each(oWidget.aValidation.rules, function(sKey){
					
					var oField = $(oWidget.oForm).find('[name="'+sKey+'"]');
					var sField = oField.attr('field-type') ? oField.attr('field-type') : 'default';
					
					if(oWidget.sModule == 'Dashboard' && sKey == 'iTime') {
						//no action
					}
					else
					{
						oForms.aFields[ sField ].populate(oField,aResp,aBundles,bInit);
					}
				});

				fCb && fCb(aResp);
			}
		);
	},
	notify : function(sPath,aArg,oButton)
	{
		if(!oTools.oConfirm) return;
		
		var sAction = ($(oButton).attr('action').toLowerCase()).trim();
		
		$(oTools.confirm).find('.modal-body').html(sAction+' the doctor and patient?');
		$(oTools.confirm).find('form').attr({'path' : sPath, 'data-field' : $(oButton).attr('data-field') });

		oTools.aArg = aArg;

		oTools.oConfirm.modal('show');
	},
	confirm : function(sPath,aArg,oButton)
	{
		if(!oTools.oConfirm) return;
		
		var sAction = ($(oButton).attr('action').toLowerCase()).trim();

		$(oTools.confirm).find('.modal-body').html('Are you sure you want to  '+sAction+' this record?');
		$(oTools.confirm).find('form').attr({'path' : sPath, 'data-field' : $(oButton).attr('data-field') });

		oTools.aArg = aArg;

		oTools.oConfirm.modal('show');
	},
	
	reset : function(oWidget,aFields){

		for(sField in aFields){
			var oField = $(oWidget.sForm).find('[name="'+ sField +'"]');
			oForms.aFields[ $(oField).attr('field-type') ? $(oField).attr('field-type') : 'default' ].reset(oField);
		};
	},
	close : function(oWidget)
	{
		return oWidget.oModal.modal('hide');
	},

	/**
	 * Get's the fields to be send
	 *  - Default
	 *  - Select/Multiple Select
	 *  - Toggle
	 *  - Date
	 *  - upload
	 *  - checkboxes
	 *  - Radiobuttons
	 * @param  {[type]} oForm   : container
	 * @param  {[type]} aFields : fields to be get
	 * @return {[type]}         : array({ [field name] : [value] })
	 */
	get: function(oForm,aFields)
	{
		var oSelf = this;
		var aReturn = {};

		for(sField in aFields){
			
			var oField = $(oForm).find('[name="'+sField+'"]');
			aReturn[sField] = oForms.aFields[ $(oField).attr('field-type') ? $(oField).attr('field-type') : 'default' ].get($(oField));
		};

		aReturn['mode'] = $(oForm).closest('div[mode]').attr('mode');

		return aReturn;
	},
	
	validation : function(oWidget)
	{
		$.validator.addMethod('bBetweenSchedule',function(value,element){
			var aSelected = ($(oWidget.oForm.sDay).find('option:selected').text()).split(' ');
			var iDateFrom = oPhp.strtotime( oPhp.date('Y m d',new Date()) + ' ' + aSelected[1]+" "+aSelected[2] );	
			var iDateTo = oPhp.strtotime( oPhp.date('Y m d',new Date()) + ' ' + aSelected[4]+" "+(aSelected[5]) );	

			return ((iDateFrom <= oPhp.strtotime(oPhp.date('Y m d',new Date()) + ' ' + value)) && (iDateTo > oPhp.strtotime(oPhp.date('Y m d',new Date()) + ' ' + value)) );
		},"Time should be between selected schedule");

		$(oWidget.oForm).validate(oWidget.aValidation);
	},
	
	aFields :
	{
		default :
		{
			init : function(oField)
			{
				return $(this).oField;
				//sVal && this.populate(oField,sVal);
			},
			populate : function(oField,aRequest,aBundles, bInit)
			{
				return aRequest.aRequest && $(oField).val( aRequest.aRequest[0][ $(oField).attr('name') ]);
			},
			get : function(oField)
			{
				if($(oField).length > 1)
				{
					var aReturn = [];
					$.each(oField, function(sKey,oVal){
						aReturn.push( $(oVal).val() );
					});
					return aReturn;
				}
				else
				{
					return $(oField).val();
				}
			},
			reset: function(oField)
			{
				return $(oField).val( $(oField).attr('field-default') ? $(oField).attr('field-default') : '' );
			}
		},
		select :
		{
			init : function(oField,sVal)
			{
				return $(this).oField;
			},
			populate : function(oField,aRequest,aBundles,bInit)
			{
				if(aBundles[1].indexOf( $(oField).attr('name') ) > -1)
				{
					aData = aRequest.aBundles[ aBundles[1].indexOf( $(oField).attr('name') ) ]
					var sOption = '<option value="0"></option>';

					if(oField.attr('name') == 'sDay')
					{	
						if($('div[name="Dashboard"] form').find('input[name="iTransactionId"]').val() || $('div[name="Public"] form').find('input[name="iTransactionId"]').val())
						{

							//EDIT
							$.each(aData,function(iKey,aVal){

								var aSelected = $('input[name="sDayValue"]').val().split(' ');

								var aCompare = aVal.sSchedule.split(' ');
								var selected = null;

								var bValid = ( aCompare[0] == aSelected[0] && 
										(oPhp.strtotime(oPhp.date('Y m d',new Date())+' '+aCompare[4]+' '+aCompare[5]) > oPhp.strtotime(oPhp.date('Y m d',new Date())+' '+aSelected[2]+' '+aSelected[3]) && 
											oPhp.strtotime(oPhp.date('Y m d',new Date())+' '+aCompare[1]+' '+aCompare[2]) <= oPhp.strtotime(oPhp.date('Y m d',new Date())+' '+aSelected[2]+' '+aSelected[3]) ) 
									);

								if(aVal.iSlots < aVal.iPatients || bValid)
								{
									if(bValid){										
										sOption+= '<option selected="selected">'+ aVal.sSchedule +'</option>';
									}
									else
									{
										sOption+= '<option>'+ aVal.sSchedule +'</option>';	
									}
									
								}
							});	

							return $(oField).empty().append(sOption);
						}
						else
						{
							//ADD
							$.each(aData,function(iKey,aVal){
								if(aVal.iSlots < aVal.iPatients) sOption+= '<option>'+ aVal.sSchedule +'</option>'
							});	

							return $(oField).empty().append(sOption);
						}
					}
					else
					{	
						$.each(aData,function(iKey,sVal)
						{
							if( aRequest.aRequest && iKey == aRequest.aRequest[0][oField.attr('name')] ){
								sOption+= '<option value="'+iKey+'" selected = "selected">'+ sVal +'</option>';
							}
							else 
							{
								sOption+= '<option value="'+iKey+'">'+ sVal +'</option>';
							}
						});
	
					}
					
					$(oField).empty().append(sOption).change(function(){
						if(!$(this).val() || !$(this).attr('chain-module')) return;

						var sRequest = oCommons.modules[$(this).attr('chain-module')];
						sRequest.aRequest[$(this).attr('name')] = $(this).val();

		 				var aBundles = [[sRequest],[$(this).attr('chain-name')]];
		 				var aModules = ($(oField).closest('div[path]').attr('path')).split('/');

						oForms.populate(oModules.oWidgets[aModules[0]].aWidgets[aModules[1]],null,[[sRequest],[ $(this).attr('chain-name') ]]);	
					});
				}	
				if(aRequest.aRequest && $(oField).attr('name') == 'iDoctorId' && $(oField).attr('chain-module') ) 
					$(oField).val( aRequest.aRequest[0][ $(oField).attr('name') ]).trigger('change');
			},
			get : function(oFields)
			{
				if(oFields.length > 1)
				{
					var aReturn = [];
					$.each(oFields, function(sKey,oField){
						aReturn.push( $(oField).val() );
					});
					
					return aReturn;
				}
				else
				{
					if(oFields.attr('name') == 'sDay'){
						var aSplit = ($(oFields).find('option:selected').text()).split(' ');
						return aSplit[0];
					} else {
						return $(oFields).val();
					}
				}
			},

			reset: function(oField)
			{
				return $(oField).val( $(oField).attr('field-default') ? $(oField).attr('field-default') : '' );	
			}

		},	
		Checkbox : 
		{
			init : function(oField,aData,aSelected)
			{
				
			},
			populate : function(oField,aRequest,aBundles,bInit)
			{
				var sTemp = "";

				$.each(aRequest.aBundles[aBundles[1].indexOf( $(oField).attr('name') )], function(iKey,sVal){
					sTemp+= '<div class="col-sm-12">'+
								'<div class="form-group">'+
									'<label><input name="'+ $(oField).attr('field-name') +'" value="'+ iKey +'" type="checkbox" class="minimal" '+ (aRequest.aRequest && aRequest.aRequest[$(oField).attr('name')][iKey] ? 'checked' : '') +'></label> '+sVal+
								'</div>'+
							'</div>';
				});

				$(oField).next('.box').find('.box-body').empty().append(sTemp);
				$(oField).next('.box').find('.box-body').find('input[type="checkbox"].minimal').iCheck({
					checkboxClass: 'icheckbox_minimal-grey',
				}).on('ifChanged', function(e) {
					$(this).attr('checked',e.currentTarget.checked ? true : false);
				});
			},
			get : function(oField)
			{
				var aSelected = {};
				$.each( $(oField).next('.box').find('.box-body').find('input[name="'+ $(oField).attr('field-name') +'"]'), function(sKey){
					if($(this).attr('checked')) aSelected[sKey] = $(this).val();
				});
				
				return aSelected;
			},	
			reset : function(oField)
			{

			}
		},
		Datepicker : 
		{
			init: function(oField)
			{
				oField.datetimepicker({
			        format: oField.attr('widget-date-format') ? oField.attr('widget-date-format') : 'M dd, yyyy (D)',
			        autoclose: true,
			        todayBtn: true,
					minView: 2
			    });

				oField.val(oPhp.date( this.sAttribute(oField) ,new Date()) );

			},
			populate : function(oField,sVal,bInit)
			{
				if(!oField) return;
				return oField.val(oPhp.date( this.sAttribute(oField) ,sVal) );
			},
			get : function(oFields)
			{
				if(oFields.length > 1)
				{
					var aReturn = [];
					$.each(oFields, function(sKey,oField){
						aReturn.push( $(oField).val() ? oPhp.strtotime($(oField).val()) : null );
					});
					
					return aReturn;
				}
				else
				{
					return $(oFields).val() ? oPhp.strtotime($(oFields).val()) : null;
				}
			},
			reset : function(oField)
			{
				//return $(oField).Timepicker.getTime();	
			},
			sAttribute : function(oField)
			{
				return $(oField).attr('widget-date-format') ? oField.attr('widget-date-format') : 'M d, Y (D)';
			}
		},

		Timepicker : 
		{
			init : function(oFields)
			{
				$(oFields).timepicker({ 
					showInputs: false,
					minuteStep: 15
				});

				if(oFields.length > 1)
				{
					$.each(oFields, function(sKey,oField){
						$(oField).timepicker('setTime', $(oField).attr('field-default') ? $(oField).attr('field-default') : oPhp.date('g:i A',new Date())).attr('readonly','readonly');
					});
				}
			},
			populate : function(oField,aResp,bInit)
			{
				if(!oField) return;
				
				$(oField).timepicker('setTime',oPhp.date('g:i A',aResp));
			},
			get : function(oField)
			{
				if($(oField).length > 1)
				{
					var aReturn = [];
					$.each(oField, function(sKey,oVal){
						aReturn.push( $(oVal).val() ? oPhp.strtotime(oPhp.date('Y m d',new Date())+' '+$(oVal).val()) : null );
					});
					
					return aReturn;
				}
				else
				{
					return $(oField).val() ? oPhp.strtotime(oPhp.date('Y m d',new Date())+' '+$(oField).val()) : null;
				}
			},
			reset : function(oField)
			{
				//return $(oField).Timepicker.getTime();	
			}
		}
	}

}

window.oActions = 
{
	save : function(oWidget,aArg, fCb)
	{
		var aTemp = {
			sMethod: 'main/save',
			sModule: oWidget.sModule,
			sTableName: oWidget.sTableName,
			sGroup : oWidget.sGroup,
			aRequest : aArg ? aArg : oForms.get(oWidget.oForm, oWidget.aValidation.rules)
		};

		if( $('div[name="Public"]') ){
			aTemp['sType'] = 'Public';	
		} 
		
		oData.send(oData.sUrl,aTemp,function(aResp){
			if($('div[name="Public"]')) 
			{
				if(aArg.mode && aArg.mode == 'edit' && aArg.bActive && aArg.bActive == 0)
				{
					if($('div[name="Public"]').attr('mode') == 'edit') $('div[name="Public"] .login-box-msg').empty().append('Reservation Cancelled!').css('color','green');
				}
				else
				{
					if($('div[name="Public"]').attr('mode') == 'add') $('div[name="Public"] .login-box-msg').empty().append('You have susccessfully reserved your transction! Transaction Code: <b>'+ aResp.aData.sTransactionCode+'</b><br> You can edit your schedule by entering your transaction code on the search box below.').css('color','green');
					if($('div[name="Public"]').attr('mode') == 'edit') $('div[name="Public"] .login-box-msg').empty().append('Reservation Updated!').css('color','green');
				}

				oForms.reset(oWidget,oWidget.aValidation.rules);
			}
			fCb && fCb();
		});
	},

	notify : function(oWidget,aArg, fCb)
	{
		var aTemp = {
			sMethod: 'main/sms',
			sModule: oWidget.sModule,
			sTableName: oWidget.sTableName,
			sGroup : oWidget.sGroup,
			aRequest : aArg ? aArg : oForms.get(oWidget.oForm, oWidget.aValidation.rules)
		};

		oData.send(oData.sUrl,aTemp,function(aResp){
			fCb && fCb();
		});
	},

	upload : function()
	{

	}
}

window.oData = {
	sUrl: 'api',

	parse: function(aData,fCb){

	},

	send: function(sUrl,aData,fCb){
		if(!aData.aRequest && !aData.aBundles.length) return;
		
		var oSelf = this;
		
		$.ajax({
			url: oSelf.sUrl,
			data: window.btoa(JSON.stringify(aData)),
			method: 'POST',
			success: function(aResp) {
				var aResp = JSON.parse(window.atob(aResp));

				if(aResp.bNotify){
					if(aResp.iError == '1005') location.reload();
					oTools.Toastr.notify(aResp);
				} 
				
				fCb && fCb(aResp);
			},
			error: function(jqxhr,sStatus,errorThrown){

			}
		});
	}
}

window.oCommons = {
	modules : {
		Cities : 
		{
			sModule: 'Cities',
			sGroup: 'Management',
			sTableName: 'cities',
			aRequest	: {
				sIndex : 'iCityId',
				sColumn : 'sCity',
				aColumns	: [
					'iCityId',
					'sCity'
				]
			}
		},
		Doctors : 
		{
			sModule: 'Doctors',
			sGroup: 'Management',
			sTableName: 'doctors',
			aRequest	: {
				sIndex : 'iDoctorId',
				sColumn : 'sFullName',
				aColumns	: [
					'iDoctorId',
					'sFullName'
				]
			}
		},
		DoctorSchedules : 
		{
			sModule: 'DoctorSchedules',
			sGroup: 'Management',
			sTableName: 'doctors_schedules',
			aRequest	: {
				sColumn: 'sSchedule',
				aColumns	: [
					'sSchedule'
				]
			}
		}
	}
}

