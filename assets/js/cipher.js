window.oCipher = {
	cMode : 'cbc',
	cCipher : 'rijndael-128',
	cKey : '12345678911234567892123456789312',
	ciphers : {
		'rijndael-128' : [16, 32],
		'rijndael-192' : [24, 32],
		'rijndael-256' : [32, 32]
	},

	blockCipherCalls : {
		'rijndael-128' : function(cipher, block, key, encrypt, oSelf) {
			return this.rijndael(cipher, block, key, encrypt, oSelf);
		},

		'rijndael-192' : function(cipher, block, key, encrypt, oSelf) {
			return this.rijndael(cipher, block, key, encrypt, oSelf);
		},

		'rijndael-256' : function(cipher, block, key, encrypt, oSelf) {
			return this.rijndael(cipher, block, key, encrypt, oSelf);
		},

		rijndael : function(cipher, block, key, encrypt, oSelf) {
			if (key.length < 32)
				key += Array(33 - key.length).join(String.fromCharCode(0));
			if (encrypt)
				oSelf.aCiphers.Rijndael.Encrypt(block, key);
			else
				oSelf.aCiphers.Rijndael.Decrypt(block, key);
			return block;
		}
	},

	Encrypt : function(aArg) {
		return window.btoa(this.Crypt(true, aArg.sText, aArg.sIv, aArg.sKey, aArg.sCipher, aArg.sMode));
	},

	Decrypt : function(aArg) {
		return this.Crypt(false, window.atob(aArg.sText), aArg.sIv, aArg.sKey, aArg.sCipher, aArg.sMode);
	},

	Crypt : function(encrypt, text, IV, key, cipher, mode) {
		if (key)
			this.cKey = key;
		else
			key = this.cKey;
		if (cipher)
			this.cCipher = cipher;
		else
			cipher = this.cCipher;
		if (mode)
			this.cMode = mode;
		else
			mode = this.cMode;
		if (!text)
			return true;
		if (this.blockCipherCalls[cipher].init)
			this.blockCipherCalls[cipher].init(cipher, key, encrypt);
		var blockS = this.ciphers[cipher][0];
		var chunkS = blockS;
		var iv = new Array(blockS);
		switch(mode) {
		case 'cfb':
			chunkS = 1;
		case 'cbc':
		case 'ncfb':
		case 'nofb':
		case 'ctr':
			if (!IV)
				throw 'mcrypt.Crypt: IV Required for mode ' + mode;
			if (IV.length != blockS)
				throw 'mcrypt.Crypt: IV must be ' + blockS + ' characters long for ' + cipher;
			for (var i = blockS - 1; i >= 0; i--)
				iv[i] = IV.charCodeAt(i);
			break;
		case 'ecb':
			break;
		default:
			throw 'mcrypt.Crypt: Unsupported mode of opperation' + this.cMode;
		}
		var chunks = Math.ceil(text.length / chunkS);
		var orig = text.length;
		text += Array(chunks * chunkS - orig + 1).join(String.fromCharCode(0));
		var out = '';
		switch(mode) {
		case 'ecb':
			for (var i = 0; i < chunks; i++) {
				for (var j = 0; j < chunkS; j++)
					iv[j] = text.charCodeAt((i * chunkS) + j);
				this.blockCipherCalls[cipher](cipher, iv, this.cKey, encrypt, this);
				for (var j = 0; j < chunkS; j++)
					out += String.fromCharCode(iv[j]);
			}
			break;
		case 'cbc':
			if (encrypt) {
				for (var i = 0; i < chunks; i++) {
					for (var j = 0; j < chunkS; j++)
						iv[j] = text.charCodeAt((i * chunkS) + j) ^ iv[j];
					this.blockCipherCalls[cipher](cipher, iv, this.cKey, true, this);
					for (var j = 0; j < chunkS; j++)
						out += String.fromCharCode(iv[j]);
				}
			} else {
				for (var i = 0; i < chunks; i++) {
					var temp = iv;
					iv = new Array(chunkS);
					for (var j = 0; j < chunkS; j++)
						iv[j] = text.charCodeAt((i * chunkS) + j);
					var decr = iv.slice(0);
					this.blockCipherCalls[cipher](cipher, decr, this.cKey, false, this);
					for (var j = 0; j < chunkS; j++)
						out += String.fromCharCode(temp[j] ^ decr[j]);
				}
			}
			break;
		case 'cfb':
			for (var i = 0; i < chunks; i++) {
				var temp = iv.slice(0);
				this.blockCipherCalls[cipher](cipher, temp, this.cKey, true, this);
				temp = temp[0] ^ text.charCodeAt(i);
				iv.push( encrypt ? temp : text.charCodeAt(i));
				iv.shift();
				out += String.fromCharCode(temp);
			}
			out = out.substr(0, orig);
			break;
		case 'ncfb':
			for (var i = 0; i < chunks; i++) {
				this.blockCipherCalls[cipher](cipher, iv, this.cKey, true, this);
				for (var j = 0; j < chunkS; j++) {
					var temp = text.charCodeAt((i * chunkS) + j);
					iv[j] = temp ^ iv[j];
					out += String.fromCharCode(iv[j]);
					if (!encrypt)
						iv[j] = temp;
				}
			}
			out = out.substr(0, orig);
			break;
		case 'nofb':
			for (var i = 0; i < chunks; i++) {
				this.blockCipherCalls[cipher](cipher, iv, this.cKey, true, this);
				for (var j = 0; j < chunkS; j++)
					out += String.fromCharCode(text.charCodeAt((i * chunkS) + j) ^ iv[j]);
			}
			out = out.substr(0, orig);
			break;
		case 'ctr':
			for (var i = 0; i < chunks; i++) {
				temp = iv.slice(0);
				this.blockCipherCalls[cipher](cipher, temp, this.cKey, true, this);
				for (var j = 0; j < chunkS; j++)
					out += String.fromCharCode(text.charCodeAt((i * chunkS) + j) ^ temp[j]);
				var carry = 1;
				var index = chunkS;
				do {
					index--;
					iv[index] += 1;
					carry = iv[index] >> 8;
					iv[index] &= 255;
				} while(carry);
			}
			out = out.substr(0, orig);
			break;
		}
		if (this.blockCipherCalls[cipher].deinit)
			this.blockCipherCalls[cipher].deinit(cipher, key, encrypt);
		return out;
	},

	get_block_size : function(cipher, mode) {
		if (!cipher)
			cipher = this.cCipher;
		if (!this.ciphers[cipher])
			return false;
		return this.ciphers[cipher][0];
	},

	get_cipher_name : function(cipher) {
		if (!cipher)
			cipher = this.cCipher;
		if (!this.ciphers[cipher])
			return false;
		return cipher;
	},

	get_iv_size : function(cipher, mode) {
		if (!cipher)
			cipher = this.cCipher;
		if (!this.ciphers[cipher])
			return false;
		return this.ciphers[cipher][0];
	},

	get_key_size : function(cipher, mode) {
		if (!cipher)
			cipher = this.cCipher;
		if (!this.ciphers[cipher])
			return false;
		return this.ciphers[cipher][1];
	},

	gen_random : function(sHash, iLen) {
		return this.sha1(Math.floor(Date.now() * Math.random() * Math.random()) + sHash).substr(Math.random() * (40 - iLen), iLen);
	},

	gen_hash : function(sHash, iStart, iLen) {
		return this.sha1(sHash.substr(iStart, iLen)).substr(iStart, iLen);
	},

	list_algorithms : function() {
		var ret = [];
		for (var i in this.ciphers)
		ret.push(i);
		return ret;
	},

	list_modes : function() {
		return ['ecb', 'cbc', 'cfb', 'ncfb', 'nofb', 'ctr'];
	},

	aCiphers : {
		Rijndael : {

			expandedKeys : {},

			xtime : [],

			Sbox_Inv : [],

			ShiftRowTab_Inv : [],

			sizes : [16, 24, 32],

			rounds : [[10, 12, 14], [12, 12, 14], [14, 14, 14]],

			Sbox : [99, 124, 119, 123, 242, 107, 111, 197, 48, 1, 103, 43, 254, 215, 171, 118, 202, 130, 201, 125, 250, 89, 71, 240, 173, 212, 162, 175, 156, 164, 114, 192, 183, 253, 147, 38, 54, 63, 247, 204, 52, 165, 229, 241, 113, 216, 49, 21, 4, 199, 35, 195, 24, 150, 5, 154, 7, 18, 128, 226, 235, 39, 178, 117, 9, 131, 44, 26, 27, 110, 90, 160, 82, 59, 214, 179, 41, 227, 47, 132, 83, 209, 0, 237, 32, 252, 177, 91, 106, 203, 190, 57, 74, 76, 88, 207, 208, 239, 170, 251, 67, 77, 51, 133, 69, 249, 2, 127, 80, 60, 159, 168, 81, 163, 64, 143, 146, 157, 56, 245, 188, 182, 218, 33, 16, 255, 243, 210, 205, 12, 19, 236, 95, 151, 68, 23, 196, 167, 126, 61, 100, 93, 25, 115, 96, 129, 79, 220, 34, 42, 144, 136, 70, 238, 184, 20, 222, 94, 11, 219, 224, 50, 58, 10, 73, 6, 36, 92, 194, 211, 172, 98, 145, 149, 228, 121, 231, 200, 55, 109, 141, 213, 78, 169, 108, 86, 244, 234, 101, 122, 174, 8, 186, 120, 37, 46, 28, 166, 180, 198, 232, 221, 116, 31, 75, 189, 139, 138, 112, 62, 181, 102, 72, 3, 246, 14, 97, 53, 87, 185, 134, 193, 29, 158, 225, 248, 152, 17, 105, 217, 142, 148, 155, 30, 135, 233, 206, 85, 40, 223, 140, 161, 137, 13, 191, 230, 66, 104, 65, 153, 45, 15, 176, 84, 187, 22],

			rowshifts : [[0, 1, 2, 3], [0, 1, 2, 3], [0, 1, 3, 4]],

			ShiftRowTab : [3],

			Encrypt : function(block, key) {
				this.crypt(block, key, true);
			},

			Decrypt : function(block, key) {
				this.crypt(block, key, false);
			},

			ExpandKey : function(key) {
				if (!this.expandedKeys[key]) {
					var kl = key.length,
					    ks,
					    Rcon = 1;
					ks = 15 << 5;
					var keyA = new Array(ks);
					for (var i = 0; i < kl; i++)
						keyA[i] = key.charCodeAt(i);
					for (var i = kl; i < ks; i += 4) {
						var temp = keyA.slice(i - 4, i);
						if (i % kl == 0) {
							temp = [this.Sbox[temp[1]] ^ Rcon, this.Sbox[temp[2]], this.Sbox[temp[3]], this.Sbox[temp[0]]];
							if ((Rcon <<= 1) >= 256)
								Rcon ^=0x11b;
						} else if ((kl > 24) && (i % kl == 16))
							temp = [this.Sbox[temp[0]], this.Sbox[temp[1]], this.Sbox[temp[2]], this.Sbox[temp[3]]];
						for (var j = 0; j < 4; j++)
							keyA[i + j] = keyA[i + j - kl] ^ temp[j];
					}
					this.expandedKeys[key] = keyA;
				}
				return this.expandedKeys[key];
			},

			crypt : function(block, key, encrypt) {
				var bB = block.length;
				var kB = key.length;
				var bBi = 0;
				var kBi = 0;
				switch(bB) {
				case 32:
					bBi++;
				case 24:
					bBi++;
				case 16:
					break;
				default:
					throw 'rijndael: Unsupported block size: ' + block.length;
				}
				switch(kB) {
				case 32:
					kBi++;
				case 24:
					kBi++;
				case 16:
					break;
				default:
					throw 'rijndael: Unsupported key size: ' + key.length;
				}
				var r = this.rounds[bBi][kBi];
				key = this.ExpandKey(key);
				var end = r * bB;

				this.init();
				if (encrypt) {
					this.AddRoundKey(block, key.slice(0, bB));
					var SRT = this.ShiftRowTab[bBi];
					for (var i = bB; i < end; i += bB) {
						this.SubBytes(block, this.Sbox);
						this.ShiftRows(block, SRT);
						this.MixColumns(block);
						this.AddRoundKey(block, key.slice(i, i + bB));
					}
					this.SubBytes(block, this.Sbox);
					this.ShiftRows(block, SRT);
					this.AddRoundKey(block, key.slice(i, i + bB));
				} else {
					this.AddRoundKey(block, key.slice(end, end + bB));
					var SRT = this.ShiftRowTab_Inv[bBi];
					this.ShiftRows(block, SRT);
					this.SubBytes(block, this.Sbox_Inv);
					for (var i = end - bB; i >= bB; i -= bB) {
						this.AddRoundKey(block, key.slice(i, i + bB));
						this.MixColumns_Inv(block);
						this.ShiftRows(block, SRT);
						this.SubBytes(block, this.Sbox_Inv);
					}
					this.AddRoundKey(block, key.slice(0, bB));
				}

			},

			init : function() {
				for (var i = 0; i < 3; i++) {
					this.ShiftRowTab[i] = Array(this.sizes[i]);
					for (var j = this.sizes[i]; j >= 0; j--)
						this.ShiftRowTab[i][j] = (j + (this.rowshifts[i][j & 3] << 2)) % this.sizes[i];
				}
				this.Sbox_Inv = new Array(256);
				for (var i = 0; i < 256; i++)
					this.Sbox_Inv[this.Sbox[i]] = i;
				this.ShiftRowTab_Inv = Array(3);
				for (var i = 0; i < 3; i++) {
					this.ShiftRowTab_Inv[i] = Array(this.sizes[i]);
					for (var j = this.sizes[i]; j >= 0; j--)
						this.ShiftRowTab_Inv[i][this.ShiftRowTab[i][j]] = j;
				}
				this.xtime = new Array(256);
				for (var i = 0; i < 128; i++) {
					this.xtime[i] = i << 1;
					this.xtime[128 + i] = (i << 1) ^ 0x1b;
				}
			},

			SubBytes : function(state, sbox) {
				for (var i = state.length - 1; i >= 0; i--)
					state[i] = sbox[state[i]];
			},

			AddRoundKey : function(state, rkey) {
				for (var i = state.length - 1; i >= 0; i--)
					state[i] ^=rkey[i];
			},

			ShiftRows : function(state, shifttab) {
				var h = state.slice(0);
				for (var i = state.length - 1; i >= 0; i--)
					state[i] = h[shifttab[i]];
			},

			MixColumns : function(state) {
				for (var i = state.length - 4; i >= 0; i -= 4) {
					var s0 = state[i + 0],
					    s1 = state[i + 1];
					var s2 = state[i + 2],
					    s3 = state[i + 3];
					var h = s0 ^ s1 ^ s2 ^ s3;
					state[i + 0] ^=h ^ this.xtime[s0 ^ s1];
					state[i + 1] ^=h ^ this.xtime[s1 ^ s2];
					state[i + 2] ^=h ^ this.xtime[s2 ^ s3];
					state[i + 3] ^=h ^ this.xtime[s3 ^ s0];
				}
			},

			MixColumns_Inv : function(state) {
				for (var i = state.length - 4; i >= 0; i -= 4) {
					var s0 = state[i + 0],
					    s1 = state[i + 1];
					var s2 = state[i + 2],
					    s3 = state[i + 3];
					var h = s0 ^ s1 ^ s2 ^ s3;
					var xh = this.xtime[h];
					var h1 = this.xtime[this.xtime[xh ^ s0 ^ s2]] ^ h;
					var h2 = this.xtime[this.xtime[xh ^ s1 ^ s3]] ^ h;
					state[i + 0] ^=h1 ^ this.xtime[s0 ^ s1];
					state[i + 1] ^=h2 ^ this.xtime[s1 ^ s2];
					state[i + 2] ^=h1 ^ this.xtime[s2 ^ s3];
					state[i + 3] ^=h2 ^ this.xtime[s3 ^ s0];
				}
			}
		}
	}
}; 