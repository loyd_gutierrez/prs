window.oCommon =
{
	aModules : {
		Agencies : {
			sMethod		: 'get',
			sModule		: 'Agencies',
			aRequest	: {
				sIndex		: 'iAgencyId',
				sColumn		: 'sAgencyCode',
				aColumns	: [
					'iAgencyId',
					'sAgencyCode'
				]
			}
		},
		ApplicationStatuses : {
			sMethod		: 'get',
			sModule		: 'ApplicationStatuses',
			aRequest	: {
				sIndex		: 'iApplicationStatusId',
				sColumn		: 'sApplicationStatusName',
				aColumns	: [
					'iApplicationStatusId',
					'sApplicationStatusName'
				]
			}
		},
		CityMunicipalities : {
			sMethod		: 'get',
			sModule		: 'CityMunicipalities',
			aRequest	: {
				sIndex		: 'iCityMunicipalityId',
				sColumn		: 'sCityMunicipalityName',
				aColumns	: [
					'iCityMunicipalityId',
					'sCityMunicipalityName'
				]
			}
		},
		
		Countries : {
			sMethod		: 'get',
			sModule		: 'Countries',
			aRequest	: {
				sIndex		: 'iCountryId',
				sColumn		: 'sCountryName',
				aColumns	: [
					'iCountryId',
					'sCountryName'
				]
			}
		},

		FileIcons : {
			sMethod		: 'get',
			sModule		: 'FileIcons',
			aRequest	: {
				sIndex		: 'iFileIconId',
				sColumn		: 'sFileIconName',
				aColumns	: [
					'iFileIconId',
					'sFileIconName'
				]
			}
		},
		Modules : {
			sMethod		: 'get',
			sModule		: 'Modules',
			aRequest	: {
				sIndex		: 'iModuleId',
				aColumns	: [
					'iModuleId',
					'sModuleName',
				],
				aModulePermissions : {
					aOrder		: [
						'iBitIndex ASC'
					],
					aColumns	: [
						'iPermissionId',
						'sPermissionName'
					]
				}
			}
		},
		MetaGroups : {
			sMethod		: 'get',
			sModule		: 'MetaGroups',
			aRequest	: {
				sIndex		: 'iMetaGroupId',
				sColumn		: 'sMetaGroupCode',
				aColumns	: [
					'iMetaGroupId',
					'sMetaGroupCode'
				]
			}
		},
		
		Permissions: {
			sMethod		: 'get',
			sModule		: 'Permissions',
			aRequest	: {
				sIndex		: 'iPermissionId',
				aColumns	: [
					'iPermissionId',
					'sPermissionName'
				]
			}
		},
		Roles : {
			sMethod		: 'get',
			sModule		: 'Roles',
			aRequest	: {
				sIndex	: 'iRoleId',
				sColumn	: 'sRoleName',
				aColumns	: [
					'iRoleId',
					'sRoleName'
				]
			}
		},
		Users : {
			sMethod		: 'get',
			sModule		: 'Users',
			aRequest	: {
				sIndex	: 'iUserId',
				sColumn	: 'sFullName',
				aColumns	: [
					'iUserId',
					'sFullName'
				]
			}
		},
		VisaCategories : {
			sMethod 	: 'get',
			sModule 	: 'VisaCategories',
			aRequest 	: {
				sIndex 		: 'iVisaCategoryId',
				sColumn 	: 'sVisaCategoryName',
				aColumns 	: [
					'iVisaCategoryId',
					'sVisaCategoryName'
				]
			}
		},
		VisaDeliveries : {
			sMethod		: 'get',
			sModule 	: 'VisaDeliveries',
			aRequest 	: {
				sIndex 		: 'iVisaDeliveryId',
				sColumn 	: 'sVisaDeliveryName',
				aColumns 	: [
					'iVisaDeliveryId',
					'sVisaDeliveryName'
				]
			}
		}
	},

	options : function(sModule, aArg, cCb)
	{
		oSecret.call(oSettings.sUrl+'api/', {
			sMethod	: 'dt/item/get',
			aArg	: {
				sModule		: sModule,
				aRequest	: oPhp.array_merge(aArg, this.aModules[sModule].aRequest)
			}
		}, cCb);
	}	
};