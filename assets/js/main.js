

$(document).ready(
	function()
	{
		oMain.init();
		setTimeout(function(){
			$(window).resize();
		}, 0);
	}
);
$(window).resize(
	function()
	{
		oResize.adjust();
	}
);

window.oActions =
{
	execute : function(aArg)
	{
		if (aArg.aData && aArg.aData.aActions) {
			for (var i = 0, iL = aArg.aData.aActions.length; i < iL; i++) {
				for (var sAction in aArg.aData.aActions[i]) {
					oActions[sAction](aArg.aData.aActions[i][sAction], aArg);
				}
			}
		}
	},

	setPriKey : function(aArg)
	{
		oSecret.setKey(aArg);
	},

	setUserInfo : function(aArg)
	{
		oInit.settings(aArg);
	},

	login : function()
	{
		$('body').toggleClass('login-page', true);
		oMain.init();
	},

	logout : function()
	{
		oSecret.call(oSettings.sUrl+'api/', {
			sMethod	: 'users/auth/logout',
			aArg	: {
				aRequest	: {
				}
			}
		}, function(aResp) {
			oSettings.clear();
			location.reload();
		});
	}
};

window.oData =
{
	iBits : 256,
	iTimeOut : 30000,

	call : function(sUrl, aArg, cCb)
	{
		var iNoCache	= aArg.iNoCache || new Date().valueOf();

		aArg.iNoCache && (delete aArg.iNoCache);
		$.ajax({
			url			: (sUrl + '?nocache=' + iNoCache),
			type		: 'POST',
			dataType	: 'text',
			processData	: false,
			timeout		: this.iTimeOut,
			data		: window.btoa(JSON.stringify(aArg)),
			success		: function(sResp)
			{
				var aResp	= oData.parse(sResp);

				oData.check(aResp) && cCb && cCb(aResp);
			},

			complete : function(oXhr, sStatus) {
				
			},

			error : function(oXhr, sStatus, sMsg) {

			}
		});
	},

	check : function(aArg)
	{
		aArg.iErr && oDialog.error(aArg);

		oActions.execute(aArg);

		return !aArg.iErr; 
	},

	parse : function(sResp)
	{
		try {
			var aResp	= JSON.parse(sResp);
		} catch(e) {
			try {
				var aResp	= oSecret.decrypt(sResp);
			} catch(e) {
				var aResp	= {
					iErr : 1,
					sMsg : 'Failed to parse data'
				};
			}
		}

		return aResp;
	}
};

window.oDialog =
{
	message : function(aArg)
	{
		toastr[aArg.sType](aArg.sMsg, aArg.sTitle ? aArg.sTitle : '');
	},

	success : function(aArg)
	{
		aArg.sType	= 'success';

		this.message(aArg);
	},

	info : function(aArg)
	{
		aArg.sType	= 'info';

		this.message(aArg);
	},

	warning : function(aArg)
	{
		aArg.sType	= 'warning';

		this.message(aArg);
	},

	error : function(aArg)
	{
		aArg.sType	= 'error';

		this.message(aArg);
	},

	confirm : function(aArg, cCb)
	{
		var oForm		= $('.modal[name="Message"]').modal({
			keyboard	: false
		});

		oForm.find('.modal-title').empty().append(aArg.sTitle);
		oForm.find('.modal-body').empty().append(aArg.sMsg);

		$('.modal[name="Message"] [name="Confirm"]').get(0).onclick = function()
		{
			$(this).siblings('button[name="Cancel"]').get(0).click();
			cCb && cCb();
		};
	}
};

window.oFiles =
{
	sRoute : null,

	sPrevious : null,

	aModals : [
		'Users',
		'Groups'
	],	

	setRoute : function(sRoute)
	{
		if (!($.inArray(sRoute, this.aModals) < 0)) {			
			this.sPrevious	= this.sRoute;
		}

		this.sRoute	= sRoute;
	},

	returnRoute : function()
	{
		this.sRoute	= this.sPrevious;
	},

	aRoutes :
	{
		Users : function(aFiles)
		{
			oModules.Users.upload(aFiles);
		},

		Personnels : function(aFiles)
		{
			oModules.Personnels.upload(aFiles);
		},

		Students : function(aFiles)
		{
			oModules.Students.upload(aFiles);
		},

		Teachers : function(aB64Files, aRawFiles)
		{
			oModules.Teachers.upload(aB64Files, aRawFiles);
		},

		Parents : function(aB64Files, aRawFiles)
		{
			oModules.Parents.upload(aB64Files, aRawFiles);
		},

		ActivityObservations : function(aB64Files, aRawFiles)
		{
			oModules.ActivityObservations.upload(aB64Files, aRawFiles);
		}
	},

	oFileUpload : {
		removeDataUriScheme	: true,
		overClass			: 'FilesHover',
		addClassTo			: 'body',
		onFileRead			: function(aFiles, aRawFiles)
		{
			oFiles.sRoute && oFiles.aRoutes[oFiles.sRoute](aFiles, aRawFiles);
		}
	},

	init : function()
	{
		$('html').fileDrop(this.oFileUpload);
	},

	upload : function(oNode, ev)
	{
		oFiles.drop(ev, this.oFileUpload);

		$(oNode).replaceWith($(oNode).clone(true));
	},

    stopEvent : function(ev)
    {
        ev.stopPropagation();
        ev.preventDefault();
    },

    decodeBase64String : function (str) {
        var decoded = window.atob(str);
        try {
            return decodeURIComponent(window.escape(decoded));
        } catch (ex) {
            return '';
        }
    },

	drop : function(ev, opts)
	{
		opts && $(opts.addClassTo).removeClass(opts.overClass);

		this.stopEvent(ev);

		var fileList	= (
			ev.dataTransfer ?
			ev.dataTransfer.files :
			(
				ev.target ?
				ev.target.files :
				[]
			)
		);

		for (var i = 0, fileArray = []; i <= fileList.length - 1; i++) {

			var reader		= new FileReader();
			var completeFn	= this.handleFile(fileList[i], fileArray, fileList.length, opts, fileList);

			reader.addEventListener ?
			reader.addEventListener('loadend', completeFn, false) :
			reader.onloadend = completeFn;

			reader.readAsDataURL(fileList[i]);
		}
	},

	handleFile : function (theFile, fileArray, fileCount, opts, fileList)
	{
		return function (ev)
		{
			var fileData = ev.target.result;

			opts && opts.removeDataUriScheme && (fileData = $.removeUriScheme(fileData));
			opts && opts.decodeBase64 && (fileData = oFiles.decodeBase64String(fileData));

			fileArray.push({
				sName	: theFile.name,
				iSize	: theFile.size,
				sMime	: theFile.type,
				sAlter	: theFile.lastModifiedDate,
				sData	: fileData
			});

			if (fileArray.length === fileCount && $.isFunction(opts.onFileRead)) {
				opts.onFileRead(fileArray, fileList);
			}
		};
	}
};

window.oForms =
{
	init : function(oWidget, aArg)
	{
		if (oTools.isInit($(oWidget.sForm))) return;

		var aFields	= oWidget.aValidation.rules;
		var sRole	= oTools.getRole(oWidget.sForm);
		var aTitles	= {
			page	: '.box-title',
			dialog	: '.modal-title'
		};

		$(oWidget.sForm+' '+aTitles[sRole]+':first').empty().append(aArg.oHeader.clone(true));

		oWidget.sForm && (oWidget.oForm = $(oWidget.sForm).get(0));
		oWidget.sList && (oWidget.oList = $(oWidget.sList).get(0));

		for (var sField in aFields) {
			var sFieldType	= $(oWidget.oForm[sField]).attr('field-type') || 'Default';

			this[sFieldType].init && this[sFieldType].init(oWidget, sField, aArg);
		}

		if (oWidget.oList) {
			for (var sField in aFields) {
				if ($(oWidget.oList[sField]).get(0)) {
					var sFieldType	= $(oWidget.oList[sField]).attr('field-type') || 'Default';

					this[sFieldType].init && this[sFieldType].init(oWidget, sField, aArg, 'oList');
				}
			}
		}

		this.setUpload(oWidget);
		this.validate(oWidget);
		this.actions(oWidget);
	},

	populate : function(oWidget, aList)
	{
		var aFields	= oWidget.aValidation.rules;

		for (var sField in aFields) {
			var sFieldType	= $(oWidget.oForm[sField]).attr('field-type') || 'Default';

			this[sFieldType].populate && this[sFieldType].populate(oWidget, aList);
		}
	},

	set : function(oWidget, aArg, aResp, aIndexes)
	{
		var aFields	= oWidget.aValidation.rules;
		var aItem	= aArg ? aResp.aData[aResp.aData.length - 1].aItem : {};

		for (var sField in aFields) {
			var sFieldType	= $(oWidget.oForm[sField]).attr('field-type') || 'Default';
			this[sFieldType].populate && this[sFieldType].populate(oWidget, sField, (
				aIndexes.indexOf(sField) >= 0 ? aResp.aData[aIndexes.indexOf(sField)].aList.aRows : {}	
			), aItem);
		}

		if (aArg) {
			for (var sField in aFields) {
				var sFieldType	= $(oWidget.oForm[sField]).attr('field-type') || 'Default';
				this[sFieldType].set && this[sFieldType].set(oWidget, sField, aItem);
			}
		} else {
			if (!oWidget.oList) return;

			var aGet	= oForms.get(oWidget, aArg, 'oList');

			for (var sField in aGet.aFields) {				
				var sFieldType	= $(oWidget.oForm[sField]).attr('field-type') || 'Default';
				this[sFieldType].set && this[sFieldType].set(oWidget, sField, aGet.aFields);				
			}
		}
	},

	get : function(oWidget, aArg, sForm)
	{
		var aFields		= oWidget.aValidation.rules;
		var aRequest	= {};
		var aColumns	= {};
		var mField		= null;
		var oForm		= oWidget[sForm ? sForm : 'oForm'];

		for (var sField in aFields) {
			if (oForm[sField] !== undefined) {
				mField	= this[$(oForm[sField]).attr('field-type') || 'Default'].get(oWidget, oForm, sField, aArg);
				oWidget.sIndex == sField ? (aRequest[sField] = mField) : (aColumns[sField] = mField);
			}
		}

		aRequest.aFields	= aColumns;

		return aRequest;
	},

	reset : function(oWidget, aArg)
	{
		oFiles.sRoute	= oWidget.sModule;

		var aFields	= oWidget.aValidation.rules;
		var sRole	= oTools.getRole(oWidget.sForm);
		var sMode	= aArg ? (aArg.sMode ? aArg.sMode : 'Edit') : 'Add';
		var aTitles	= {
			page	: '.box-title',
			dialog	: '.modal-title'
		};

		$(oWidget.sForm).attr('mode', sMode);
		$(oWidget.sForm+' '+aTitles[sRole]+' > span > span').empty().append(sMode);

		if (sRole == 'page') {
			$(oWidget.sForm).parents('[role]:first')
				.fadeIn()
				.siblings()
				.css('display', 'none')
				.fadeOut();
		}
		if (sRole == 'dialog') {
			$(oWidget.sForm).parents('[role]:first')
				.modal({
					backdrop	: 'static',
					keyboard	: false
				});
		}

		oWidget.oForm.reset();

		for (var sField in aFields) {
			var sFieldType	= $(oWidget.oForm[sField]).attr('field-type') || 'Default';

			this[sFieldType].reset && this[sFieldType].reset(oWidget, sField);
		}

		$(oWidget.oForm).toggleClass('disabled', false).find('input:not(.static), select:not(.static), textarea:not(.static)').prop("disabled", false);
		$(oWidget.oForm).validate().resetForm();

		return oWidget.oForm;
	},

	CheckBoxes :
	{
		init : function()
		{
		},

		populate : function(oWidget, sField, aList, aItem)
		{
			$(oWidget.oForm[sField]).parents('.form-group:first').find('.Container').empty().append(oTools.genCheckBoxes({
				aList	: aList,
				iCol	: parseInt($(oWidget.oForm[sField]).attr('field-columns') || 12)
			}));
		},

		set : function(oWidget, sField, aItem)
		{
			oTools.setChecked(oWidget.oForm, {
				sKey	: sField,
				aList	: aItem[sField]
			});
		},

		get : function(oWidget, oForm, sField, aArg)
		{
			
		},

		reset : function(oWidget, sField, aItem)
		{
		}
	},

	Date :
	{
		init : function(oWidget, sField, aArg)
		{
			var oField	= $(oWidget.oForm[sField]);

			oField.datetimepicker({
		        format: oField.attr('widget-date-format') ? oField.attr('widget-date-format') : 'M d, yyyy',
		        autoclose: true,
		        todayBtn: true,
				minView: 2
		    });
		},

		set : function(oWidget, sField, aItem)
		{
			oWidget.oForm[sField].value	= oTools.date($(oWidget.oForm[sField]).attr('field-date-format'), aItem[sField]);
		},

		get : function(oWidget, oForm, sField, aArg)
		{
			return oPhp.strtotime(oForm[sField].value);
		},

		reset : function(oWidget, sField, aItem)
		{
			var oField	= $(oWidget.oForm[sField]);

			oField.val(
				oField.attr('field-default') ?
				oTools.date(oField.attr('field-date-format'), parseInt(oField.attr('field-default'))) :
				''
			);
		}
	},

	Default :
	{
		set : function(oWidget, sField, aItem)
		{
			oWidget.oForm[sField].value	= aItem[sField];
		},

		get : function(oWidget, oForm, sField, aArg)
		{
			return oForm[sField].value;
		},

		reset : function(oWidget, sField, aItem)
		{
			var oField	= $(oWidget.oForm[sField]);
			
			oField.val(
				oField.attr('field-default') ?
				oField.attr('field-default') :
				''
			);
		}
	},

	Editor :
	{
		init : function(oWidget, sField, aArg)
		{
			var sSelect	= oWidget.sForm+' [field-type="Editor"][name="'+sField+'"]';

			tinyMCE.init({
				selector				: sSelect,
				height					: 300,
				init_instance_callback	: function (oEditor) {
					$(sSelect).get(0).Editor	= oEditor;
				}
			});
		},

		set : function(oWidget, sField, aItem, aLists)
		{
			var oField	= $(oWidget.oForm[sField]).get(0);

			oField.Editor.setContent(aItem[sField]);
			tinyMCE.triggerSave();
		},

		get : function(oWidget, oForm, sField, aArg)
		{
			var oField	= $(oForm[sField]);

			tinyMCE.triggerSave();
			
			return oField.val();
		},

		reset : function(oWidget, sField, aItem)
		{
			var oField	= $(oWidget.oForm[sField]);

			oField.get(0).Editor.setContent(
				oField.attr('field-default') ?
				oField.attr('field-default') :
				''
			);
			tinyMCE.triggerSave();
		}
	},

	Image :
	{
		reset : function(oWidget, sField, aItem)
		{
			oTools.image(oWidget, sField, '');
		},

		get : function(oWidget, oForm, sField, aArg)
		{
			if ($(oWidget.oForm[sField]).hasClass('static')) return;

			return oTools.image(oWidget, sField);
		},

		set : function(oWidget, sField, aItem)
		{
			aItem[sField] && oTools.image(oWidget, sField, aItem[oWidget.sIndex]+'/'+sField+'/'+$(oWidget.oForm[sField]).attr('field-size'));
		}
	},

	ItemList :
	{
		init : function(oWidget, sField, aArg)
		{
			var oNode	= $(oWidget.oForm).find('select[name="'+sField+'"]').get(0);

			$(oNode).parent().append('<div></div>');
			$(oNode).change(function()
			{
				var oSelf	= this;

				if ($(this).val() && !$(this).siblings('div').find('input[value="'+$(this).val()+'"]:not(.static)').get().length) {
					$('<i action="delItem" class="fa fa-minus-circle"></i>').click(function(){
						$(oSelf).find('option[value="'+$(this).closest('.input-group').find('input:not(.static)').val()+'"]').prop('disabled', false);
						$(this).closest('.input-group').remove();
					}).appendTo(
						$('<span class="input-group-addon"></span>').appendTo(
							$('<div class="input-group"></div>').append(
								'<input value="'+$(this).val()+'" type="hidden" >'+
								'<input class="form-control static" value="'+$(this).find('option:selected').prop('disabled', true).text()+'" type="text" disabled="disabled" >'
							).appendTo($(this).siblings('div'))
						)
					);
				}

				$(this).val('');
			});
		},

		populate : function(oWidget, sField, aList, aItem)
		{
			oForms.Select.populate(oWidget, sField, aList, aItem);
		},

		set : function(oWidget, sField, aItem)
		{
			oForms.Select.set(oWidget, sField, aItem);

			var aList	= aItem[sField].aRows; 
			
			for (var i = 0, l = aList.length; i < l; i++) {
				$(oWidget.oForm[sField]).val(aList[i]).trigger('change');
			};
		},

		set_old : function(oWidget, sField, aItem)
		{
			oForms.Select.set(oWidget, sField, aItem);

			var aList	= aItem[$(oWidget.oForm[sField]).attr('field-list')].aRows; 
			
			for (var i = 0, l = aList.length; i < l; i++) {
				$(oWidget.oForm[sField]).val(aList[i]).trigger('change');
			};
		},

		get : function(oWidget, oForm, sField, aArg)
		{
			var aItems	= $(oWidget.oForm[sField]).siblings('div').find('input[type="hidden"]').get();

			for (var i = 0, l = aItems.length, aReturn=[]; i < l; i++) aReturn.push(aItems[i].value);

			return aReturn;
		},

		reset : function(oWidget, sField, aItem)
		{
			$(oWidget.oForm[sField]).parent().find('div:first').empty();
		}
	},

	MultiSelect :
	{
		populate : function(oWidget, sField, aList, aItem)
		{
			oForms.oOptions.set(oWidget.oForm[sField], {
				aList 	: aList,
				sKey	: $(oWidget.oForm[sField]).attr('field-key'),
				sVal	: $(oWidget.oForm[sField]).attr('field-val')
			});
		},

		set : function(oWidget, sField, aItem)
		{
			$(oWidget.oForm[sField]).val(aItem[sField].aRows).trigger('change');
		},

		get : function(oWidget, oForm, sField, aArg)
		{
			console.log($(oForm[sField]).val());

			return $(oForm[sField]).val();
		},

		reset : function(oWidget, sField, aItem)
		{
			$(oWidget.oForm[sField]).empty().trigger('change');
		}
	},

	Password :
	{
		get : function(oWidget, oForm, sField, aArg)
		{
			return oForm[sField].value === '' ? undefined : oForm[sField].value;
		}
	},

	Select :
	{
		init : function(oWidget, sField, aArg, sForm)
		{
			var oForm	= oWidget[sForm ? sForm : 'oForm'];
			var oField	= $(oForm[sField]);

			if (oField.attr('field-chain')) {
				oField.change(
					function()
					{
						oField.attr('field-filter') && oTables.filter(oWidget, oField.attr('field-filter'), $(this).val());

						var aCustom		= {};
						var sChainId	= $(this).attr('field-chain');
						aCustom[sField] = $(this).val();						
						(
							$(this).val() ?
							oCommon.options($(this).attr('field-module'), aCustom, function(aResp)
							{
								var aCustom				= {};
								aCustom[sChainId] = {
									aList	: aResp.aData.aList.aRows,
									sPick	: oTools.getCustom(oForm[sField], sChainId, true)
								};

								$(oForm[sChainId]).select2('val', '');
								oForms.oOptions.many(oForm, aCustom);
							}) :
							oForms.oOptions.empty(oForm[sChainId])
						);
					}
				);
			} else if (oField.attr('field-filter')) {
				oField.change(
					function()
					{
						oTables.filter(oWidget, oField.attr('field-filter'), $(this).val());
					}
				);
			}
		},

		populate : function(oWidget, sField, aList, aItem)
		{
			oForms.oOptions.set(oWidget.oForm[sField], {
				aList 	: aList,
				sKey	: $(oWidget.oForm[sField]).attr('field-key'),
				sVal	: $(oWidget.oForm[sField]).attr('field-val')
			});
		},

		set : function(oWidget, sField, aItem)
		{
			var aCustom	= {};
			var oField	= $(oWidget.oForm[sField]);

			if (oField.attr('field-chain')) {
				var sChain		= oField.attr('field-chain');
				aCustom[sChain] = aItem[sChain];
				$(oWidget.oForm[sChain]).data('bChange', false);
			};

			oField.data('aCustom', aCustom);
			oField.data('bChange') !== false && oField.val(aItem[sField]).trigger('change');
		},

		get : function(oWidget, oForm, sField, aArg)
		{
			return oForm[sField].value;
		},

		reset : function(oWidget, sField, aItem)
		{
			var oField	= $(oWidget.oForm[sField]);

			oField.val(
				oField.attr('field-default') ?
				oField.attr('field-default') :
				''
			).trigger('change');
		}
	},

	SortableAccess :
	{
		init : function(oWidget, sField, aArg)
		{
			$(oWidget.sForm+' .sortBasket .sortableGroup, '+oWidget.sForm+' .sortShelf .sortableGroup').sortable({
				distance : 10,
				connectWith: ".sortableGroup",
				receive: function(event, ui)
				{
					$('.sortShelf div.panel.expand').toggleClass('expand');
					$('.sortShelf ul.list-group > .list-group-item.check').toggleClass('check');
				}
			}).disableSelection();
		},

		populate : function(oWidget, sField, aList, aItem)
		{
			var sList				= $(oWidget.oForm[sField]).attr('field-list');
			var sPair				= $(oWidget.oForm[sField]).attr('field-pair');
			var sVal				= $(oWidget.oForm[sField]).attr('field-val');
			var sPairKey			= $(oWidget.oForm[sField]).attr('field-pair-key');
			var sPairVal			= $(oWidget.oForm[sField]).attr('field-pair-val');
			var aRecordPermissions	= aItem[sList] ? aItem[sList].aRows : [];
			var sSelect				= oWidget.sForm+' [field-type="Editor"][name="'+sField+'"]';

			for (var i = 0, iL = aRecordPermissions.length; i < iL; i++) {
				aList[aRecordPermissions[i][sField]].bBasket	= true;
				var aListItems		= aList[aRecordPermissions[i][sField]][sPair].aRows;
				var aPermissions	= parseInt(aRecordPermissions[i].iPermission, 10).toString(2).split('').reverse();

				for (var j=0, jL = aListItems.length, aHtml = []; j < jL; j++) {
					aHtml.push(
						'<li class="list-group-item'+(aPermissions[j] && parseInt(aPermissions[j]) ? ' check' : '')+'"><span name="'+sPairKey+'" val="'+aListItems[j][sPairKey]+'" class="fa"></span> '+aListItems[j][sPairVal]+'</li>'
					);
				}

				$(oWidget.sForm+' .sortBasket .sortableGroup').append(
					'<li>'+
						'<div class="panel panel-primary">'+
							'<div class="panel-heading"><span name="'+sField+'" val="'+aRecordPermissions[i][sField]+'" class="fa"></span> '+aList[aRecordPermissions[i][sField]][sVal]+'</div>'+
							'<ul class="list-group">'+aHtml.join('')+'</ul>'+
						'</div>'+
					'</li>'
				);
			}

			for (var iRecordId in aList) {
				if (!aList[iRecordId].bBasket) {
					var aListItems	= aList[iRecordId][sPair].aRows;
					for (var i=0, iL = aListItems.length, aHtml = []; i < iL; i++) {
						aHtml.push(
							'<li class="list-group-item"><span name="'+sPairKey+'" val="'+aListItems[i][sPairKey]+'" class="fa"></span> '+aListItems[i][sPairVal]+'</li>'
						);
					}
	
					$(oWidget.sForm+' .sortShelf .sortableGroup').append(
						'<li>'+
							'<div class="panel panel-primary">'+
								'<div class="panel-heading" name="'+sField+'" val="'+iRecordId+'"><span class="fa"></span> '+aList[iRecordId][sVal]+'</div>'+
								'<ul class="list-group">'+aHtml.join('')+'</ul>'+
							'</div>'+
						'</li>'
					);
				}
			}

			$(oWidget.sForm+' .sortableGroup .panel-heading').click(function()
			{
				$(this).closest('.sortableGroup').parents('ul:first').parent().find('.sortBasket').find(this).parent().toggleClass('expand');
			});
			$(oWidget.sForm+' .sortableGroup .list-group > .list-group-item').click(function()
			{
				$(this).toggleClass('check');
			});			
		},

		set : function(oWidget, sField, aItem)
		{

		},

		get : function(oWidget, oForm, sField, aArg)
		{
			var aParams		= {};
			aParams[sField]	= {
				sSelector	: '.list-group > li.check',
				aKeys		: {}
			};
			aParams[sField].aKeys[$(oWidget.oForm[sField]).attr('field-pair-key')]	= {};

			return oTools.getValues($(oWidget.sForm+' .sortBasket .sortableGroup > li'), aParams);
		},

		reset : function(oWidget, sField, aItem)
		{
			$.each($(oWidget.sForm+' .sortableGroup > li').get(), function(){
				$(this).remove();
			});
		}
	},

	SortableList :
	{
		init : function(oWidget, sField, aArg)
		{
			$('.modal[name="Modules"] .sortBasket .sortableGroup, .modal[name="Modules"] .sortShelf .sortableGroup').sortable({
				distance : 10,
				connectWith: ".sortableGroup",
				receive: function( event, ui ) {
					$('.sortShelf div.panel.expand').toggleClass('expand');
					$('.sortShelf ul.list-group > .list-group-item.check').toggleClass('check');
				}
			}).disableSelection();
		},

		populate : function(oWidget, sField, aList, aItem)
		{
			var sList		= $(oWidget.oForm[sField]).attr('field-list');
			var sVal		= $(oWidget.oForm[sField]).attr('field-val');
			var aRecordList	= aItem[sList] ? aItem[sList].aRows : [];

			for (var i = 0, l = aRecordList.length; i < l; i++) {
				aList[aRecordList[i][sField]].bBasket	= true;
				$(oWidget.sForm+' .sortBasket .sortableGroup').append(
					'<li val="'+aRecordList[i][sField]+'">'+
						'<div class="panel panel-primary">'+
							'<div class="panel-heading" name="'+sField+'" val="'+aRecordList[i][sField]+'"><span class="fa"></span> '+aList[aRecordList[i][sField]][sVal]+'</div>'+
						'</div>		'+
					'</li>'
				);
			}
			for (var iRecordId in aList) {
				!aList[iRecordId].bBasket && $(oWidget.sForm+' .sortShelf .sortableGroup').append(
					'<li>'+
						'<div class="panel panel-primary">'+
							'<div class="panel-heading" name="'+sField+'" val="'+iRecordId+'"><span class="fa"></span> '+aList[iRecordId][sVal]+'</div>'+
						'</div>		'+
					'</li>'
				);
			}
		},

		set : function(oWidget, sField, aItem)
		{
		},

		get : function(oWidget, oForm, sField, aArg)
		{
			var aParams		= {};
			aParams[sField]	= {};

			return oTools.getValues($(oWidget.sForm+' .sortBasket .sortableGroup > li'), aParams);
		},

		reset : function(oWidget, sField, aItem)
		{
			$.each($(oWidget.sForm+' .sortableGroup > li').get(), function(){
				$(this).remove();
			});
		}
	},

	Toggle :
	{
		populate : function(oWidget, sField, aList, aItem)
		{			
		},

		set : function(oWidget, sField, aItem)
		{
			$(oWidget.oForm[sField]).bootstrapToggle(parseInt(aItem[sField]) ? 'on' : 'off');
		},

		get : function(oWidget, oForm, sField, aArg)
		{
			return $(oForm[sField]).prop("checked") ? 1 : 0; 
		},

		reset : function(oWidget, sField, aItem)
		{
			var oField	= $(oWidget.oForm[sField]);

			$(oField).bootstrapToggle((oField.attr('field-default') ? parseInt(oField.attr('field-default')) : false) ? 'on' : 'off');
		}
	},

	oItemList :
	{
		init : function(oWidget)
		{
			var aItemLists	= $(oWidget.oForm).find('select.ItemList').get();

			if (!aItemLists.length) return;

			for (var i = 0, l = aItemLists.length; i < l; i++) {
				
				var oParent	= $(aItemLists[i]).parent();

				oParent.append('<div></div>');

				$(aItemLists[i]).change(function()
				{
					var oSelf	= this;

					if ($(this).val() && !$(this).siblings('div').find('input[value="'+$(this).val()+'"]:not(.static)').get().length) {
						$('<i action="delItem" class="fa fa-minus-circle"></i>').click(function(){
							$(oSelf).find('option[value="'+$(this).closest('.input-group').find('input:not(.static)').val()+'"]').prop('disabled', false);
							$(this).closest('.input-group').remove();
						}).appendTo(
							$('<span class="input-group-addon"></span>').appendTo(
								$('<div class="input-group"></div>').append(
									'<input value="'+$(this).val()+'" type="hidden" >'+
									'<input class="form-control static" value="'+$(this).find('option:selected').prop('disabled', true).text()+'" type="text" disabled="disabled" >'
								).appendTo($(this).siblings('div'))
							)
						);
					}
					
					$(this).val('');
				});
			} 
		},

		set : function(oWidget, aList, sKey)
		{
			for (var i = 0, l = aList.aRows.length; i < l; i++) {
				$(oWidget.oForm[sKey]).val(aList.aRows[i]).trigger('change');
			};
		},

		get : function(oWidget, sKey)
		{
			var aItems	= $(oWidget.oForm).find('select[name="'+sKey+'"]').siblings('div').find('input[type="hidden"]').get();

			for (var i = 0, l = aItems.length, aReturn=[]; i < l; i++) aReturn.push(aItems[i].value);

			return aReturn;
		},

		reset : function(oForm)
		{
			var aItemLists	= $(oForm).find('select.ItemList').get();

			for (var i = 0, l = aItemLists.length; i < l; i++) $(aItemLists[i]).parent().find('div:first').empty();
		}
	},

	oOptions :
	{
		gen : function(aArg)
		{
			var aHtm	= [];
	
			if (aArg.aList) {
				if (aArg.aList.length) {
					for (var i = 0, l = aArg.aList.length; i < l; i++) 
						aHtm.push('<option value="'+aArg.aList[i][aArg.sKey]+'"'+(aArg.aList[i][aArg.sKey] == aArg.sPick ? ' selected' : '')+'>'+aArg.aList[i][aArg.sVal]+'<\/option>');
				} else {
					for (var sKey in aArg.aList)
						aHtm.push('<option value="'+sKey+'"'+(aArg.sPick == sKey ? ' selected' : '')+'>'+aArg.aList[sKey]+'<\/option>'+"\r");
				}
			}
	
			return aHtm.join('');
		},

		change : function(oSelect, aArg)
		{
			aArg.change && oSelect.change(aArg.change);
			aArg.sPick && oSelect.trigger('change');

			return oSelect;
		},

		set : function(oSelect, aArg)
		{
			var sOptions	= this.gen(aArg);

			return this.change($(oSelect).empty().append((
				sOptions ?
				this.gen({
					aList : (
						aArg.aCustom ?
						aArg.aCustom :
						{'' : '&nbsp'}
					)
				}) : 
				''
			) + sOptions), aArg);
		},

		many : function(oForm, aArg)
		{
			for (var sFieldName in aArg)
				this.set(oForm[sFieldName], aArg[sFieldName]);
		},

		empty : function(oSelect)
		{
			$(oSelect).select2('val', '');
			$(oSelect).empty();
		}
	},

	close : function(oWidget)
	{
		var sRole	= $(oWidget.oForm).parents('[role]:first').attr('role');

		if (sRole == 'dialog') {
		}
		if (sRole == 'page') {
			$('div[name="'+oWidget.sModule+'"][role="list"]')
				.fadeIn()
				.siblings()
				.css('display', 'none')
				.fadeOut();
		}

		$(oWidget.aDataTable.sId).dataTable()._fnAjaxUpdate();
	},

	click : function(oNode)
	{
		oTables.runAction(oModules, oNode, {});
	},

	setUpload : function(oWidget)
	{
		$(oWidget.oForm).find('div.upload > img').click(function()
		{
			$(this).parent().find('input[type="file"]').click();
		});

		$(oWidget.oForm).find('input[type="file"]').change(function(oEvent)
		{
			oTables.runAction(oModules, this, {
				oEvent	: oEvent
			});
		});
	},

	actions : function(oWidget)
	{
		var aFields	= $(oWidget.oForm).find('[action]').get();

		for (var i = 0, l = aFields.length; i < l; i++) {
			$(aFields[i]).attr('type') != 'file' &&
			$(aFields[i]).click(function()
			{
				if ($(this).attr('action') == 'save' && !oForms.check(oWidget.oForm)) return;
	
				oTables.runAction(oModules, this, {
					sMode	: $(oWidget.oForm).attr('mode')
				});
			});
		}
	},

	validate : function(oWidget)
	{
		if (oWidget.aValidation) {
			$.each(oWidget.aValidation.rules, function(sField)
			{
				if (this.required) (
					$(oWidget.oForm[sField]).siblings('label').get().length ?
					$(oWidget.oForm[sField]).siblings('label').find('i').toggleClass('v-required', true) :
					$(oWidget.oForm[sField]).parents('.form-group:first').find('label:first > i').toggleClass('v-required', true)
				);
			});

			$(oWidget.oForm).validate(oWidget.aValidation);
		}
	},

	check : function(oForm)
	{
		$(oForm).find('.select2-search__field').attr('name', 'select2-search__field');

		var bValid	= $(oForm).valid();

		bValid && $(oForm).closest('.modal[name]').modal('hide');

		return bValid;	
	},

	setFields : function(aItem, oForm)
	{
		$.each(aItem, function(sField)
		{
			oForm[sField] && oForm[sField].type != 'file' && (oForm[sField].value = aItem[sField]);
		});
	},

	cancelUpload : function(oNode)
	{
		oSecret.call(oSettings.sUrl+'api/', {
			sMethod	: 'dt/item/cancel',
			aArg	: {
				aRequest	: {
					sTemp	: $(oNode).parent().find('[name="sTemp"]').val()
				}
			}
		}, function(aResp) {
			$(oNode).parent().fadeOut(200, function(){
				$(this).remove();
			});
		});
	},

	empty : function(oWidget)
	{
		if (oWidget.oForm[oWidget.sIndex].value && oSettings.aFiles[oWidget.sModule]) delete oSettings.aFiles[oWidget.sModule][String(oWidget.oForm[oWidget.sIndex].value)];
	}
};

window.oInit =
{
	form : function()
	{
	    $('.select2').select2({
			templateResult : function formatState(state)
			{
				return state.id ?
				$('<span>'+state.text+'</span>') :
				state.text;
			}
		});

		$('[data-toggle="toggle"]').bootstrapToggle();

		$('.accordion').collapse({
			parent: true
		});
	},

	page : function()
	{
		$('body').css('display', '');
	},

	toast : function()
	{
		toastr.options = oSettings.aToast;
	},

	login : function(cCb)
	{
		if (oSettings.isLogged()) {
			oInit.settings(oSettings.get());
			cCb && cCb();
		} else {
			$(function()
			{
				$('.login-box input[type="checkbox"]').iCheck({
					checkboxClass: 'icheckbox_square-blue',
					radioClass: 'iradio_square-blue',
					increaseArea: '20%' // optional
				});
			});

			oLogin.init(function(){
				cCb && cCb();
			});

			oConfig.bDebug && (
				$('.login-page input[name="sEmail"]').val('elowie.cruz@gmail.com'),
				$('.login-page input[name="sPassword"]').val('abc123'),
				$('.login-page button[name="sLogin"]').get(0)
//				.click()
			);
		}
	},

	settings : function(aArg)
	{
		oSettings.set(aArg);

		//this.projects();

		$('.user-menu [data="sFullName"], .user-panel [data="sFullName"]').empty().html(
			oSettings.aData.aUser.sFirstName+' '+
			oSettings.aData.aUser.sLastName
		);
		$('.user-menu [data="sCreateDate"]').empty().html(
			'Member since '+oPhp.date("F j, Y", oSettings.aData.aUser.iCreateDate)
		);
		oTools.setImage(
			'.user-menu img[data="sPhoto"], .user-panel img[data="sPhoto"]',
			'Users/'+oSettings.aData.aUser.iUserId+'/sPhoto/140x140'
		);
		$('.user-menu [data="sLastLogIn"]').empty().html(
			'Last login '+oPhp.date('F j, Y, g:i a', oSettings.aData.aUser.iLastLogIn)
		);

		oPages.init(aArg);
		oFiles.init();

		$('[data-toggle="tooltip"]').tooltip();
	}
};

window.oLogin =
{
	bInit : false,

	init : function(cCb)
	{
		oSecret.getKey() ? (cCb && cCb()) : $('body').toggleClass('login-page', true);

		if (this.bInit || !(this.bInit = true)) return;

		$('.login-box button[name="sLogin"]').click(function () {

			var aLogin	= {
				sSecret		: oPhp.sha1($('.login-box input[name="sEmail"]').val()),
//				sEmail		: $('.login-box input[name="sEmail"]').val(),
				sCaptcha	: $('.login-box input[name="sCaptcha"]').val(),
				iUnixTime	: Date.now()
			};

			oSecret.setKey({
				sPriKey : oSecret.genHash(
					oPhp.sha1(
						oPhp.sha1(
							$('.login-box input[name="sPassword"]').val()
						) + aLogin.iUnixTime
					), 0, 32
				)
			});

			$('.login-box input[name="sEmail"]').val('');
			$('.login-box input[name="sPassword"]').val('');
			$('.login-box input[name="sCaptcha"]').val('');

			oLogin.authenticate(aLogin, cCb);
		});
	},

	captcha : function(cCb)
	{
		oData.call(oSettings.sUrl+'captcha/get', '', function(aResp) {

			$('.Pages .Captcha').attr("src", aResp.sCaptcha);

			cCb && cCb(aResp);
		});
	},

	authenticate : function(aArg, cCb)
	{
		var sPubKey	= oSecret.genRandom(oSecret.getKey(), 16);

		oData.call(oSettings.sUrl+'api/', {
			sSecret		: aArg.sSecret,
//			sEmail		: aArg.sEmail,
			sPubKey		: sPubKey,
			iNoCache	: aArg.iUnixTime,
			sData		: oCipher.Encrypt({
				sText	: JSON.stringify({
					sMethod		: 'users/auth/login',
					aArg		: {
						sSecret		: aArg.sSecret,
						sCaptcha	: aArg.sCaptcha
					}
				}),
				sIv		: sPubKey,
				sKey	: oSecret.getKey(),
				sCipher	: oSecret.sCipher,
				sMode	: oSecret.sMode
			})
		}, cCb);
	}
};

window.oMain =
{
	aUser	: null,
	bDebug	: true,

	init : function()
	{
		oInit.page();
		oInit.toast();
		oInit.login(function()
		{
			oInit.form();
		});
	}
};

window.oMap  =
{
	init : function()
	{

	}
};

window.oModules =
{
	sModule : null,

	getCrumbs : function(oNode, aCrumbs)
	{
		var aCrumb	= $(oNode).find('span:first').parent().children().clone(true);

		aCrumbs.unshift(aCrumb);

		aCrumb.length > 2 && delete(aCrumb[2]);

		return (
			$(oNode).parents('ul:first').hasClass('root-menu') ?
			aCrumbs :
			this.getCrumbs($(oNode).parents('ul:first').parents('li:first').children('a:first'), aCrumbs)
		);
	},

	init : function(oNode, aArg)
	{
		this.sModule	= $(oNode).attr('name');

		$('.content-wrapper .breadcrumb').empty();

		var aCrumbs	= this.getCrumbs(oNode, []);

		$('.content-wrapper > .content-header > h1')
			.empty().append($(aCrumbs[aCrumbs.length - 1]).clone(true))
			.append($('<small>').append(this[this.sModule].sDesc));

		$.each(aCrumbs, function(i)
		{
			$('.content-wrapper .breadcrumb').append($('<li>').append(aCrumbs[i]));
		});

		this[this.sModule].init && this[this.sModule].init({
			oHeader	: $(aCrumbs[aCrumbs.length - 1])
		});
	},

	reload : function(oWidget)
	{
		oTables.reload(null, oWidget, { bDialog : false});
	},

	request : function(oWidget, aArg, aRequests, cCb, cResp)
	{
		oForms.reset(oWidget, aArg);

		aRequests[0].length ?
		oSecret.call(oSettings.sUrl+'api/', {
			sMethod	: 'dt/item/requests',
			aArg	: {
				aRequests : aRequests[0]
			}
		}, function(aResp)
		{
			console.log(aResp);
			oForms.set(oWidget, aArg, aResp, aRequests[1]);
			oTools.callback(cResp, aResp);
			oTools.callback(cCb, aResp);
		}) : oTools.callback(cCb, null);		
	},

	save : function(oWidget, aArg, cCb)
	{
		oSecret.call(oSettings.sUrl+'api/', {
			sMethod	: 'dt/item/'+aArg.sMode.toLowerCase(),
			aArg	: {
				sModule		: oWidget.sModule,
				aRequest	: oForms.get(oWidget, aArg)
			}
		}, function(aResp)
		{
			oForms.empty(oWidget);
			$(oWidget.aDataTable.sId).dataTable()._fnAjaxUpdate();
			oForms.close(oWidget);
			oDialog.success({
				sTitle : aArg.sMode+' '+oWidget.sModule,
				sMsg : 'Successful'
			});

			cCb && cCb(aResp);
		});
	}
};

window.oPages =
{
	show : function()
	{
		$('body').removeClass('login-page').addClass('Main');		
	},

	init : function(aArg)
	{
		this.show();
		this.set();

		for (var sModule in oPages.aModules) {
			oSettings.aData.aPermissions[sModule] && oPages.aModules[sModule].init && oPages.aModules[sModule].init();
		};
	},

	set : function(cCb)
	{
		var aPermissions	= oSettings.aData.aPermissions;

		for (var sModuleName in aPermissions) {
			aPermissions[sModuleName].aModules.sList && $('.wrapper > .content-wrapper > .content').append(aPermissions[sModuleName].aModules.sList);
			aPermissions[sModuleName].aModules.sForm && $('body > #ModalContainer').append(aPermissions[sModuleName].aModules.sForm);
			aPermissions[sModuleName].aModules.sJs && eval(aPermissions[sModuleName].aModules.sJs);

			delete(aPermissions[sModuleName].aModules);
		}
	},

	aModules :
	{
		Admin :
		{
			init : function()
			{
				var aList		= oPages.aModules.Admin.aWidgets.aList;
				var bNotSet		= true;
				var	aPermission	= null;

				for (var sItem in aList) {
					aPermission	= oSettings.aData.aPermissions[sItem];
					(
						aPermission ?
						$('.main-sidebar .sidebar-menu a[name="'+sItem+'"]')
							.click(function()
							{
								$(this).parents('.treeview:first').find('li.active').removeClass('active');
								$(this).parent().addClass('active');

								$('.content-wrapper > section.content > div[name="'+$(this).attr('name')+'"][role="list"]')
									.fadeIn()
									.siblings()
									.css('display', 'none')
									.fadeOut();

								oModules.init(this);

							})
							.addClass(aPermission && bNotSet ? 'active' : '')
							.parent().css('display', '') :
						$('.main-sidebar .sidebar-menu a[name="'+sItem+'"]').parent().css('display', 'none')
					);
					$('.content-wrapper > section.content > div[name="'+sItem+'"][role="list"]')
						.css('display', aPermission && bNotSet ? '' : 'none');

					bNotSet		= bNotSet && !(aPermission && bNotSet);
				}

				oModules.init($('.main-sidebar .sidebar-menu a.active').get(0), {
					sModule : $('.main-sidebar .sidebar-menu a.active').attr('name') 
				});
			},

			aWidgets :
			{
				aList : {
					Agencies				: null,
					ApplicationStatuses		: null,
					AppointmentVenues		: null,
					CityMunicipalities		: null,
					Countries 				: null,
					Couriers 				: null,
					Metas 					: null,
					MetaGroups 				: null,
					Users					: null,
					FileIcons				: null,
					FileTypes				: null,
					Modules					: null,
					Permissions				: null,
					Roles					: null,
					Titles					: null,
					VisaCategories			: null,
					VisaClasses				: null,
					VisaDeliveries			: null,
					VisaTypes				: null

				}
			}
		}
	}
};

window.oResize =
{
	aTasks : [
/*
		function()
		{
			$('.content-wrapper').css('height', (
				$(window).innerHeight()
				- $('.main-header').innerHeight()
				- $('.main-footer').innerHeight()
				- 2
			));
		}
*/
		function()
		{

			$('#PageContainer').css('height', (
				$(window).innerHeight()
				- $('.main-header').innerHeight()
				- $('.main-footer').innerHeight()
				- 2
				- 38
			));
		}
	],

	adjust : function()
	{
		for (var i = 0, l = this.aTasks.length; i < l; i++) {
			this.aTasks[i]();
		}
	}
};

window.oSecret =
{
	sPriKey : null,
	sCipher : 'rijndael-128',
	sMode : 'cbc',

	call : function(sUrl, aArg, cCb)
	{
		oData.call(sUrl, this.encrypt(JSON.stringify(aArg)), cCb);
	},

	encrypt : function(sDecData)
	{
		var sPubKey	= this.genRandom(oSecret.getKey(), 16);

		return {
			sPubKey	: sPubKey,
			sData	: oCipher.Encrypt({
				sText	: sDecData,
				sIv		: sPubKey,
				sKey	: oSecret.getKey(),
				sCipher	: this.sCipher,
				sMode	: this.sMode
			})
		};
	},

	decrypt : function(sEncData)
	{
		var aEncData	= JSON.parse(window.atob(sEncData));
		var sDecData	= oCipher.Decrypt({
			sText	: aEncData.sData,
			sIv		: aEncData.sPubKey,
			sKey	: oSecret.getKey(),
			sCipher	: this.sCipher,
			sMode	: this.sMode
		});			

		return JSON.parse(oPhp.trim(sDecData, String.fromCharCode(0)));
	},

	genFileReqst : function(sReqst)
	{
		var sUrl	= oTools.oNode.get(oSettings.aFiles, sReqst.split('/'));

		return sUrl ? sUrl : oTools.oNode.set(oSettings.aFiles, sReqst.split('/'), this.genFileUrl(sReqst));
	},

	genFileUrl : function(sReqst)
	{
		var aEncData	= this.encrypt(JSON.stringify([sReqst]));
		return oSettings.sUrl+'files/'+aEncData.sData.replace(/\//g, "-")+':'+aEncData.sPubKey.replace(/\//g, "-");		
	},

	genShareReqst : function(sReqst)
	{
		var aEncData	= this.encrypt(JSON.stringify([sReqst]));
		return oSettings.sUrl+'shared/'+aEncData.sData.replace(/\//g, "-")+':'+aEncData.sPubKey.replace(/\//g, "-");		
	},

	setKey : function(aArg)
	{
		oSettings.set({
			sPriKey	: aArg.sPriKey
		});
	},

	getKey : function()
	{
		return oSettings.aData.sPriKey;
	},

	genRandom : function(sHash, iLen)
	{
		return oPhp.sha1(Math.floor(Date.now() * Math.random() * Math.random())+sHash).substr(Math.random()  * (40 - iLen), iLen);
	},

	genHash : function(sHash, iStart, iLen)
	{
		return oPhp.sha1(sHash.substr(iStart, iLen)).substr(iStart, iLen);
	}
};

window.oSettings =
{
	sKey	: 'sData',
	sUrl 	: oConfig.sUrl,
	aToast	: {
		timeOut		: 10000,
		progressBar	: true
	},

	aData : {},

	aFiles : {},

	isLogged : function(cCb)
	{
		return false;
//		return !!this.get('sPriKey');
	},

	get : function(sKey)
	{
		var aData = JSON.parse(sessionStorage.getItem(this.sKey));

		return (
			aData ?
			(
				sKey ?
				aData[sKey] :
				aData
			) :
			null
		); 
	},

	set : function(aArg)
	{
		for (var sKey in aArg) {
			this.aData[sKey] = aArg[sKey];
			sessionStorage.setItem(sKey, aArg[sKey]);
		};

		sessionStorage.setItem(this.sKey, JSON.stringify(this.aData));
	},

	clear : function()
	{
		sessionStorage.clear();
		this.aData	= {};
		this.aFiles	= {};
	}
};

window.oLogin =
{
	bInit : false,

	init : function(cCb)
	{
		oSecret.getKey() ? (cCb && cCb()) : $('body').toggleClass('login-page', true);

		if (this.bInit || !(this.bInit = true)) return;

		$('.login-box button[name="sLogin"]').click(function () {

			var aLogin	= {
				sSecret		: oPhp.sha1($('.login-box input[name="sEmail"]').val()),
//				sEmail		: $('.login-box input[name="sEmail"]').val(),
				sCaptcha	: $('.login-box input[name="sCaptcha"]').val(),
				iUnixTime	: Date.now()
			};

			oSecret.setKey({
				sPriKey : oSecret.genHash(
					oPhp.sha1(
						oPhp.sha1(
							$('.login-box input[name="sPassword"]').val()
						) + aLogin.iUnixTime
					), 0, 32
				)
			});

			$('.login-box input[name="sEmail"]').val('');
			$('.login-box input[name="sPassword"]').val('');
			$('.login-box input[name="sCaptcha"]').val('');

			oLogin.authenticate(aLogin, cCb);
		});
	},

	captcha : function(cCb)
	{
		oData.call(oSettings.sUrl+'captcha/get', '', function(aResp) {

			$('.Pages .Captcha').attr("src", aResp.sCaptcha);

			cCb && cCb(aResp);
		});
	},

	authenticate : function(aArg, cCb)
	{
		var sPubKey	= oSecret.genRandom(oSecret.getKey(), 16);

		oData.call(oSettings.sUrl+'api/', {
			sSecret		: aArg.sSecret,
//			sEmail		: aArg.sEmail,
			sPubKey		: sPubKey,
			iNoCache	: aArg.iUnixTime,
			sData		: oCipher.Encrypt({
				sText	: JSON.stringify({
					sMethod		: 'users/auth/login',
					aArg		: {
						sSecret		: aArg.sSecret,
						sCaptcha	: aArg.sCaptcha
					}
				}),
				sIv		: sPubKey,
				sKey	: oSecret.getKey(),
				sCipher	: oSecret.sCipher,
				sMode	: oSecret.sMode
			})
		}, cCb);
	}
};

window.oLogin =
{
	bInit : false,

	init : function(cCb)
	{
		oSecret.getKey() ? (cCb && cCb()) : $('body').toggleClass('login-page', true);

		if (this.bInit || !(this.bInit = true)) return;

		$('.login-box button[name="sLogin"]').click(function () {

			var aLogin	= {
				sSecret		: oPhp.sha1($('.login-box input[name="sEmail"]').val()),
//				sEmail		: $('.login-box input[name="sEmail"]').val(),
				sCaptcha	: $('.login-box input[name="sCaptcha"]').val(),
				iUnixTime	: Date.now()
			};

			oSecret.setKey({
				sPriKey : oSecret.genHash(
					oPhp.sha1(
						oPhp.sha1(
							$('.login-box input[name="sPassword"]').val()
						) + aLogin.iUnixTime
					), 0, 32
				)
			});

			$('.login-box input[name="sEmail"]').val('');
			$('.login-box input[name="sPassword"]').val('');
			$('.login-box input[name="sCaptcha"]').val('');

			oLogin.authenticate(aLogin, cCb);
		});
	},

	captcha : function(cCb)
	{
		oData.call(oSettings.sUrl+'captcha/get', '', function(aResp) {

			$('.Pages .Captcha').attr("src", aResp.sCaptcha);

			cCb && cCb(aResp);
		});
	},

	authenticate : function(aArg, cCb)
	{
		var sPubKey	= oSecret.genRandom(oSecret.getKey(), 16);

		oData.call(oSettings.sUrl+'api/', {
			sSecret		: aArg.sSecret,
//			sEmail		: aArg.sEmail,
			sPubKey		: sPubKey,
			iNoCache	: aArg.iUnixTime,
			sData		: oCipher.Encrypt({
				sText	: JSON.stringify({
					sMethod		: 'users/auth/login',
					aArg		: {
						sSecret		: aArg.sSecret,
						sCaptcha	: aArg.sCaptcha
					}
				}),
				sIv		: sPubKey,
				sKey	: oSecret.getKey(),
				sCipher	: oSecret.sCipher,
				sMode	: oSecret.sMode
			})
		}, cCb);
	}
};

window.oLogin =
{
	bInit : false,

	init : function(cCb)
	{
		oSecret.getKey() ? (cCb && cCb()) : $('body').toggleClass('login-page', true);

		if (this.bInit || !(this.bInit = true)) return;

		$('.login-box button[name="sLogin"]').click(function () {

			var aLogin	= {
				sSecret		: oPhp.sha1($('.login-box input[name="sEmail"]').val()),
//				sEmail		: $('.login-box input[name="sEmail"]').val(),
				sCaptcha	: $('.login-box input[name="sCaptcha"]').val(),
				iUnixTime	: Date.now()
			};

			oSecret.setKey({
				sPriKey : oSecret.genHash(
					oPhp.sha1(
						oPhp.sha1(
							$('.login-box input[name="sPassword"]').val()
						) + aLogin.iUnixTime
					), 0, 32
				)
			});

			$('.login-box input[name="sEmail"]').val('');
			$('.login-box input[name="sPassword"]').val('');
			$('.login-box input[name="sCaptcha"]').val('');

			oLogin.authenticate(aLogin, cCb);
		});
	},

	captcha : function(cCb)
	{
		oData.call(oSettings.sUrl+'captcha/get', '', function(aResp) {

			$('.Pages .Captcha').attr("src", aResp.sCaptcha);

			cCb && cCb(aResp);
		});
	},

	authenticate : function(aArg, cCb)
	{
		var sPubKey	= oSecret.genRandom(oSecret.getKey(), 16);

		oData.call(oSettings.sUrl+'api/', {
			sSecret		: aArg.sSecret,
//			sEmail		: aArg.sEmail,
			sPubKey		: sPubKey,
			iNoCache	: aArg.iUnixTime,
			sData		: oCipher.Encrypt({
				sText	: JSON.stringify({
					sMethod		: 'users/auth/login',
					aArg		: {
						sSecret		: aArg.sSecret,
						sCaptcha	: aArg.sCaptcha
					}
				}),
				sIv		: sPubKey,
				sKey	: oSecret.getKey(),
				sCipher	: oSecret.sCipher,
				sMode	: oSecret.sMode
			})
		}, cCb);
	}
};

window.oTools =
{
	getApi : function(sModule, aArg)
	{
		var aApi		= JSON.parse(JSON.stringify(oCommon.aModules[sModule]));
		aApi.aRequest	= oPhp.array_merge(aApi.aRequest, aArg);

		return aApi;
	},
	
	getRole : function(sSelect)
	{
		return $(sSelect).parents('[role]:first').attr('role');
	},

	callback : function(cCb, aResp)
	{
		cCb && cCb(aResp);
	},

	formatBytes : function (bytes, decimals)
	{
	   if(bytes == 0) return '0 Byte';
	   var k = 1000;
	   var dm = decimals + 1 || 3;
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
	   var i = Math.floor(Math.log(bytes) / Math.log(k));
	   return (bytes / Math.pow(k, i)).toPrecision(dm) + ' ' + sizes[i];
	},

	date : function(sFormat, iTime)
	{
		return parseInt(iTime) ? oPhp.date(sFormat, iTime) : '';
	},

	image : function(oWidget, sKey, sVal)
	{
		var oImg	= $(oWidget.sForm+' .upload > img[name="'+sKey+'"]');

		return (
			sVal === undefined ?
			oImg.attr("src") :
			(
				sVal === '' ?
				oImg.attr("src", '') :
				oTools.setImage(oImg, oWidget.sModule+'/'+sVal)
			)
		);
	},

	isInit : function(oNode)
	{
		return (
			typeof(oNode.bInit) == 'boolean' ?
			oNode.bInit || !(oNode.bInit = true) :
			!!oNode.attr('init') || !oNode.attr('init', 1)
		);
	},

	isBase64Image : function(sImg)
	{
		return sImg.indexOf('image') >= 0;
	},

	json64Encode : function(aArg)
	{
		return window.btoa(JSON.stringify(aArg));
	},

	json64Decode : function(sArg)
	{
		return JSON.parse(window.atob(sArg));
	},

	tick : function(oNode)
	{
		var oTick	= $(oNode).attr('type') === 'checkbox' ? $(oNode) : $(oNode).siblings('input').click(); 

		oTick.siblings('div')
			.toggleClass('Hide', !oTick.get(0).checked)
			.find('input').removeAttr('checked');
	},

	calcTimeDiff : function(iTime)
	{
		var aHtml	= [];
		var aTime	= {
			iDays	: ['d', Math.floor(iTime / 86400)],
			iHours	: ['h', Math.floor((iTime % 86400) / 3600)],
			iMins	: ['m', Math.floor((iTime % 3600) / 60)]
		};

		for (var sTime in aTime) {
			aTime[sTime][1] && aHtml.push(aTime[sTime][1]+aTime[sTime][0]);
		}

		return aHtml.join(' ');
	},

	setChecked : function(oForm, aArg)
	{
		for (var i = 0, iL = aArg.aList.length, oCheckbox = null; i < iL; i++) {
			oCheckbox = $(oForm).find('[name="'+aArg.sKey+'"][value="'+aArg.aList[i][aArg.sKey]+'"]').get(0);
			oCheckbox.checked || $(oCheckbox).click();
		}
	},

	setParent : function(oNode)
	{
		$.each(oNode.aWidgets, function(sWidget)
		{
			this.oParent	= oNode.aWidgets; 
		});
	},

	getCustom : function(oNode, sKey, bClear)
	{
		var aCustom	= $(oNode).data('aCustom') ? $(oNode).data('aCustom') : {};

		bClear === true && $(oNode).removeData('aCustom');

		return (
			sKey === undefined ?
			aCustom :
			aCustom[sKey]
		); 
	},

	getValues : function(oSelector, aKeys)
	{
		var aList	= oSelector.get();
		
		for (var i = 0, iL = aList.length, aVals = []; i < iL; i++) {
			var aVal = {};
			for (var sKey in aKeys) {
				aVal[sKey]	= [
					$(aList[i]).find('[name="'+sKey+'"]').attr('val'),
					this.getValues($(aList[i]).find(aKeys[sKey].sSelector), aKeys[sKey].aKeys)
				];
			}
			aVals.push(aVal);
		}

		return aVals;
	},

	getChecked : function(oSelector)
	{
		var aList	= oSelector.get();
		
		for (var i = 0, iL = aList.length, aVals = []; i < iL; i++) aVals.push(aList[i].value);

		return aVals;
	},

	genCheckBoxes : function(aArg)
	{
		var aHtml	= [];
		
		if (aArg.aList.length) {
			for (var i = 0, iL = aArg.aList.length; i < iL; i++) {
				
				if (aArg.aSubs) {
					for (var j = 0, jL = aArg.aSubs.length, aHtml2 = []; j < jL; j++) {
						aArg.aSubs[j].aList = aArg.aList[i][aArg.aSubs[j].sListName].aRows;
						aHtml2.push(
							this.genCheckBoxes(aArg.aSubs[j])
						);
					}
				}

				aHtml.push(
					'<div class="Checkbox col-sm-'+aArg.iCol+'">' +
						'<input type="checkbox" name="'+aArg.sKey+'" value="'+aArg.aList[i][aArg.sKey]+'" style="width:15px;" onclick="oTools.tick(this)"><span onclick="oTools.tick(this)">'+aArg.aList[i][aArg.sVal]+'</span>'+
						(
							aArg.aSubs ? 
							'<div class="Sub Hide">'+
								aHtml2.join('')+
							'</div>' :
							''
						)+
					'</div>'
				);
			}
		} else {
			for (var sKey in aArg.aList) {
				aHtml.push(
					'<div class="Checkbox col-sm-'+aArg.iCol+'">' +
						'<input type="checkbox" name="'+aArg.sKey+'" value="'+sKey+'" style="width:15px;" onclick="oTools.tick(this)"><span onclick="oTools.tick(this)">'+aArg.aList[sKey]+'</span>'+
					'</div>'
				);
			}
		}

		return aHtml.join('');
	},

	genFileReqst : function($sReqst)
	{
		return oSettings.sUrl+'files/'+oPhp.trim(window.btoa($sReqst), '=');
	},

	saveContent : function (fileContents, fileName)
	{
		var link = document.createElement('a');
		link.download = fileName;
		link.href = fileContents;
		link.click();
	},

	setFile : function(aArg)
	{
		if (aArg.aRow.sFileTypeMime.indexOf('image') >= 0) {
			return '<img src="'+oSecret.genFileUrl(aArg.sModule+'/'+aArg.aRow[aArg.sIndex]+'/sFileName/'+aArg.sSize)+'">';
		}
		if (aArg.aRow.sFileTypeMime.indexOf('audio') >= 0) {
			return '<audio controls><source src="'+oSecret.genFileReqst(aArg.sModule+'/'+aArg.aRow[aArg.sIndex]+'/sFileName')+'" type="'+aArg.aRow.sFileTypeMime+'">Your browser does not support the audio element.</audio>';
		}
		if (aArg.aRow.sFileTypeMime.indexOf('video') >= 0) {
			return '<video width="320" height="240" controls><source src="'+oSecret.genFileReqst(aArg.sModule+'/'+aArg.aRow[aArg.sIndex]+'/sFileName')+'" type="'+aArg.aRow.sFileTypeMime+'">Your browser does not support the video element.</video>';
		}

		return aArg.aRow.sOrigName;
	},

	setImage : function(mSelector, sArg)
	{
		$(mSelector).attr("src", oSecret.genFileReqst(sArg));
	},

	setForm : function(aArg, sModule)
	{
		$('.modal[name="'+sModule+'"] .modal-title > span > span').html(aArg ? (aArg.sMode ? aArg.sMode : 'Edit') : 'Add');
		$('.modal[name="'+sModule+'"] form').attr('mode', aArg ? (aArg.sMode ? aArg.sMode : 'Edit') : 'Add');

		oFiles.sRoute	= sModule;
		var oForm		= $('.modal[name="'+sModule+'"]').modal({
			backdrop	: 'static',
			keyboard	: false
		}).find('form').get(0);

		$(oForm).toggleClass('disabled', false).find('input:not(.static), select:not(.static), textarea:not(.static)').prop("disabled", false);

		oForm.reset();
		$(oForm).validate().resetForm();
		return oForm;
	},

	checkField : function(mField)
	{
		return mField ? mField : null;
	},

	strtotime : function(sVal)
	{
		var iTime	= oPhp.strtotime(sVal);
		return iTime === false ? '' : iTime;
	},

	oAccordion :
	{
		toggle : function(oNode)
		{
			
			
			
			$(oNode)
				.data('toggle', $(oNode).data('toggle') == 'show' ? 'hide' : 'show')
				.toggleClass('Collapse', $(oNode).data('toggle') == 'hide')
				.parent().find('.accordion .collapse')
				.collapse($(oNode).data('toggle'));
				
				$(oNode).find('i').removeClass('fa-plus-square-o fa-minus-square-o').addClass($(oNode).data('toggle') == 'show' ? 'fa-minus-square-o' : 'fa-plus-square-o');

				

		}
	},

	oNode :
	{
		set : function(oNode, aPath, mVal)
		{
			var sPath	= String(aPath.shift());

			if (aPath.length) {
				oNode[sPath] || (oNode[sPath] = {});  
				return this.set(oNode[sPath], aPath, mVal);
			} else {
				return (oNode[sPath] = mVal);
			}
		},
		
		get : function(oNode, aPath)
		{
			var sPath	= aPath.shift();

			return (
				oNode[sPath] && aPath.length ?
				this.get(oNode[sPath], aPath) :
				oNode[sPath]
			);
		}
	}
};

window.oTables =
{
	cancel : function(oNode, oWidget, aArg)
	{
		var sRole	= $(oNode).parents('[role]:first').attr('role');

		if (sRole == 'page') {
			$('div[name="'+oWidget.sModule+'"][role="list"]')
				.fadeIn()
				.siblings()
				.css('display', 'none')
				.fadeOut();
		}
	},
	
	init : function(oWidget)
	{
		if (oTools.isInit($(oWidget.aDataTable.sId))) return;
		
		var aDataTable		= oWidget.aDataTable;
		var aColumnDefs		= aDataTable.aColumnDefs ? aDataTable.aColumnDefs : [];
		var aPermissions	= oSettings.aData.aPermissions[aDataTable.sModule];
		var aOrder			= aDataTable.aOrder ? aDataTable.aOrder : [[ 0, 'asc' ]];

		if (oWidget.aActions && oWidget.aActions.aHeader) {
			var aHtml	= [];
			for (var i=0; i<oWidget.aActions.aHeader.length; i++) {
				(aPermissions[oWidget.aActions.aHeader[i].sPermission] || oWidget.aActions.aHeader[i].sPermission === '')&& (
					aHtml.push('<li action="'+oWidget.aActions.aHeader[i].sAction+'"><a href="javascript:void(0)"><i class="fa '+oWidget.aActions.aHeader[i].sIcon+'"></i>'+oWidget.aActions.aHeader[i].sText+'</a></li>')	
				);
			}

			$('.content-wrapper [name="'+oWidget.sModule+'"] .box-tools')
				.prepend(
					'<div class="btn-group">'+
						'<button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i></button>'+
						'<ul class="dropdown-menu pull-right" role="menu"></ul>'+
					'</div>'
				)
				.find('.dropdown-menu')
				.append(aHtml.join(''));
			$('.content-wrapper [name="'+oWidget.sModule+'"] .box-tools')
				.find('[action]')
				.click(function()
				{
					oTables.runAction(oModules, this, {});
				});
		}

		(aPermissions.create || aPermissions.update || aPermissions.delete || aPermissions.update || (oWidget.aActions && oWidget.aActions.aRows)) && aDataTable.aColumns.push({
			bSortable	: false,
			mData		: null,
			sWidth		: 28 * 2,
			sTitle		: (
				aPermissions.create ?
				'<div class="btn-group">'+
					'<button action="form" type="button" onclick="oTables.runAction(oModules, this, false);" class="btn btn-default btn-sm fa">&#xf067;</button>'+
				'</div>' :
				''
			),
			render		: function (data, type, row)
			{
				oWidget.aActions ? null : (oWidget.aActions = {}); 
				oWidget.aActions.aRows ? null : (oWidget.aActions.aRows = []);

				var aActions	= JSON.parse(JSON.stringify(oWidget.aActions.aRows)); 

				aPermissions.delete !== undefined && aPermissions.delete && (
					parseInt(row.bDeleted) ? 
					aActions.unshift({
						sPermission	: 'revive',
						sAction		: 'revive',
						sClass		: '',
						sIcon		: 'fa-undo',
						sText		: 'Revive'
					}) :
					aActions.unshift({
						sPermission	: 'delete',
						sAction		: 'delete',
						sClass		: '',
						sIcon		: 'fa-minus',
						sText		: 'Delete'
					})
				);

				aPermissions.update && aActions.unshift({
					sPermission	: 'update',
					sAction		: 'form',
					sClass		: '',
					sIcon		: 'fa-pencil-square-o',
					sText		: 'Edit'
				});

				aPermissions.view && aActions.unshift({
					sPermission	: 'view',
					sAction		: 'view',
					sClass		: '',
					sIcon		: 'fa-search',
					sText		: 'View'
				});

				if (!aActions.length) return '';

				var aButtons	= [aActions.shift()];

				aActions.length == 1 && (
					aButtons.push(aActions.shift())
				);

				for (var i=0, aHtmlList = []; i<aActions.length; i++) {
					(aPermissions[aActions[i].sPermission] || aActions[i].sPermission === '') && (
						aHtmlList.push('<li action="'+aActions[i].sAction+'"><a href="javascript:void(0)"><i class="fa '+aActions[i].sIcon+'"></i>'+aActions[i].sText+'</a></li>')	
					);
				}
				for (var i=0, aHtmlBtns = []; i<aButtons.length; i++) {
					aHtmlBtns.push('<button action="'+aButtons[i].sAction+'" type="button" class="btn btn-default btn-sm fa '+aButtons[i].sIcon+'"></button>');
				}

				return (
					'<div class="btn-group tools">'+
						aHtmlBtns.join('')+
						(
							aHtmlList.length ? 
							'<div class="btn-group">'+
								'<button class="btn dropdown-toggle btn-default btn-sm fa fa-bars" data-toggle="dropdown"></button>'+
								'<ul class="dropdown-menu pull-right" role="menu">'+
									aHtmlList.join('')+
								'</ul>'+
							'</div>' :
							''
						)+
					'</div>'
				);
			},

			createdCell	: function(oTd, cellData, rowData, row, col)
			{
				$(oTd).find('[action]').click(function()
				{
					oTables.runAction(oModules, this, {
						aData	: rowData
					});
				});
			}
		});

		var oDt	= $(aDataTable.sId).dataTable({
	        serverSide : true,
			oLanguage : {
				oPaginate	: {
					sPrevious	: '<',
					sNext		: '>'
				}
			},
	        searching : !!aPermissions.search,
	        order : aOrder,
	        ajax : function(aRequest, fCallback, aSettings)
	        {
	        	aRequest.custom	= (
	        		$(this.selector).attr('custom-data') ?
	        		oTools.json64Decode($(this.selector).attr('custom-data')) :
	        		{}
	        	);

				oSecret.call(oSettings.sUrl+'api/', {
					sMethod	: 'dt/list/get',
					aArg	: {
						sModule		: aDataTable.sModule,
						aRequest	: aRequest
					}
				}, function(aResp)
				{
					fCallback(aResp.aData.aList);

					oTables.setBadges(oWidget, {
						iTotal	: aResp.aData.aList.recordsTotal
					});
				});
	        },
	        columns: aDataTable.aColumns,
			columnDefs : aColumnDefs
	    });

	    oDt.parents('.dataTables_wrapper:first').find('input[type="search"]').attr('name', 'sSearch');

	    
	},

	filter : function(oWidget, iIndex, mVal)
	{
		$(oWidget.aDataTable.sId).dataTable().api().columns(iIndex).search(mVal ? mVal : '').draw();
	},

	runAction : function(oWidget, oNode, aArg, aPath)
	{
		if (typeof(aPath) != 'object') {

			return this.runAction(oWidget, oNode, aArg, $(oNode).parents('[path]:first').attr('path').split('/'));
		}

		var sPath = aPath.shift();

		return (
			aPath.length ? 
			this.runAction(oWidget[sPath].aWidgets, oNode, aArg, aPath) :
			(
				oWidget[sPath][$(oNode).attr('action')] ?
				oWidget[sPath][$(oNode).attr('action')](oNode, aArg) :
				(
					this[$(oNode).attr('action')] ?
		 			this[$(oNode).attr('action')](oNode, oWidget[sPath], aArg) :
					''
				)
			)
		);
	},

	setBadges : function(oWidget, aArg)
	{
		if (oWidget.aBadges) {
			for (var i=0; i<oWidget.aBadges.length; i++) {
				$(oWidget.aBadges[i])
					.html(aArg.iTotal)
					.attr('title', aArg.iTotal+' Recrods');
			}
		}
	},

	delete : function(oNode, oWidget, aArg)
	{
		var aRequest	= {};

		oDialog.confirm({
			sTitle : 'Delete',
			sMsg : 'Comfirm delete '+oWidget.sItem+'?'
		}, function()
		{
			aRequest[oWidget.sIndex]	= aArg.aData[oWidget.sIndex];
			
			oSecret.call(oSettings.sUrl+'api/', {
				sMethod	: 'dt/item/delete',
				aArg	: {
					sModule		: oWidget.sModule,
					aRequest	: aRequest
				}
			}, function(aResp)
			{
				$('#'+oWidget.sModule+'DataTable').dataTable()._fnAjaxUpdate();
			});
		});
	},

	upload : function(oNode, oWidget, aArg)
	{
		oFiles.drop(aArg.oEvent, oWidget.aUploads[oNode.name]);
		
		$(oNode).replaceWith($(oNode).clone(true));
	},

	revive : function(oNode, oWidget, aArg)
	{
		var aRequest				= {};
		aRequest[oWidget.sIndex]	= aArg.aData[oWidget.sIndex];

		oSecret.call(oSettings.sUrl+'api/', {
			sMethod	: 'dt/item/revive',
			aArg	: {
				sModule		: oWidget.sModule,
				aRequest	: aRequest
			}
		}, function(aResp)
		{
			$('#'+oWidget.sModule+'DataTable').dataTable()._fnAjaxUpdate();
		});
	},

	reset : function(oNode, oWidget, aArg)
	{
//		$('.content-wrapper [name="'+oWidget.sModule+'"] form').get(0).reset();
	},

	genItemPdf : function(oNode, oWidget, aArg)
	{
		oSecret.call(oSettings.sUrl+'api/', {
			sMethod	: 'dt/item/pdf',
			aArg	: {
				sModule		: oWidget.sModule,
				aRequest	: {
					sHtml : oPhp.utf8_encode($(oNode).parents('[path]:first').get(0).innerHTML)
				}
			}
		}, function(aResp)
		{
			window.open("data:application/pdf;base64," + aResp.aData.sB64Pdf);
		});
	},

	pdfList : function(oNode, oWidget, aArg)
	{
		var aRequest	= $(oWidget.aDataTable.sId).dataTable()._fnAjaxParameters();
    	aRequest.custom	= (
    		$(oWidget.aDataTable.sId).attr('custom-data') ?
    		oTools.json64Decode($(oWidget.aDataTable.sId).attr('custom-data')) :
    		{}
    	);

		oSecret.call(oSettings.sUrl+'api/', {
			sMethod	: 'dt/list/pdf',
			aArg	: {
				sModule		: oWidget.sModule,
				aRequest	: aRequest
			}
		}, function(aResp)
		{
			window.open("data:application/pdf;base64," + aResp.aData.sB64Pdf);
		});
	},

	pdfItem : function(oNode, oWidget, aArg)
	{
		oSecret.call(oSettings.sUrl+'api/', {
			sMethod	: 'dt/item/pdf',
			aArg	: {
				sModule		: oWidget.sModule,
				aRequest	: aArg.aData
			}
		}, function(aResp)
		{
			window.open("data:application/pdf;base64," + aResp.aData.sB64Pdf);
		});
	},

	view : function(oNode, oWidget, aArg)
	{
		oWidget.form(oNode, aArg, function()
		{
			$('.modal[name="'+oWidget.sModule+'"] .modal-title > span > span').html('View');
			$(oWidget.oForm).toggleClass('disabled', true).find('input:not(.static), select:not(.static), textarea:not(.static)').prop('disabled', true);
		});
	},
	
	reload : function(oNode, oWidget, aArg)
	{
		$(oWidget.aDataTable.sId).dataTable()._fnAjaxUpdate();
		aArg.bDialog !== false && oDialog.success({
			sTitle	: 'Reload '+oWidget.sModule,
			sMsg	: 'Successful'
		});
	},

	status : function(aArg)
	{
		var aColumns	= [];

		if (aArg.aColumns) {
			for (var i = 0, l = aArg.aColumns.length; i < l; i++) {
				aColumns.push(aArg.aRow[aArg.aColumns[i]]);
			}
		}

		return (
			(aArg.aRow.bActive != undefined || aArg.aRow.bDeleted != undefined) ?
			parseInt(aArg.aRow.bDeleted) ?
			'<i class="fa fa-trash FontRed"></i>' : (
				aArg.aRow.bActive == undefined || parseInt(aArg.aRow.bActive) ?
				'<i class="fa fa-check-circle-o FontGreen"></i>' :
				'<i class="fa fa-times-circle-o FontRed"></i>'
			) :
			''
		)+aColumns.join(' ');
	}	
};
